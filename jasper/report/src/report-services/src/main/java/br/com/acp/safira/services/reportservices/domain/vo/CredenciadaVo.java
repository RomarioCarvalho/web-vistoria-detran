package br.com.acp.safira.services.reportservices.domain.vo;

import lombok.Data;

@Data
public class CredenciadaVo {
    private String ecv;
    private String razaoSocial;
    private String cnpj;
    private String logo;
    private String endereco;
    private String portariaDenatran;
    private String status;
    private String municipio;
    private String uf;
    private String cep;
    private String telefone;
    private VistoriadorVo vistoriador;
}
