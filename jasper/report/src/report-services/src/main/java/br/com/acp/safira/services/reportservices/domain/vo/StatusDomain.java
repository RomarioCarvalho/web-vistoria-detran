package br.com.acp.safira.services.reportservices.domain.vo;

public enum StatusDomain {
    PENDING,
    FAILED,
    SUCCESS
}
