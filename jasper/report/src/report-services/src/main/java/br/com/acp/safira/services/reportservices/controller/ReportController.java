package br.com.acp.safira.services.reportservices.controller;

import br.com.acp.safira.services.reportservices.configuration.validation.InternalServerErrorException;
import br.com.acp.safira.services.reportservices.domain.vo.RequestReportVO;
import br.com.acp.safira.services.reportservices.domain.vo.ResponseReportVO;
import br.com.acp.safira.services.reportservices.domain.vo.StatusDomain;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.query.JsonQueryExecuterFactory;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.UUID;

@RestController
@Slf4j
public class ReportController {

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${report.prefix}")
    private String reportPrefix;

    @Value("${report.path.result}")
    private String reportTarget;

    @Value("${report.path.default-img}")
    private String defaultImg;

    @Value("${report.path.storage}")
    private String reportStorage;

    @RequestMapping(value = "/api/v1/laudo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody

    @ApiResponses(value = {
            @ApiResponse(responseCode ="200", description = "Operação realizada com sucesso"),
            @ApiResponse(responseCode ="403", description = "Acesso não autorizado"),
            @ApiResponse(responseCode ="500", description = "Ocorreu um erro interno e não foi possivel entregar uma resposta") })
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseReportVO v1GerarLaudo(@RequestBody RequestReportVO request) {
        ResponseReportVO responseReportVO = null;
        try {
            responseReportVO = new ResponseReportVO();
            if(request.getVistoriaId()!= null){
                responseReportVO.setId(request.getVistoriaId());
            }else {
                responseReportVO.setId(UUID.randomUUID().toString());
            }
            JasperReport jasReport = null;
            jasReport = (JasperReport) JRLoader.loadObject(this.getClass().getResource("/reports/laudo-vistoria.jasper"));

            String jsonAsString = objectMapper.writeValueAsString(request.getVistoria());
            InputStream targetStream = new ByteArrayInputStream(jsonAsString.getBytes());

            LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
            params.put("defaultImgPath", defaultImg);
            params.put("storagePath", reportStorage);
            //params.put("subreportPath", this.getClass().getResource("/reports/ItensVistoria.jasper").getPath());
            log.info(this.getClass().getResource("/reports/ItensVistoria.jasper").getPath());
            params.put("subreportPath", "C:\\Users\\romario.cruz\\Desktop\\jasper\\report\\src\\report-services\\src\\main\\resources\\reports\\ItensVistoria.jasper");
            params.put("subreport_data", request.getVistoria().getItensReprovacao());

            params.put(JsonQueryExecuterFactory.JSON_INPUT_STREAM, targetStream);
            JasperPrint document = JasperFillManager.fillReport(jasReport, params);
            JasperExportManager.exportReportToPdfFile(document, reportTarget+"/"+reportPrefix+ "_" +responseReportVO.getId()+ ".pdf");
            responseReportVO.setStatus(StatusDomain.SUCCESS);
        } catch (JRException ex) {
            log.error(ex.getMessage());
            throw new InternalServerErrorException(ex.getLocalizedMessage());
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            throw new InternalServerErrorException(e.getLocalizedMessage());
        }
        return responseReportVO;
    }
}
