package br.com.acp.safira.services.reportservices.configuration;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfiguration {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }


    @Bean
    public OpenAPI OpenAPIInfo() {
        return new OpenAPI()
                .info(new Info().title("Report Service API")
                        .description("Processador de Relatorios")
                        .version("v0.0.1")
                        .license(new License().name("Apache 2.0").url("http://springdoc.org")))
                //.externalDocs(new ExternalDocumentation()
                //        .description("Report Service Documentation")
                //        .url(""))
        ;
    }
}
