package br.com.acp.safira.services.reportservices.domain.vo;

import lombok.Data;

@Data
public class VeiculoVo {
    private String placa;
    private String marcaModelo;
    private String tipo;
    private Integer anoFabricacao;
    private Integer anoModelo;
    private String cor;
    private String tipoCarroceria;
    private String especie;
    private String categoria;
    private String combustivel;
    private String eixos;
    private String potencia;
    private String cilindrada;
    private String capCarga;
    private String cmt;
    private String pbt;
    private String capPass;
    private String lacre;
    private Double km;
    private String chassi;
    private String vinRegravado;
    private String motor;
    private String apontamento;
    private String fotoTraseira;
    private String fotoChassi;
    private String fotoMotor;
    private String fotoHodometro;
    private String dataFotoTraseira;
    private String dataFotoChassi;
    private String dataFotoMotor;
    private String dataFotoHodometro;
}
