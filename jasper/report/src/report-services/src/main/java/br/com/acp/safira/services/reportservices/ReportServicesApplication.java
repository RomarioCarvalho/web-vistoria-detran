package br.com.acp.safira.services.reportservices;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.annotation.PostConstruct;
import java.io.File;

@SpringBootApplication
@EnableWebMvc
@Slf4j
public class ReportServicesApplication {
	@Value("${report.path.result}")
	private String reportTarget;

	public static void main(String[] args) {
		SpringApplication.run(ReportServicesApplication.class, args);
	}

	@PostConstruct
	void init(){
		File file = new File(reportTarget);
		if(!file.exists()){
			file.mkdir();
		}
	}
}
