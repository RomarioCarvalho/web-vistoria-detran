package br.com.acp.safira.services.reportservices.configuration.validation;

public class InternalServerErrorException extends RuntimeException {

	public InternalServerErrorException(String msg) {
		super(msg);
	}
}
