package br.com.acp.safira.services.reportservices.domain.vo;

import lombok.Data;

@Data
public class VistoriadorVo {
    private String codigo;
    private String nome;
}
