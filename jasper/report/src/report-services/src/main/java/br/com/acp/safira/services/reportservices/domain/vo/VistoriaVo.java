package br.com.acp.safira.services.reportservices.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class VistoriaVo {
    private Boolean aprovado;
    private String laudo;
    private String sclv;
    private String data;
    private String validade;
    private String servicoPrincipal;
    private String servicosAlternativos;
    private String observacoes;
    private String ugc;
    private List<ItensReprovacaoVo> itensReprovacao;
    private ProprietarioVo proprietario;
    private VeiculoVo veiculo;
    private CredenciadaVo credenciada;

}
