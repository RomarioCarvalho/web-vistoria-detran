package br.com.acp.safira.services.reportservices.configuration.validation;

import br.com.acp.safira.services.reportservices.domain.vo.ApiErrorVo;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
public class CustomRestExceptionHandler {
    @ExceptionHandler(InternalServerErrorException.class)
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiErrorVo handleInternalErrorException(InternalServerErrorException ex, WebRequest request) {
        ApiErrorVo apiError = new ApiErrorVo(HttpStatus.INTERNAL_SERVER_ERROR, ex.getLocalizedMessage(), request.getContextPath());
        return apiError;
    }
}