package br.com.acp.safira.services.reportservices.domain.vo;

import lombok.Data;

@Data
public class ProprietarioVo {
    private String nome;
    private String cpfCnpj;
    private String cep;
    private String endereco;
    private String municipio;
    private String uf;
}
