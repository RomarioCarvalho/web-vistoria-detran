package br.com.acp.safira.services.reportservices.domain.vo;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Data
public class ApiErrorVo {
	private Date timestamp;
	private HttpStatus error;
	private Integer status;
	private String message;
	private List<String> errorsStack;
	private String path;

	public ApiErrorVo(HttpStatus status, String message, String path, List<String> errors) {
		super();
		this.timestamp = new Date();
		this.error = status;
		this.status = status.value();
		this.message = message;
		this.path = path;
		this.errorsStack = errors;

	}

	public ApiErrorVo(HttpStatus status, String message, String path, String error) {
		super();
		this.timestamp = new Date();
		this.error = status;
		this.status = status.value();
		this.message = message;
		this.path = path;
		errorsStack = Arrays.asList(error);
	}

	public ApiErrorVo(HttpStatus status, String message, String path) {
		super();
		this.timestamp = new Date();
		this.error = status;
		this.status = status.value();
		this.message = message;
		this.path = path;
	}
}