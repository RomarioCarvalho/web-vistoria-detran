package br.com.acp.safira.services.reportservices.domain.vo;

import lombok.Data;

@Data
public class ItensReprovacaoVo {
    private Boolean aprovado;
    private Boolean reprovado;
    private Boolean na;
    private String nomeItem;
    private String condicao;
    private String id;

}
