package br.com.acp.safira.services.reportservices.domain.vo;

import br.com.acp.safira.services.reportservices.domain.enums.ModelosLaudoVistoria;
import lombok.Data;

import java.util.HashMap;

@Data
public class RequestReportVO {
    private ModelosLaudoVistoria modelosLaudoVistoria;
    private HashMap<String, Object> parameters = new HashMap<>();
    private HashMap<String, Object> filters = new HashMap<>();
    private String vistoriaId;
    private VistoriaVo vistoria;
}
