package br.com.acp.safira.services.reportservices.domain.vo;

import lombok.Data;

import java.util.HashMap;

@Data
public class ResponseReportVO {
    private String id;
    private StatusDomain status;
    private String detalhes;
}
