package br.com.acp.safira.services.reportservices;

import br.com.acp.safira.services.reportservices.controller.ReportController;
import br.com.acp.safira.services.reportservices.domain.vo.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ReportControllerTest {

    @Autowired
    ReportController reportController;

    @Test
    void contextLoads(){

    }

    @Test
    void v1GerarLaudoTest(){
        RequestReportVO request = new RequestReportVO();
        VistoriaVo vistoria = new VistoriaVo();

        ProprietarioVo proprietarioVo = new ProprietarioVo();
        vistoria.setProprietario(proprietarioVo);

        VeiculoVo veiculo = new VeiculoVo();
        vistoria.setVeiculo(veiculo);

        CredenciadaVo credenciada = new CredenciadaVo();
        vistoria.setCredenciada(credenciada);

        request.setVistoria(vistoria);
        ResponseReportVO response = reportController.v1GerarLaudo(request);
    }
}
