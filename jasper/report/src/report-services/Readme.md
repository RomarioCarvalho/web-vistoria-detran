# Report Service

Prototipo de serviço para geração de relatorios.

## Documentação
API: http://localhost:8888/api/api-docs.html

O Swagger Codegen é uma ferramenta que com base no descritor da API é possivel gerar codigo para o lado cliente.
[Swagger Codegen](https://swagger.io/tools/swagger-codegen/)

Resumo do download e execução.
``` shell
# Download current stable 2.x.x branch (Swagger and OpenAPI version 2)
wget https://repo1.maven.org/maven2/io/swagger/swagger-codegen-cli/2.4.26/swagger-codegen-cli-2.4.26.jar -O swagger-codegen-cli.jar

java -jar swagger-codegen-cli.jar help

# Download current stable 3.x.x branch (OpenAPI version 3)
wget https://repo1.maven.org/maven2/io/swagger/codegen/v3/swagger-codegen-cli/3.0.31/swagger-codegen-cli-3.0.31.jar -O swagger-codegen-cli.jar

java -jar swagger-codegen-cli.jar --help

```
O descritor é obtido no link proximo ao logo da documentação ou pelo complemento de URL /v3/api-docs

```shell
#ex: para Csharp
java -jar swagger-codegen-cli.jar generate -i http://172.27.160.1:8888/v3/api-docs -l csharp-dotnet2 -o ./codeGen
```