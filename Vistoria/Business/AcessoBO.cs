﻿using Vistoria.Models.ViewModel;
using System.Collections.Generic;
using System.Linq;

namespace Vistoria.Business
{
    public class AcessoBO
    {
        public static bool TemAcesso(List<MenuVM> lista)
        {
            foreach (MenuVM item in lista)
            {
                if (TemAcesso(item.Projeto, item.Controller, item.Action))
                {
                    return true;
                }
            }
            return false;
        }
        public static bool TemAcesso(string projeto, string controller, string action)
        {
            if (SessionHelper.Usuario != null)
            {
                if (!SessionHelper.Usuario.ListModulo
                        .Where(m =>
                            (m.Path.ToLower() == projeto.ToLower() + "/" + controller.ToLower() + "/" + action.ToLower()) ||
                            (m.Path.ToLower() == projeto.ToLower() + "/" + controller.ToLower() + "/*") ||
                            (m.Path.ToLower() == projeto.ToLower() + "/*")
                        ).Any())
                {
                    return false;
                }
                //return true;
            }
            return true;
        }

        public static bool IsController(List<MenuVM> lista, string projeto, string controller)
        {
            foreach (MenuVM item in lista)
            {
                if (item.Projeto == projeto && item.Controller == controller)
                {
                    return true;
                }
            }
            return false;
        }
    }
}