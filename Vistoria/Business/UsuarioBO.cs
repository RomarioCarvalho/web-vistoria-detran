﻿using Vistoria.Models;
using Vistoria.Models.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Vistoria.Business
{
    public class UsuarioBO
    {
        public static void ValidaLogin(VistoriaContext db, string login, long id)
        {
            var found = db.Usuario.Ativos().Where(x => x.Login.Equals(login) && x.Id != id);
            if (found.Count() > 0)
            {
                throw new Exception("Login já existe na base de dados");
            }
        }

        public static Usuario ObterAtivoPorLogin(VistoriaContext db, string login)
        {
            return db.Usuario
                .Ativos()
                .Where(p => p.Login.ToUpper().Equals(login.ToUpper()))
                .FirstOrDefault();
        }


        public static void Inserir(VistoriaContext db, Usuario usuario)
        {
            if(usuario.AutenticaAD == false)
            {
                ValidaSenhaForte(usuario.Senha);
            }
            ValidaLogin(db, usuario.Login, usuario.Id);
            ValidarEmail(db, usuario.Email, usuario.Id);

            if (!string.IsNullOrEmpty(usuario.Senha))
            {
                usuario.Senha = Hash.SHA256(usuario.Senha);
            }
            usuario.ListGrupo = new List<Grupo>();
            usuario.GruposId.ForEach(x =>
               usuario.ListGrupo.Add(db.Grupo.Find(x))
            );

            db.Usuario.Add(usuario);
            db.SaveChanges();
        }

        public static void Alterar(VistoriaContext db, Usuario usuario)
        {
            if (usuario.AutenticaAD == false)
            {
                ValidaSenhaForte(usuario.Senha);
            }
            ValidaLogin(db, usuario.Login, usuario.Id);
            ValidarEmail(db, usuario.Email, usuario.Id);

            Usuario banco = db.Usuario.Find(usuario.Id);

            if (!string.IsNullOrEmpty(usuario.Senha) && usuario.AutenticaAD == false)
            {
                usuario.Senha = Hash.SHA256(usuario.Senha);
            }
            else
            {
                usuario.Senha = db.Usuario
                    .Ativos()
                    .Where(u => u.Id == usuario.Id)
                    .Select(u => u.Senha)
                    .FirstOrDefault();
            }

            banco.Senha = usuario.Senha;
            banco.Nome = usuario.Nome;
            banco.Login = usuario.Login;
            banco.Email = usuario.Email;
            banco.MatriculaAtendente = usuario.MatriculaAtendente;
            banco.AutenticaAD = usuario.AutenticaAD;
            banco.SitioId = usuario.SitioId;

            banco.ListGrupo.Clear();
            usuario.GruposId.ForEach(x =>
               banco.ListGrupo.Add(db.Grupo.Find(x))
            );
            db.SaveChanges();
        }

        public static void ValidarEmail(VistoriaContext db, string Email, long id)
        {
            var found = db.Usuario.Ativos().Where(x => x.Email.Equals(Email) && x.Id != id);
            if (found.Count() > 0)
            {
                throw new Exception("E-mail já existe na base de dados");
            }

            String[] elements = Regex.Split(Email, "@");
            if (elements.Length == 2)
            {
                elements = Regex.Split(elements[1], ".");
                if (elements.Length >= 2)
                {
                    return;
                }
            }
            throw new Exception("E-mail Inválido");
        }

        public static void ValidaSenhaForte(string senha)
        {
            if (!string.IsNullOrEmpty(senha))
            {
                if ((senha.Length < 8) || (!senha.Any(c => char.IsDigit(c))) || (!senha.Any(c => char.IsLower(c))))
                {
                    throw new Exception("Senha deve conter no minimo 8 digitos (Letras/Números)");
                }
            }
        }
    }
}