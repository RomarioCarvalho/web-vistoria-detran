﻿using Vistoria.Enumerators;
using Vistoria.Models.ViewModel;
using System.Collections.Generic;
using System.Web;

namespace Vistoria.Business
{
    public class SessionHelper
    {
        public static List<MessageVM> ListMessage
        {
            get
            {
                if (HttpContext.Current.Session["ListMessage"] == null)
                {
                    HttpContext.Current.Session["ListMessage"] = new List<MessageVM>();
                }
                return HttpContext.Current.Session["ListMessage"] as List<MessageVM>;
            }
            set
            {
                HttpContext.Current.Session["ListMessage"] = value;
            }
        }

        public static void AddMessage(MessageTipo tipoMessage, string titulo, string texto)
        {
            ListMessage.Add(new MessageVM
            {
                MessageTipo = tipoMessage,
                Titulo = titulo,
                Texto = texto
            });
        }

        public static UsuarioVM Usuario
        {
            get
            {
                if (HttpContext.Current.Session["Usuario"] == null)
                {
                    return null;
                }
                else
                {
                    return HttpContext.Current.Session["Usuario"] as UsuarioVM;
                }
            }
            set
            {
                HttpContext.Current.Session["Usuario"] = value;
            }
        }
    }
}