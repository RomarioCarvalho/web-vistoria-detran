﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Vistoria.Business
{
    public class SendEmail
    {
        private static readonly string host = ConfigurationManager.AppSettings["SMTP_HOST"];
        private static readonly int port = Int32.Parse(ConfigurationManager.AppSettings["SMTP_PORT"]);
        private static readonly string from = ConfigurationManager.AppSettings["SMTP_USER"];
        private static readonly string pass = ConfigurationManager.AppSettings["SMTP_PASS"];
        private static readonly bool isSSl = Boolean.Parse(ConfigurationManager.AppSettings["SMTP_IS_SSL"]);
        private static readonly bool isHTML = true;
        
        public static void Enviar(List<string> tos, string assunto, string body, List<string> paths)
        {
            try
            {
                if (tos != null && tos.Count > 0)
                {
                    using (MailMessage mail = new MailMessage())
                    {
                        mail.From = new MailAddress(from);
                        tos.ForEach(x => mail.To.Add(x));
                        mail.IsBodyHtml = isHTML;
                        mail.Subject = assunto;
                        mail.Body = body;

                        if (paths != null && paths.Count > 0)
                        {
                            foreach (string path in paths)
                            {
                                FileInfo fi = new FileInfo(path);
                                if (fi.Exists)
                                {
                                    System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(path);
                                    mail.Attachments.Add(attachment);
                                }
                            }
                        }

                        using (SmtpClient SmtpServer = new SmtpClient(host, port))
                        {

                            SmtpServer.UseDefaultCredentials = false;
                            SmtpServer.Credentials = new System.Net.NetworkCredential(from, pass);
                            SmtpServer.EnableSsl = isSSl;
                            SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;

                            SmtpServer.Send(mail);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void Enviar(string para, string assunto, string body)

        {
            var tos = new List<string>();
            tos.Add(para);
            Enviar(tos, assunto, body, null);

        }

        public static async Task EnviarAsync(string destinatario, string assunto, string body, List<string> paths)
        {
            try
            {
                 
                    using (MailMessage mail = new MailMessage())
                    {
                        mail.From = new MailAddress(from);
                        mail.To.Add(destinatario);                         
                        mail.IsBodyHtml = isHTML;
                        mail.Subject = assunto;
                        mail.Body = body;

                        if (paths != null && paths.Count > 0)
                        {
                            foreach (string path in paths)
                            {
                                FileInfo fi = new FileInfo(path);
                                if (fi.Exists)
                                {
                                System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(path);
                                    mail.Attachments.Add(attachment);
                                }
                            }
                        }

                        using (SmtpClient SmtpServer = new SmtpClient(host, port))
                        {

                            SmtpServer.UseDefaultCredentials = false;
                            SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                        SmtpServer.Credentials = new System.Net.NetworkCredential(from, pass);
                            SmtpServer.EnableSsl = isSSl;

                                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s,
                                System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                                System.Security.Cryptography.X509Certificates.X509Chain chain,
                                System.Net.Security.SslPolicyErrors sslPolicyErrors)
                                {
                                return true;

                                };

                          await SmtpServer.SendMailAsync(mail);
                    }
                    }
                
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
