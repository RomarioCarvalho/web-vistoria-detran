﻿using Azure.Storage.Blobs;
using Vistoria.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Vistoria.Business
{
    public static class Utils
    {

        public static string txtSelecioneCombos = ".:: Selecione ::.";

        public static IQueryable<T> Ativos<T>(this DbQuery<T> db) where T : AbstractModel
        {
            return db.Where(x => x.Ativo == true);
        }

        public static IEnumerable<T> Ativos<T>(this ICollection<T> db) where T : AbstractModel
        {
            return db.Where(x => x.Ativo == true);
        }

        public static Bitmap Base64ToBitmap(string base64)
        {
            using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(base64)))
            {
                return new Bitmap(ms);
            }
        }

        public static string GerarToken(int length = 32)
        {
            Random random = new Random();
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string Decriptar(this string str,string chavePrivada)
        {
            byte[] inputByteArray = new byte[str.Length + 1];
            byte[] rgbIV = { 0x21, 0x43, 0x56, 0x87, 0x10, 0xfd, 0xea, 0x1c };
            byte[] key = { };

            try
            {
                key = System.Text.Encoding.UTF8.GetBytes(chavePrivada);
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(str);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, rgbIV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string Encriptar(this string str, string chavePrivada)
        {
            byte[] inputByteArray = Encoding.UTF8.GetBytes(str);
            byte[] rgbIV = { 0x21, 0x43, 0x56, 0x87, 0x10, 0xfd, 0xea, 0x1c };
            byte[] key = { };
            try
            {
                key = System.Text.Encoding.UTF8.GetBytes(chavePrivada);
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, rgbIV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }


        public static string Encriptar(this string str)
        {
            return Encriptar(str, "A0D1nX0Q");
        }

        public static string Decriptar(this string str)
        {
            return Decriptar(str, "A0D1nX0Q");
        }


        public static string AccentInsensitive(string texto)
        {
            var normalizedString = texto.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        public static string RemoveSpecialCharacters(this string str)
        {
            str = AccentInsensitive(str);
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == ' ')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static string ToNumber(this string str)
        {
            str = AccentInsensitive(str);
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if (c >= '0' && c <= '9')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static List<T> ListEnum<T>(Type t)
        {
            List<T> retorno = new List<T>();
            foreach (T value2 in Enum.GetValues(t))
            {
                retorno.Add(value2);
            }
            return retorno;
        }

        /// <summary>
        ///     Função de extensão de Enums.
        ///     Obtém um atributo associado ao Enum.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T ObterAtributo<T>(this Enum value) where T : Attribute
        {
            var type = value.GetType();
            var memberInfo = type.GetMember(value.ToString());
            var attributes = memberInfo[0].GetCustomAttributes(typeof(T), false);
            return (T)attributes[0];
        }

        public static List<SelectListItem> EnumToSelect<T>(Type t)
        {
            return ListEnum<T>(t).Select(x => new SelectListItem()
            {
                Text = GetEnumDescription(x),
                Value = "" + ((int)Enum.Parse(t, Enum.GetName(t, x))),
                Selected = false
            }).OrderBy(x => x.Text).ToList();
        }

        public static List<SelectListItem> EnumToSelect<T>(Type t, T item)
        {
            return ListEnum<T>(t).Select(x => new SelectListItem()
            {
                Text = GetEnumDescription(x),
                Value = "" + ((int)Enum.Parse(t, Enum.GetName(t, x))),
                Selected = (int)Enum.Parse(t, Enum.GetName(t, item)) == (int)Enum.Parse(t, Enum.GetName(t, x))
            }).OrderBy(x => x.Text).ToList();
        }

        public static List<SelectListItem> EnumToSelect2<T>(Type t, T item)
        {
            return ListEnum<T>(t).Select(x => new SelectListItem()
            {
                Text = GetEnumDescription(x),
                Value = "" + ((int)Enum.Parse(t, Enum.GetName(t, x))),
                Selected = x.ToString() == item.ToString()
            }).OrderBy(x => x.Text).ToList();
        }


        public static string GetEnumDescription<T>(T value)
        {
            try
            {
                FieldInfo fi = value.GetType().GetField(value.ToString());

                DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                string description = value.ToString();

                if (attributes != null && attributes.Length > 0)
                    description = attributes[0].Description;

                return description;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string GetEnumDescription(this Enum value)
        {
            try
            {
                FieldInfo fi = value.GetType().GetField(value.ToString());

                DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                string description = value.ToString();

                if (attributes != null && attributes.Length > 0)
                    description = attributes[0].Description;

                return description;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string FormatarCpfCnpj(string CpfCnpj)
        {
            try
            {
                if (CpfCnpj.Length <= 11)
                {
                    return Convert.ToUInt64(CpfCnpj).ToString(@"000\.000\.000\-00");
                }
                else
                {
                    return Convert.ToUInt64(CpfCnpj).ToString(@"00\.000\.000\/0000\-00");
                }
            }
            catch (Exception)
            {
                return CpfCnpj;
            }
            
        }

        public static string FormatarRG(string Rg)
        {
            return Convert.ToUInt64(Rg).ToString(@"000\.000\.000\-0");
        }

        public static string FormatarTelefone(string Telefone)
        {
            if (Telefone.Length == 8)
            {
                return Convert.ToUInt64(Telefone).ToString(@"0000\-0000");
            }
            else
            {
                return Convert.ToUInt64(Telefone).ToString(@"00000\-0000");
            }
        }
        public static string FormatarTelefoneDDD(string Telefone)
        {
            if (Telefone.Length == 10)
            {
                return Convert.ToUInt64(Telefone).ToString(@"(00) 0000\-0000");
            }
            else
            {
                return Convert.ToUInt64(Telefone).ToString(@"(00) 00000\-0000");
            }
        }

        public static double ToHour(DateTime? time)
        {
            if (time != null)
            {
                return ((DateTime)time).Hour + ((DateTime)time).Minute / 60;
            }
            return 0;
        }

        public static string ToHour(TimeSpan myTimeSpan)
        {
            return string.Format("{0:D2}:{1:D2}", (int)myTimeSpan.TotalHours, myTimeSpan.Minutes);
        }

        public static string ToHour(this double? tempo)
        {
            if (tempo == null)
            {
                tempo = 0;
            }
            return string.Format("{0:D2}:{1:D2}", (int)tempo, (int)((tempo - (int)tempo) * 60));
        }

        public static string ToHour(this double? tempo, string retorno = "---")
        {
            if (tempo == null || tempo == 0)
            {
                tempo = 0;
                return retorno;
            }
            return string.Format("{0:D2}:{1:D2}", (int)tempo, (int)((tempo - (int)tempo) * 60));
        }

        public static string FullUrl
        {
            get
            {
                var urlhelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
                return string.Format("{0}://{1}:{2}{3}", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.Url.Host, HttpContext.Current.Request.Url.Port, urlhelper.Content("~"));
                //return string.Format("{0}://{1}{2}", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.Url.Host, urlhelper.Content("~"));
            }
        }

        public static string FullUrlServer
        {
            get
            {
                var urlhelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
                return string.Format("{0}://{1}:{2}", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.Url.Host, HttpContext.Current.Request.Url.Port);
            }
        }

        public static bool IsCPFCNPJ(string cpfcnpj, bool vazio)
        {
            if (string.IsNullOrEmpty(cpfcnpj))
                return vazio;
            else
            {
                int[] d = new int[14];
                int[] v = new int[2];
                int j, i, soma;
                string Sequencia, SoNumero;

                SoNumero = Regex.Replace(cpfcnpj, "[^0-9]", string.Empty);
                if (new string(SoNumero[0], SoNumero.Length) == SoNumero) return false;

                if (SoNumero.Length >= 11)
                {
                    for (i = 0; i <= 10; i++) d[i] = Convert.ToInt32(SoNumero.Substring(i, 1));
                    for (i = 0; i <= 1; i++)
                    {
                        soma = 0;
                        for (j = 0; j <= 8 + i; j++) soma += d[j] * (10 + i - j);

                        v[i] = (soma * 10) % 11;
                        if (v[i] == 10) v[i] = 0;
                    }
                    return (v[0] == d[9] & v[1] == d[10]);
                }
                else if (SoNumero.Length >= 12)
                {
                    Sequencia = "6543298765432";
                    for (i = 0; i <= 13; i++) d[i] = Convert.ToInt32(SoNumero.Substring(i, 1));
                    for (i = 0; i <= 1; i++)
                    {
                        soma = 0;
                        for (j = 0; j <= 11 + i; j++)
                            soma += d[j] * Convert.ToInt32(Sequencia.Substring(j + 1 - i, 1));

                        v[i] = (soma * 10) % 11;
                        if (v[i] == 10) v[i] = 0;
                    }
                    return (v[0] == d[12] & v[1] == d[13]);
                }
                else return false;
            }
        }

        public static string GET(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                    return reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    // log errorText
                }
                throw;
            }
        }
        public static string POST(string url, string jsonContent)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(jsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }
            long length = 0;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    length = response.ContentLength;
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        return reader.ReadToEnd();
                    }
                }

            }
            catch (WebException)
            {
                // Log exception and throw as for GET example above
            }
            return null;
        }

        /// <summary>
        /// Substituir chave pelo valor
        /// </summary>
        /// <param name="html"></param>
        /// <param name="replacesList"></param>
        /// <returns></returns>
        private static string Substituir(string html, Dictionary<string, string> replacesList)
        {
            foreach (KeyValuePair<string, string> replaces in replacesList)
                html = Regex.Replace(html, replaces.Key, replaces.Value ?? string.Empty, RegexOptions.IgnoreCase, new TimeSpan(0, 0, 5));
            return html;
        }

        /// <summary>
        ///     Realizar a leitura de arquivo e substitui tags
        /// </summary>
        /// <param name="printing"></param>
        /// <param name="dictionaryReplace"></param>
        /// <returns></returns>
        public static string LerTemplateEsubstituirTags(string templatePath, Dictionary<string, string> dictionaryReplace)
        {
            if (dictionaryReplace == null) return string.Empty;
            return !dictionaryReplace.Any() ? string.Empty : Substituir(LerTemplate(templatePath), dictionaryReplace);
        }

        /// <summary>
        ///     Ler template HTML
        /// </summary>
        /// <param name="templatePath"></param>
        /// <returns></returns>
        private static string LerTemplate(string templatePath)
        {
            //var caminho = //$@"{Environment.CurrentDirectory}\{templatePath}.html";//ConfigurationManager.AppSettings["TEMPLATE_SENHA"].ToString(); //$@"{Environment.CurrentDirectory}\{templatePath}.html";

            try
            {

                //var caminho =  Server.MapPath(templatePath + ".html");
                //Server.MapPath(path) + botId + ".jpg", temp);
                using (StreamReader streamReader = new StreamReader(templatePath))
                {
                    return streamReader.ReadToEnd();
                }
            }
            catch (Exception)
            {
                throw new Exception($"Não foi possível processar ou encontrar o arquivo {templatePath}");
            }
        }

        public static string GetBoolean(this bool tempo)
        {
            return tempo ? "Sim" : "Não";
        }

        public static void UploadBase64ImageAzure(byte[] imageBytes, string container, long botId)
        {
            var fileName = botId.ToString() + ".jpg";
            
            var blobClient = new BlobClient(ConfigurationManager.AppSettings["STORAGE_CONNECTION"], container, fileName);
            
            using (var stream = new MemoryStream(imageBytes))
            {
                blobClient.Upload(stream, true);
            }

            // Retorna a URL da imagem
            //return blobClient.Uri.AbsoluteUri;
        }

        public static string NomeTipoVeiculo(string id)
        {
            string nome = "";
    
            switch (id) {
              case "1":
                nome = "Bicicleta";
                break;
              case "2":
                nome = "Ciclomotor";
                break;
              case "3":
                nome = "Motoneta";
                break;
              case "4":
                nome = "Motocicleta";
                break;
              case "5":
                nome = "Triciclo";
                break;
              case "6":
                nome = "Automóvel";
                break;
              case "7":
                nome = "Microônibus";
                break;
              case "8":
                nome = "Ônibus";
                break;
              case "9":
                nome = "Bonde";
                break;
              case "10":
                nome = "Reboque";
                break;
              case "11":
                nome = "Semi-Reboque";
                break;
              case "12":
                nome = "CHARRETE";
                break;
              case "13":
                nome = "Camioneta";
                break;
              case "14":
                nome = "Caminhão";
                break;
              case "15":
                nome = "CARROCA";
                break;
              case "16":
                nome = "C. DE MAO";
                break;
              case "17":
                nome = "Caminhão Trator";
                break;
              case "18":
                nome = "Trator Rodas";
                break;
              case "19":
                nome = "Trator Esteiras";
                break;
              case "20":
                nome = "Trator Misto";
                break;
              case "21":
                nome = "Quadriciclo";
                break;
              case "22":
                nome = "Chassi Plataforma";
                break;
              case "23":
                nome = "Caminhonete";
                break;
              case "24":
                nome = "SIDE-CAR";
                break;
              case "25":
                nome = "Utilitário";
                break;
              case "26":
                nome = "Motor-Casa";
                break;

              default:
                nome = "Automóvel";
                break;
              }
      
              return nome;
          }

        public static string CorVeiculo(string id)
        {
            string cor = "";

            switch (id)
            {
                case "1":
                    cor = "Amarela";
                    break;
                case "2":
                    cor = "Azul";
                    break;
                case "3":
                    cor = "Bege";
                    break;
                case "4":
                    cor = "Branca";
                    break;
                case "5":
                    cor = "Cinza";
                    break;
                case "6":
                    cor = "Dourada";
                    break;
                case "7":
                    cor = "Grena";
                    break;
                case "8":
                    cor = "Laranja";
                    break;
                case "9":
                    cor = "Marrom";
                    break;
                case "10":
                    cor = "Prata";
                    break;
                case "11":
                    cor = "Preta";
                    break;
                case "12":
                    cor = "Rosa";
                    break;
                case "13":
                    cor = "Roxa";
                    break;
                case "14":
                    cor = "Verde";
                    break;
                case "15":
                    cor = "Vermelha";
                    break;
                case "16":
                    cor = "Fantasia";
                    break;

                default:
                    cor = "NAO INFORMADO";
                    break;
            }
            return cor;
          }

        public static string TipoCarroceria(string id)
        {
            string nome = "";

            switch (id)
            {
                case "-1": nome = "INEXISTENTE"; break;
                case "101": nome = "Ambulância"; break;
                case "102": nome = "Basculante"; break;
                case "103": nome = "Blindada"; break;
                case "104": nome = "Bombeiro"; break;
                case "105": nome = "Buggy"; break;
                case "106": nome = "Cabine Dupla"; break;
                case "107": nome = "Carroceria Aberta"; break;
                case "108": nome = "Carroceria Fechada"; break;
                case "109": nome = "Chassi Porta Contêiner"; break;
                case "110": nome = "Conversível"; break;
                case "111": nome = "Funeral"; break;
                case "112": nome = "Furgão"; break;
                case "113": nome = "Jipe"; break;
                case "114": nome = "KIT MOTOCICLETA/TRICICLO"; break;
                case "115": nome = "Limusine"; break;
                case "116": nome = "Mecanismo Operacional"; break;
                case "117": nome = "MOTOR CASA"; break;
                case "118": nome = "Prancha"; break;
                case "119": nome = "SideCar"; break;
                case "120": nome = "Silo"; break;
                case "121": nome = "Tanque"; break;
                case "122": nome = "Trailler"; break;
                case "123": nome = "Transporte de Militar"; break;
                case "124": nome = "Transporte de Presos"; break;
                case "125": nome = "Transporte Recreativo"; break;
                case "126": nome = "Transporte Trabalhador"; break;
                case "127": nome = "Prancha Porta Contêiner Conversão para Carroceria Aberta"; break;
                case "128": nome = "Prancha Porta Contêiner"; break;
                case "129": nome = "Cabine Estendida"; break;
                case "130": nome = "Trio Elétrico"; break;
                case "131": nome = "Dolly"; break;
                case "132": nome = "Intercambiável"; break;
                case "133": nome = "Roll-on Roll-off"; break;
                case "134": nome = "Carroceria Aberta/Cabine Dupla"; break;
                case "135": nome = "Carroceria Aberta/Cabine Estendida"; break;
                case "136": nome = "Carroceria Aberta/Cabine Suplementar"; break;
                case "137": nome = "Carroceria Fechada/Cabine Dupla"; break;
                case "138": nome = "Carroceria Fechada/Cabine Estendida"; break;
                case "139": nome = "Carroceria Fechada/Cabine Suplementar"; break;
                case "140": nome = "Carroceria Aberta/Intercambiável"; break;
                case "141": nome = "Cabine Dupla/Inacabada"; break;
                case "142": nome = "Mecanismo Operacional/Cabine Dupla"; break;
                case "143": nome = "Transporte Toras/Madeira Bruta"; break;
                case "144": nome = "Inacabada/Cabine Estendida"; break;
                case "145": nome = "Carroceria Aberta/Mecanismo Operacional"; break;
                case "146": nome = "Carroceria Fechada/Mecanismo Operacional"; break;
                case "147": nome = "Tanque/Mecanismo Operacional"; break;
                case "148": nome = "Prancha/Mecanismo Operacional"; break;
                case "149": nome = "Carroceria Aberta/Mecanismo Operacional/Cabine Dupla"; break;
                case "150": nome = "Carroceria Aberta/Mecanismo Operacional/Cabine Estendida"; break;
                case "151": nome = "Carroceria Aberta/Mecanismo Operacional/Cabine Suplementar"; break;
                case "152": nome = "Carroceria Fechada/Mecanismo Operacional/Cabine Dupla"; break;
                case "153": nome = "Carroceria Fechada/Mecanismo Operacional/Cabine Estendida"; break;
                case "154": nome = "Carroceria Fechada/Mecanismo Operacional/Cabine Suplementar"; break;
                case "155": nome = "Tanque/Cabine Dupla"; break;
                case "156": nome = "Tanque/Cabine Estendida"; break;
                case "157": nome = "Tanque/Cabine Suplementar"; break;
                case "158": nome = "Tanque/Mecanismo Operacional/Cabine Dupla"; break;
                case "159": nome = "Tanque/Mecanismo Operacional/Cabine Estendida"; break;
                case "160": nome = "Tanque/Mecanismo Operacional/Cabine Suplementar"; break;
                case "161": nome = "Roll-on Roll-off/Cabine Dupla"; break;
                case "162": nome = "Roll-on Roll-off/Cabine Estendida"; break;
                case "163": nome = "Roll-on Roll-off/Cabine Suplementar"; break;
                case "164": nome = "Basculante/Cabine Dupla"; break;
                case "165": nome = "Basculante/Cabine Estendida"; break;
                case "166": nome = "Basculante/Cabine Suplementar"; break;
                case "167": nome = "Prancha/Cabine Dupla"; break;
                case "168": nome = "Prancha/Cabine Estendida"; break;
                case "169": nome = "Prancha/Cabine Suplementar"; break;
                case "170": nome = "Prancha/Mecanismo Operacional/Cabine Dupla"; break;
                case "171": nome = "Prancha/Mecanismo Operacional/Cabine Estendida"; break;
                case "172": nome = "Prancha/Mecanismo Operacional/Cabine Suplementar"; break;
                case "173": nome = "Carroceria Aberta/Intercambiável/Cabine Dupla"; break;
                case "174": nome = "Carroceria Aberta/Intercambiável/Cabine Estendida"; break;
                case "175": nome = "Carroceria Aberta/Intercambiável/Cabine Suplementar"; break;
                case "176": nome = "Carroceria Aberta/Cabine Tripla"; break;
                case "177": nome = "Carroceria Fechada/Cabine Tripla"; break;
                case "178": nome = "Comércio"; break;
                case "179": nome = "Transporte Granito"; break;
                case "180": nome = "Silo/Basculante"; break;
                case "181": nome = "Basculante/Mecanismo Operacional"; break;
                case "182": nome = "Chassi Contêiner/Cabine Estendida"; break;
                case "183": nome = "Mecanismo Operacional/Cabine Estendida"; break;
                case "184": nome = "Silo/Cabine Estendida"; break;
                case "185": nome = "Container/Carroceria Aberta/Cabine Estendida"; break;
                case "186": nome = "Prancha Contêiner/Cabine Estendida"; break;
                case "187": nome = "Transporte Toras/Cabine Estendida"; break;
                case "188": nome = "Silo/Basculante/Cabine Estendida"; break;
                case "189": nome = "Som"; break;
                case "190": nome = "Transporte de Escolares"; break;
                case "191": nome = "Transporte de Valores"; break;
                case "192": nome = "Transporte de Valores/Mecanismo Operacional"; break;
                case "193": nome = "Tanque Produto Perigoso"; break;
                case "194": nome = "Inacabada"; break;
                case "195": nome = "Transporte de Granito/Cabine Estendida"; break;
                case "196": nome = "Basculante/Mecanismo Operacional /Cabine Estendida"; break;
                case "197": nome = "Chassi Contêiner/ Cabine Dupla"; break;
                case "198": nome = "Silo/Cabine Dupla"; break;
                case "199": nome = "Container/Carroceria Aberta/Cabine Dupla"; break;
                case "200": nome = "Prancha Contêiner/Cabine Dupla"; break;
                case "201": nome = "Transporte Toras/Cabine Dupla"; break;
                case "202": nome = "Transporte Granito/Cabine Dupla"; break;
                case "203": nome = "Silo/Basculante/Cabine Dupla"; break;
                case "204": nome = "Basculante/Mecanismo Operacional/Cabine Dupla"; break;
                case "205": nome = "Cabine Suplementar"; break;
                case "206": nome = "Chassi Contêiner/Cabine Suplementar"; break;
                case "207": nome = "Mecanismo Operacional/Cabine Suplementar"; break;
                case "208": nome = "Silo/Cabine Suplementar"; break;
                case "209": nome = "Container/Carroceria Aberta/Cabine Suplementar"; break;
                case "210": nome = "Prancha Contêiner/Cabine Suplementar"; break;
                case "211": nome = "Transporte Toras/Cabine Suplementar"; break;
                case "212": nome = "Transporte Granito/Cabine Suplementar"; break;
                case "213": nome = "Silo/Basculante/Cabine Suplementar"; break;
                case "214": nome = "Basculante/Mecanismo Operacional/Cabine Suplementar"; break;
                case "215": nome = "Inacabada/Cabine Suplementar"; break;
                case "216": nome = "Cabine Linear"; break;
                case "217": nome = "Basculante/Cabine Linear"; break;
                case "218": nome = "Carroceria Aberta/Cabine Linear"; break;
                case "219": nome = "Carroceria Fechada/Cabine Linear"; break;
                case "220": nome = "Chassi Contêiner/Cabine Linear"; break;
                case "221": nome = "Mecanismo Operacional/Cabine Linear"; break;
                case "222": nome = "Prancha/Cabine Linear"; break;
                case "223": nome = "Silo/Cabine Linear"; break;
                case "224": nome = "Tanque/Cabine Linear"; break;
                case "225": nome = "Contêiner/Carroceria Aberta/Cabine Linear"; break;
                case "226": nome = "Prancha Contêiner/Cabine Linear"; break;
                case "227": nome = "Roll-on-Roll-off/Cabine Linear"; break;
                case "228": nome = "Transporte Toras/Cabine Linear"; break;
                case "229": nome = "Carroceria Aberta/Intercambiável/Cabine Linear"; break;
                case "230": nome = "Carroceria Aberta/Mecanismo Operacional/Cabine Linear"; break;
                case "231": nome = "Carroceria Fechada/Mecanismo Operacional/Cabine Linear"; break;
                case "232": nome = "Tanque/Mecanismo Operacional/Cabine Linear"; break;
                case "233": nome = "Cabine Linear/Prancha/Mecanismo Operacional"; break;
                case "234": nome = "Transporte de Granito/Cabine Linear"; break;
                case "235": nome = "Silo/Basculante/Cabine Linear"; break;
                case "236": nome = "Basculante/Mecanismo Operacional/Cabine Linear"; break;
                case "237": nome = "Inacabada/Cabine Linear"; break;
                case "238": nome = "Cabine Tripla"; break;
                case "239": nome = "Mecanismo Operacional/Cabine Tripla"; break;
                case "240": nome = "Inacabada/Cabine Tripla"; break;
                case "241": nome = "Tanque Produto Perigoso/Cabine Estendida"; break;
                case "242": nome = "Tanque Produto Perigoso/Cabine Dupla"; break;
                case "243": nome = "Tanque Produto Perigoso/Cabine Suplementar"; break;
                case "244": nome = "Tanque Produto Perigoso/Cabine Linear"; break;
                case "245": nome = "Som/Cabine Dupla"; break;
                case "246": nome = "Tanque Produto Perigoso/Mecanismo Operacional"; break;
                case "247": nome = "Tanque Produto Perigoso/Mecanismo Operacional/Cabine Estendida"; break;
                case "248": nome = "Tanque Produto Perigoso/Mecanismo Operacional/Cabine Dupla"; break;
                case "249": nome = "Tanque Produto Perigoso/Mecanismo Operacional/Cabine Suplementar"; break;
                case "250": nome = "Tanque Produto Perigoso/Mecanismo Operacional/Cabine Linear"; break;
                case "251": nome = "Transporte Toras/Mecanismo Operacional"; break;
                case "252": nome = "Transporte Toras/Mecanismo Operacional/Cabine Estendida"; break;
                case "253": nome = "Transporte Toras/Mecanismo Operacional/Cabine Dupla"; break;
                case "254": nome = "Transporte Toras/Mecanismo Operacional/Cabine Suplementar"; break;
                case "255": nome = "Transporte Toras/Mecanismo Operacional/Cabine Linear"; break;
                case "256": nome = "Comboio"; break;
                case "998": nome = "DIFERENTE DA LEGISLACAO"; break;
                case "999": nome = "Nenhuma"; break;
            }
            return nome;
          }

        public static string TipoEspecie(string id)
        {
            string especie = "";

            switch (id)
            {
                case "1":
                    especie = "Passageiro";
                    break;
                case "2":
                    especie = "Carga";
                    break;
                case "3":
                    especie = "Misto";
                    break;
                case "4":
                    especie = "Competição";
                    break;
                case "5":
                    especie = "Tração";
                    break;
                case "6":
                    especie = "Especial";
                    break;
                case "7":
                    especie = "Coleção";
                    break;

                default:
                    especie = "NAO INFORMADO";
                break;
            }
            return especie;
          }

        public static string CategoriaVeiculo(string id)
        {
            string categoria = "";

            switch (id)
            {
                case "1":
                    categoria = "PARTICULAR";
                    break;
                case "2":
                    categoria = "ALUGUEL";
                    break;
                case "3":
                    categoria = "OFICIAL";
                    break;
                case "4":
                    categoria = "EXPERIENCIA";
                    break;
                case "5":
                    categoria = "APRENDIZAGEM";
                    break;
                case "6":
                    categoria = "FABRICANTE";
                    break;
                case "7":
                    categoria = "CHEFE DE MISSAO DIPLOMATICA";
                    break;
                case "8":
                    categoria = "CORPO CONSULAR";
                    break;
                case "9":
                    categoria = "ORGANISMOS INTERNACIONAL";
                    break;
                case "10":
                    categoria = "CORPO DIPLOMATICO";
                    break;
                case "11":
                    categoria = "MISSAO/REPARTICAO/REPRESENTACAO INTERNACIONAL";
                    break;
                case "12":
                    categoria = "ACORDO DE COOPERACAO INTERNACIONAL";
                    break;
                case "13":
                    categoria = "SIDECAR";
                    break;

                default:
                    categoria = "Não especificada";
                    break;
            }
            return categoria;
          }

        public static string TipoCombustivel(string id)
        {
            string combustivel = "";

            switch (id)
            {
                case "1": combustivel = "Alcool"; break;
                case "2": combustivel = "Gasolina"; break;
                case "3": combustivel = "Diesel"; break;
                case "4": combustivel = "Gasogenio"; break;
                case "5": combustivel = "Gas Metano"; break;
                case "6": combustivel = "ELT FT INT"; break;
                case "7": combustivel = "ELT FT EXT"; break;
                case "8": combustivel = "Gasolina/GNC"; break;
                case "9": combustivel = "Alcool/GNC"; break;
                case "10": combustivel = "Diesel/GNC"; break;
                case "12": combustivel = "Alcool/GNV"; break;
                case "13": combustivel = "Gasolina/GNV"; break;
                case "14": combustivel = "Diesel/GNV"; break;
                case "15": combustivel = "GNV"; break;
                case "16": combustivel = "Alcool/Gasolina"; break;
                case "17": combustivel = "GAS/ALC/GN"; break;
                case "18": combustivel = "GASO/ELET"; break;
                case "19": combustivel = "ALCOOL/GASOL/ELETRICO"; break;
                default: combustivel = "Não se aplica"; break;
            }

            return combustivel;
        }
        
        public static string RetornoWSDetran(string id)
        {
            string nome = "";

            switch (id)
            {
                case "0":
                    nome = "Transação ou atualização efetuada com sucesso";
                    break;
                case "1":
                    nome = "Nenhum registro encontrado";
                    break;
                case "3":
                    nome = "Nenhum registro encontrado";
                    break;
                case "17":
                    nome = "Veiculo não cadastrado e com ocorrência de Roubo/Furto";
                    break;
                case "18":
                    nome = "Veículo não cadastrado e com sinalização de alarme";
                    break;
                case "19":
                    nome = "Veículo cadastrado e com sinalização de alarme";
                    break;
                case "37":
                    nome = "Hodômetro inválido/não informado";
                    break;
                case "38":
                    nome = "Data de medição do hodômetro invalida/não informada";
                    break;
                case "41":
                    nome = "Veículo não cadastrado";
                    break;
                case "42":
                    nome = "Veículo baixado";
                    break;
                case "43":
                    nome = "Veículo já emplacado";
                    break;
                case "48":
                    nome = "Veículo cadastrado e com ocorrência de roubo/furto";
                    break;
                case "56":
                    nome = "Placa inválida";
                    break;
                case "57":
                    nome = "Primeiro emplacamento para veículo inacabado";
                    break;
                case "65":
                    nome = "Código RENAVAM inválido";
                    break;
                case "101":
                    nome = "Tipo de Documento Inválido (CPF/CNPJ)";
                    break;
                case "128":
                    nome = "Indicador de veiculo novo/usado inválido";
                    break;
                case "134":
                    nome = "Veículo não consta na base estadual";
                    break;
                case "139":
                    nome = "Erro de Comunicação com a BIN";
                    break;
                case "150":
                    nome = "Usuário/Senha inválido";
                    break;
                case "151":
                    nome = "Operação não permitida";
                    break;
                case "152":
                    nome = "Usuário não cadastrado";
                    break;
                case "153":
                    nome = "Matrícula do usuário está suspensa";
                    break;
                case "154":
                    nome = "Senha inválida";
                    break;
                case "190":
                    nome = "Chassi não preenchido informado";
                    break;
                case "251":
                    nome = "Serviço deve ser somente 1º emplacamento";
                    break;
                case "252":
                    nome = "Veículo já cadastrado na base da Bahia";
                    break;
                case "253":
                    nome = "UF da placa não informada ou inválida";
                    break;
                case "256":
                    nome = "Não pode ser feito 1º emplacamento para veiculo usado";
                    break;
                case "257":
                    nome = "Não disponível para placa de duas letras de outra UF";
                    break;
                case "258":
                    nome = "Serviço não disponível para Veículo de outra UF";
                    break;
                case "259":
                    nome = "Serviço não permitido para placa de experiência";
                    break;
                case "262":
                    nome = "Renavam informado não pertence ao veículo";
                    break;
                case "263":
                    nome = "CNPJ não preenchido ou inválido";
                    break;
                case "264":
                    nome = "CPF não preenchido ou inválido";
                    break;
                case "265":
                    nome = "Nome não preenchido/inválido";
                    break;
                case "266":
                    nome = "Nome contém caracteres especiais";
                    break;
                case "267":
                    nome = "Endereço não preenchido";
                    break;
                case "268":
                    nome = "Endereço contém caracteres especiais";
                    break;
                case "269":
                    nome = "Número do endereço não informado";
                    break;
                case "270":
                    nome = "Município não preenchido ou invalido";
                    break;
                case "271":
                    nome = "Bairro do endereço não informado";
                    break;
                case "272":
                    nome = "Bairro do endereço contém caracteres especiais";
                    break;
                case "273":
                    nome = "Bairro não cadastrado para o município informado";
                    break;
                case "274":
                    nome = "CEP do endereço não informado";
                    break;
                case "275":
                    nome = "DDD não preenchido ou invalido";
                    break;
                case "276":
                    nome = "Telefone não preenchido ou invalido";
                    break;
                case "277":
                    nome = "UF do domicilio não preenchido/inválido";
                    break;
                case "299":
                    nome = "Município não pertence à UF";
                    break;
                case "303":
                    nome = "Serviço de troca de placa não permitido para placa de três letras";
                    break;
                case "305":
                    nome = "RG não preenchido";
                    break;
                case "306":
                    nome = "RG não deve ser informado";
                    break;
                case "315":
                    nome = "Veiculo não possui intenção de gravame";
                    break;
                case "316":
                    nome = "Veiculo não possui intenção de baixa de gravame";
                    break;
                case "335":
                    nome = "Existe bloqueio de licenciamento";
                    break;
                case "340":
                    nome = "Veículo com Comunicação de Venda";
                    break;
                case "341":
                    nome = "Número do Documento Proprietário/Arrendatário informado diferente do cadastrado";
                    break;
                case "342":
                    nome = "Não é permitida segunda via para placa de 2 (duas) letras.";
                    break;
                case "350":
                    nome = "Informe a senha nova";
                    break;
                case "351":
                    nome = "Confirme a Senha nova";
                    break;
                case "352":
                    nome = "Senha nova não confere";
                    break;
                case "353":
                    nome = "Senha nova não pode ser senha";
                    break;
                case "354":
                    nome = "Senha nova deve ser diferente da atual";
                    break;
                case "357":
                    nome = "Foi solicitado o serviço de mudança de município sem ter sido alterado o município";
                    break;
                case "358":
                    nome = "Foi alterado o município sem a solicitação do serviço correspondente";
                    break;
                case "359":
                    nome = "Para veiculo de outra UF só é permitido alterar característica junto com a Transferência de Jurisdição";
                    break;
                case "360":
                    nome = "Para veiculo de outra UF só é permitido alterar categoria junto com a Transferência de Jurisdição";
                    break;
                case "361":
                    nome = "Para veiculo de outra UF só é permitido alterar Proprietário junto com a Transferência de Jurisdição";
                    break;
                case "364":
                    nome = "Para veiculo de outra UF só é permitido incluir gravame junto com a transferência de jurisdição";
                    break;
                case "400":
                    nome = "Tela não cadastrada";
                    break;
                case "401":
                    nome = "Tela não disponível";
                    break;
                case "402":
                    nome = "Acesso não autorizado";
                    break;
                case "501":
                    nome = "O número da vistoria não pertence ao chassi";
                    break;
                case "503":
                    nome = "Status da Vistoria diferente de A";
                    break;
                case "504":
                    nome = "Não foi preenchido o(s) motivo(s) de Reprovação da Vistoria";
                    break;
                case "506":
                    nome = "Não existe vistoria para ser cancelada";
                    break;
                case "507":
                    nome = "Vistoria Reprovada não pode ser cancelada";
                    break;
                case "509":
                    nome = "É obrigatório informar o item reprovado";
                    break;
                case "518":
                    nome = "Existe vistoria ativa iniciada em outra ECV";
                    break;
                case "519":
                    nome = "Data de emissão não preenchida/inválida";
                    break;
                case "520":
                    nome = "Data de Vistoria não preenchido/inválido";
                    break;
                case "522":
                    nome = "Data da Validade não preenchida/inválida";
                    break;
                case "525":
                    nome = "Data da Validade menor que a data da Vistoria";
                    break;
                case "529":
                    nome = "CNPJ da UGC não cadastrada";
                    break;
                case "534":
                    nome = "ECV não cadastrada no DETRAN";
                    break;
                case "535":
                    nome = "ECV não está Credenciada";
                    break;
                case "537":
                    nome = "Vistoriador não Credenciado";
                    break;
                case "538":
                    nome = "Vistoriador não Cadastrado";
                    break;
                case "539":
                    nome = "Limite de vistorias do vistoriador excedida";
                    break;
                case "541":
                    nome = "Data de emissão maior que data corrente";
                    break;
                case "542":
                    nome = "Data de vistoria maior que data corrente";
                    break;
                case "543":
                    nome = "Existe vistoria ativa iniciada no DETRAN";
                    break;
                case "545":
                    nome = "Serviço de Autorização p/ Trânsito para veículo já cadastrado na base da Bahia";
                    break;
                case "546":
                    nome = "Serviço de transferência de UF para veículo já cadastrado na base da Bahia";
                    break;
                case "547":
                    nome = "Número da vistoria da ECV não informado";
                    break;
                case "548":
                    nome = "Vistoriador pertence à outra ECV";
                    break;
                case "549":
                    nome = "ECV com data da portaria vencida";
                    break;
                case "550":
                    nome = "UGC com data da portaria vencida";
                    break;
                case "557":
                    nome = "Não existe CVC";
                    break;
                case "558":
                    nome = "CVC vencido";
                    break;
                case "559":
                    nome = "Tipo de veículo não cadastrado";
                    break;
                case "600":
                    nome = "No campo Exige CVC informe S ou N";
                    break;
                case "606":
                    nome = "Informe Placa";
                    break;
                case "607":
                    nome = "O veículo não existe na Base Local";
                    break;
                case "816":
                    nome = "Número da Vistoria não informado";
                    break;
                case "817":
                    nome = "Data da Vistoria não informada";
                    break;
                case "818":
                    nome = "Vistoria não cadastrada";
                    break;
                case "819":
                    nome = "Vistoria não é de ECV";
                    break;
                case "820":
                    nome = "Data da Vistoria diferente da informada";
                    break;
                case "821":
                    nome = "Chassi da Vistoria diferente do informado ou Vistoria não pertence ao veículo informado";
                    break;
                case "822":
                    nome = "CNPJ da ECV diferente do informado";
                    break;
                case "823":
                    nome = "Situação da taxa não permite emissão: está paga ou compensada";
                    break;
                case "824":
                    nome = "Tipo de DR inválido";
                    break;
                case "825":
                    nome = "Taxa não encontrada";
                    break;
                case "826":
                    nome = "Esta taxa está CANCELADA";
                    break;
                case "827":
                    nome = "Esta taxa está INATIVA";
                    break;
                case "828":
                    nome = "Esta taxa está BAIXADA POR CONTINGENCIA";
                    break;
                case "829":
                    nome = "Esta taxa está PAGA";
                    break;
                case "830":
                    nome = "Esta taxa está COMPENSADA";
                    break;
                case "831":
                    nome = "Não existe Vistoria cancelada";
                    break;
                case "833":
                    nome = "A cobrança desta Taxa é indevida";
                    break;
                case "840":
                    nome = "Vistoria não pertence ao mesmo Pagador";
                    break;
                case "928":
                    nome = "Código da Autorização Inválido";
                    break;
                case "932":
                    nome = "Fabricante de Estampagem pertence à outra Origem";
                    break;
                case "943":
                    nome = "Campo de Vistoria Lacrada inválido";
                    break;
                case "991":
                    nome = "Origem de Geração da Taxa não Informada ou Inválida";
                    break;
                case "1033":
                    nome = "BIN sem comunicação";
                    break;
                case "1101":
                    nome = "Tipo de Documento Inválido (CPF/CNPJ)";
                    break;
                case "1128":
                    nome = "Indicador de veiculo novo/usado inválido";
                    break;
                case "1130":
                    nome = "Motor do veiculo difere do informado (Chassi X Motor)";
                    break;
                case "1131":
                    nome = "Existe Restrição Administrativa";
                    break;
                case "1134":
                    nome = "Veiculo não existe na Base Estadual";
                    break;
                case "1150":
                    nome = "Usuário/senha inválidos";
                    break;
                case "1151":
                    nome = "Operação não permitida";
                    break;
                case "1152":
                    nome = "Usuário não cadastrado";
                    break;
                case "1153":
                    nome = "A matrícula do usuário está suspensa";
                    break;
                case "1154":
                    nome = "Senha Inválida";
                    break;
                case "1190":
                    nome = "Chassi não preenchido";
                    break;
                case "1195":
                    nome = "Existe protocolo aberto / processo em andamento no Detran";
                    break;
                case "1208":
                    nome = "Existe Restrição Judicial";
                    break;
                case "1209":
                    nome = "Existe ocorrência de Roubo/Furto na Base Estadual";
                    break;
                case "1250":
                    nome = "Nenhum serviço informado";
                    break;
                case "1251":
                    nome = "Serviço deve ser somente primeiro emplacamento";
                    break;
                case "1252":
                    nome = "Veiculo já cadastrado na base da Bahia";
                    break;
                case "1253":
                    nome = "UF da placa não informada ou invalida";
                    break;
                case "1256":
                    nome = "Não pode ser feito primeiro emplacamento para veiculo";
                    break;
                case "1257":
                    nome = "Não disponível para placa de duas letras de outra UF";
                    break;
                case "1259":
                    nome = "Serviço não permitido para placa de experiência/fabricante";
                    break;
                case "1261":
                    nome = "Quantidade de serviços maior que o permitido";
                    break;
                case "1273":
                    nome = "Bairro do endereço do Proprietário/Arrendatário não informado";
                    break;
                case "1312":
                    nome = "Bairro não cadastrado para o município informado";
                    break;
                case "1325":
                    nome = "Código do Renavam não enviado pela BIN";
                    break;
                case "1326":
                    nome = "Duplicidade de Renavam na Base Estadual";
                    break;
                case "1335":
                    nome = "Existe Bloqueio de Licenciamento";
                    break;
                case "1340":
                    nome = "Veiculo com Comunicação de Venda";
                    break;
                case "1341":
                    nome = "Serviço não permitido";
                    break;
                case "1354":
                    nome = "Tipo de Veiculo não cadastrado";
                    break;
                case "1423":
                    nome = "Vistoriador não cadastrado";
                    break;
                case "1463":
                    nome = "VIN informado diferente do cadastrado";
                    break;
                case "1482":
                    nome = "Nome do proprietário contem caracteres especiais";
                    break;
                case "1489":
                    nome = "Endereço deve ser informado";
                    break;
                case "1490":
                    nome = "Endereço contem caracteres especiais";
                    break;
                case "1492":
                    nome = "Numero do Endereço deve ser informado";
                    break;
                case "1509":
                    nome = "Município do Proprietário/Arrendatário não encontrado";
                    break;
                case "1526":
                    nome = "Não existe vistoria para o veiculo";
                    break;
                case "1572":
                    nome = "Renavam informado diferente do cadastrado";
                    break;
                case "1581":
                    nome = "Tem serviço de mudança de município";
                    break;
                case "1582":
                    nome = "Alteração de município sem o serviço correspondente";
                    break;
                case "1592":
                    nome = "Não existe gravame na V091T";
                    break;
                case "1605":
                    nome = "Numero da Vistoria não preenchida";
                    break;
                case "1609":
                    nome = "Vistoria reprovada não pode ser alterada";
                    break;
                case "1642":
                    nome = "CNPJ da UGC não cadastrada";
                    break;
                case "1644":
                    nome = "CNPJ da ECV não preenchido/invalido";
                    break;
                case "1645":
                    nome = "ECV não cadastrada";
                    break;
                case "1668":
                    nome = "Não existe CVC para o veiculo";
                    break;
                case "1669":
                    nome = "CVC está vencido";
                    break;
                case "1675":
                    nome = "No campo Exige CVC informe S ou N";
                    break;
                case "1676":
                    nome = "Exige CVC valido apenas para vistoria Reprovada";
                    break;
                case "1698":
                    nome = "Data da Vistoria Invalida";
                    break;
                case "1699":
                    nome = "Data da Vistoria maior que Data de Hoje";
                    break;
                case "1730":
                    nome = "Matricula do Vistoriador não informada";
                    break;
                case "1732":
                    nome = "Informar o motivo da Reprovação da Vistoria";
                    break;
                case "1752":
                    nome = "Não emite segunda via para placa antiga (duas letras)";
                    break;
                case "1762":
                    nome = "Não existe intenção de desalienação";
                    break;
                case "1784":
                    nome = "Data de Validade não informada/invalida";
                    break;
                case "1841":
                    nome = "Origem da solicitação inválida";
                    break;
                case "1891":
                    nome = "CEP não informado";
                    break;
                case "1892":
                    nome = "DDD/Numero do telefone fixo não informado";
                    break;
                case "1894":
                    nome = "UF do Endereço não informada/invalida";
                    break;
                case "1912":
                    nome = "Nome do Bairro contem caracteres especiais/letras minúsculas";
                    break;
                case "1917":
                    nome = "UF não deve ser informada";
                    break;
                case "1937":
                    nome = "Placa e chassi incompatíveis";
                    break;
                case "1982":
                    nome = "CPF/CNPJ não informado ou invalido";
                    break;
                case "2013":
                    nome = "Substituição de Placa = 1";
                    break;
                case "2036":
                    nome = "Existe vistoria ativa e aprovada";
                    break;
                case "2047":
                    nome = "Nome do Proprietário/Arrendatário DEVE ser informado";
                    break;
                case "2133":
                    nome = "Indicador de Vistoria Lacrada não informado/invalido";
                    break;
                case "2134":
                    nome = "Informe telefone fixo ou celular";
                    break;
                case "2232":
                    nome = "Informe o CNPJ com 14 dígitos";
                    break;
                case "2281":
                    nome = "Tipo de Placa Incompatível com Tipo de Serviço";
                    break;
                case "2310":
                    nome = "Correspondente Financeiro não cadastrado";
                    break;
                case "2311":
                    nome = "Correspondente Financeiro não Credenciado";
                    break;
                case "2358":
                    nome = "RG não deve ser informado para Pessoa Jurídica";
                    break;
                case "2426":
                    nome = "CPF/CNPJ proprietário/arrendatário diferente do cadastrado";
                    break;
                case "2488":
                    nome = "Nosso número não informado ou inválido";
                    break;
                case "2506":
                    nome = "Existe taxa ativa e não paga";
                    break;
                case "2507":
                    nome = "Taxa não encontrada";
                    break;
                case "2510":
                    nome = "Taxa cancelada";
                    break;
                case "2511":
                    nome = "Taxa inativa";
                    break;
                case "2512":
                    nome = "Vistoriador não credenciado";
                    break;
                case "2513":
                    nome = "ECV não credenciada";
                    break;
                case "2514":
                    nome = "Data de emissão não preenchida/inválida";
                    break;
                case "2515":
                    nome = "Data de emissão maior que data corrente";
                    break;
                case "2516":
                    nome = "Data de validade menor que data da vistoria";
                    break;
                case "2517":
                    nome = "Existe bloqueio por falta pagamento taxa";
                    break;
                case "2518":
                    nome = "Taxa não é de vistoria";
                    break;
                case "2519":
                    nome = "Taxa já utilizada";
                    break;
                case "2520":
                    nome = "Existe vistoria ativa iniciada em outra ECV";
                    break;
                case "2521":
                    nome = "Cancelamento da vistoria fora da data";
                    break;
                case "2522":
                    nome = "Status da vistoria inválido";
                    break;
                case "2523":
                    nome = "Taxa pertence a outro veículo";
                    break;
                case "2524":
                    nome = "Existe Taxa ativa";
                    break;
                case "2525":
                    nome = "Informe os itens reprovados";
                    break;
                case "2526":
                    nome = "UF do endereço divergente do município proprietário";
                    break;
                case "2527":
                    nome = "Taxa quitada";
                    break;
                case "2528":
                    nome = "Serviço 009 para veiculo cadastrado na base";
                    break;
                case "2529":
                    nome = "Serviço 023 para veiculo cadastrado na base";
                    break;
                case "2530":
                    nome = "Serviço incompatível para transferência de UF";
                    break;
                case "2531":
                    nome = "Validade do Credenciamento expirada";
                    break;
                case "2532":
                    nome = "Taxa gerada por outra ECV";
                    break;
                case "2639":
                    nome = "Existe vistoria ativa com os mesmos dados informados";
                    break;
            }

            return nome;
        }

        public static string ConvertIdentificacaoVeiculo(int cod)
        {
            if(cod == 1)
            {
                return "N";
            }

            return "U";
        }

        public static string ConvertSituacaoMotor(int cod)
        {
            if (cod == 1)
            {
                return "O";
            }
            else if (cod == 2)
            {
                return "R";
            }
            else if (cod == 3)
            {
                return "S";
            }
            else
            {
                return "V";
            }
        }

        public static string ConvertSituacaoVistoria(int cod)
        {
            if (cod == 1)
            {
                return "A";
            }
            else if (cod == 2)
            {
                return "R";
            }
            else
            {
                return "C";
            }
        }
        
        public static string ConvertExigenciaCVC(int cod)
        {
            if (cod == 1)
            {
                return "N";
            }
            
            return "S";
        }
        
        public static string ConvertSolicitaTrocaPlaca(int cod)
        {
            if (cod == 0)
            {
                return "N";
            }
            
            return cod.ToString();
        }

        public static string ConvertIndicadorOrigem(int cod)
        {
            if (cod == 1)
            {
                return "O";
            }

            return null;
        }
        
        public static string ConvertIndicadorVistoriaLacrada(int cod)
        {
            if (cod == 1)
            {
                return "S";
            }

            return " ";
        }

        public static string ConvertIndicadorOrigemSolicitacao(int cod)
        {
            if (cod == 1)
            {
                return "V";
            }

            return null;
        }

    }
}