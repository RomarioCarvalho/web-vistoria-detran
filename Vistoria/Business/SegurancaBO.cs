﻿using Vistoria.Models;
using Vistoria.Models.Context;
using Vistoria.Models.ViewModel;
using System;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Linq;

namespace Vistoria.Business
{
    public class SegurancaBO
    {
        public static Usuario Autenticar(VistoriaContext db, Usuario model, bool isApp)
        {
            try
            {
                string senha_cripto = Hash.SHA256(model.Senha);
                Usuario usuario = db.Usuario.Ativos().Where(x => x.Login == model.Login).FirstOrDefault();

                if (usuario == null)
                {
                    throw new Exception("Login inválido.");
                }
                else
                {
                    if(usuario.AutenticaAD == true)
                    {
                        PrincipalContext context = new PrincipalContext(ContextType.Domain,
                                                                    ConfigurationManager.AppSettings["AD_DOMINIO"],
                                                                    ConfigurationManager.AppSettings["AD_STRING"]);

                        bool valid = context.ValidateCredentials(model.Login, model.Senha);

                        if (!valid)
                        {
                            throw new Exception("Login ou senha inválida.");
                        }
                    }
                    else
                    {
                        if(usuario.Senha != senha_cripto)
                        {
                            throw new Exception("Senha inválida.");
                        }
                    }
                }

                if(isApp)
                {
                    usuario.Token = Utils.GerarToken();
                    db.SaveChanges();

                    return usuario;
                }
                
                usuario.TokenBiometria = Utils.GerarToken();
                db.SaveChanges();
                
                return null;
                
            }
            catch
            {
                throw;
            }
            
        }

        public static Usuario ValidarToken(VistoriaContext db, string token)
        {
            try
            {
                Usuario usuario = db.Usuario.Ativos().Where(x => x.Token == token).FirstOrDefault();

                if (usuario == null)
                {
                    throw new Exception("Token inválido.");
                }

                return usuario;

            }
            catch
            {
                throw;
            }

        }

        public static void AlterarSenha(VistoriaContext db, AlterarSenhaVM model)
        {
            ValidarSenhaIgual(model);
            ValidaSenhaForte(model.NovaSenha);
            
            Usuario usuario = db.Usuario.Find(SessionHelper.Usuario.Id);

            ValidarSenhaAnterior(usuario, model);

            usuario.Senha = Hash.SHA256(model.NovaSenha);
            db.SaveChanges();
        }

        public static void ValidaSenhaForte(string senha)
        {
            if (!string.IsNullOrEmpty(senha))
            {
                if ((senha.Length < 8) || (!senha.Any(c => char.IsDigit(c))) || (!senha.Any(c => char.IsLower(c))))
                {
                    throw new Exception("Senha deve conter no minimo 8 digitos (Letras/Números)");
                }
            }
        }

        public static void ValidarSenhaAnterior(Usuario temp, AlterarSenhaVM model)
        {
            if (temp.Senha != Hash.SHA256(model.SenhaAtual))
            {
                throw new Exception("Senha atual incorreta");
            }
        }

        public static void ValidarSenhaIgual(AlterarSenhaVM model)
        {
            if (model.NovaSenha != model.ConfirmarSenha)
            {
                throw new Exception("As senhas não conferem");
            }
        }
    }
}