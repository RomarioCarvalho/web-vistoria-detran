﻿using System.ComponentModel;

namespace Vistoria.Enumerators
{
    public enum IndicadorOrigemSolicitacao
    {
        [Description("Vistoria")]
        Vistoria = 1
    }
}