﻿using System.ComponentModel;

namespace Vistoria.Enumerators
{
    public enum StatusVistoria
    {
        Aprovada = 1,
        Reprovada = 2,
        Cancelada = 3,
        Aguardando_Analise = 4,
    }
}