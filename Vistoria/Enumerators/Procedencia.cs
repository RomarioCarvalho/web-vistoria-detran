﻿using System.ComponentModel;

namespace Vistoria.Enumerators
{
    public enum Procedencia
    {
        [Description("Nacional")]
        Nacional = 1,
        [Description("Estrangeiro")]
        Estrangeiro = 2
    }
}