﻿using System.ComponentModel;

namespace Vistoria.Enumerators
{
    public enum ExigenciaCVC
    {
        [Description("Não")]
        Nao = 1,
        [Description("Sim")]
        Sim = 2
    }
}