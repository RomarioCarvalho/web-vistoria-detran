﻿using System.ComponentModel;

namespace Vistoria.Enumerators
{
    public enum SolicitaTrocaPlaca
    {
        [Description("Sem solicitação de placa")]
        N,
        [Description("PLACA DIANTEIRA")]
        PLACA_DIANTEIRA = 1,
        [Description("PLACA TRASEIRA")]
        PLACA_TRASEIRA = 2,
        [Description("PLACA DIANTEIRA + PLACA TRASEIRA")]
        VestigioAdulteracao = 3,
        [Description("SEGUNDA PLACA TRASEIRA")]
        SEGUNDA_PLACA_TRASEIRA = 4,
        [Description("PLACA DIANTEIRA + SEGUNDA PLACA TRASEIRA")]
        PLACA_DIANTEIRA_SEGUNDA_PLACA_TRASEIRA = 5,
        [Description("PLACA TRASEIRA + SEGUNDA PLACA TRASEIRA")]
        PLACA_TRASEIRA_SEGUNDA_PLACA_TRASEIRA = 6,
        [Description("PLACA DIANTEIRA + TRASEIRA + SEGUNDA TRASEIRA")]
        PLACA_DIANTEIRA_TRASEIRA_SEGUNDA_TRASEIRA = 7,
    }
}