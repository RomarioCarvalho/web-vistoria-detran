﻿using System.ComponentModel;

namespace Vistoria.Enumerators
{
    public enum SituacaoChassi
    {
        [Description("Regravado")]
        Regravado = 1,
        [Description("Normal")]
        Normal = 2
    }
}