﻿namespace Vistoria.Enumerators
{
    public enum MessageTipo
    {
        Success = 1,
        Danger = 2,
        Info = 3,
        Warning = 4
    }
}