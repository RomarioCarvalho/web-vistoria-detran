﻿using System.ComponentModel;

namespace Vistoria.Enumerators
{
    public enum SituacaoMotor
    {
        [Description("Original")]
        Original = 1,
        [Description("Regravado")]
        Regravado = 2,
        [Description("Substituído")]
        Substituido = 3,
        [Description("Vestígio de Adulteração")]
        VestigioAdulteracao = 4
    }
}