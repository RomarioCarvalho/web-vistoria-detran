﻿using System.ComponentModel;

namespace Vistoria.Enumerators
{
    public enum IndicadorVistoriaLacrada
    {
        [Description("Nacional")]
        Sim = 1,
        [Description("Ignorar")]
        Ignorar = 2
    }
}