﻿using System.ComponentModel;

namespace Vistoria.Enumerators
{
    public enum SituacaoVistoria
    {
        [Description("Aprovada")]
        Aprovada = 1,
        [Description("Reprovada")]
        Reprovada = 2,
        [Description("Cancelada")]
        Cancelada = 3
    }
}