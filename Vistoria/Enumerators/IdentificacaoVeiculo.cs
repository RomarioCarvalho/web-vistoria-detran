﻿using System.ComponentModel;

namespace Vistoria.Enumerators
{
    public enum IdentificacaoVeiculo
    {
        [Description("Veículo Novo")]
        Novo = 1,
        [Description("Veículo Usado")]
        Usado = 2
    }
}