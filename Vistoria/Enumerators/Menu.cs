﻿namespace Vistoria.Enumerators
{
    public enum Menu
    {
        Lateral = 0,
        Superior = 1,
        Profile = 2
    }
}