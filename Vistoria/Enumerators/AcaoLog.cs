﻿using System.ComponentModel;

namespace Vistoria.Enumerators
{
    public enum AcaoLog
    {
        [Description("Inserir")]
        Inserir = 1,
        [Description("Alterar")]
        Alterar = 2,
        [Description("Excluir")]
        Excluir = 3
    }
}