﻿using System.ComponentModel;

namespace Vistoria.Enumerators
{
    public enum TipoVistoria
    {
        Primeiro_Registro = 12,// LAUDO DE CONFORMIDADE PARA VEÍCULOS AUTOMOTORES
        Transferência_Propriedade = 14,// LAUDO VISTORIA DE CONFORMIDADE PARA VEÍCULOS AUTOMOTORES
        Autorização_Prévia_Transformação = 15,//LAUDO DE CONFORMIDADE PARA VEÍCULOS AUTOMOTORES
        Aceite_CSV = 16,// LAUDO DE ACEITE DO CSV PARA VEÍCULOS AUTOMOTORES
        Baixa_Veículo = 19,
        Autorização_Regravação_Número_Motor_ = 20,
        Laudo_Conformidade = 24,// LAUDO DE CONFORMIDADE PARA VEÍCULOS AUTOMOTORES
        Transferência_Jurisdição = 26,// LAUDO DE VISTORIA PARA VEÍCULOS AUTOMOTORES
        Primeiro_Emplacamento_Ciclomotor = 28,// LAUDO DE CONFORMIDADE PARA VEÍCULOS AUTOMOTORES
        Registro_Número_Motor = 29,
        Registro_Laudo_Outro_Estado = 30,
        Emissão_Laudo_Leilão = 31,// dispacha para 24 e 19
        Vistoria_Autorização_Placa_Lacre = 32,
        Emissão_Laudo_Custodiado = 33// dispacha para 12 - promeiro registo ,14 - transferencia de propriedade, 26-transferencia-jurisdicao , 24 - OutrosServicos de laudo de conformidade
    }
}