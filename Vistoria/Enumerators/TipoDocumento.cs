﻿using System.ComponentModel;

namespace Vistoria.Enumerators
{
    public enum TipoDocumento
    {
        [Description("CPF")]
        CPF = 1,
        [Description("CNPJ")]
        CNPJ = 2
    }
}