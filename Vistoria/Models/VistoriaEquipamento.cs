﻿using Vistoria.Enumerators;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vistoria.Models
{
    public class VistoriaEquipamento
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Display(Name = "Vistoria")]
        [ForeignKey("VistoriaEcv")]
        public long VistoriaId { get; set; }

        [Display(Name = "Equipamento")]
        [ForeignKey("Equipamento")]
        public long EquipamentoId { get; set; }

        public bool Ativo { get; set; }

        public virtual VistoriaEcv VistoriaEcv { get; set; }
        public virtual Equipamento Equipamento { get; set; }

    }
}