﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vistoria.Models
{
    public class LogErro
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Display(Name = "Ação")]
        public string Acao { get; set; }
        public string StackTrace { get; set; }
        public string Message { get; set; }
        public string ContentApiString { get; set; }
        public DateTime Data { get; set; }
        
    }
}