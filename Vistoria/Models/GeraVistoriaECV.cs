﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vistoria.Models
{
    [Serializable]
    public class GeraVistoriaECV
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string codigoOperacao { get; set; }
        public long matriculaAtendente { get; set; }
        public string origemSolicitacao { get; set; }
        public string estacaoSolicitacao { get; set; }
        public string indicaNovoUsado { get; set; }
        public string ufVeiculo { get; set; }
        public string placaVeiculo { get; set; }
        public string solicTrocaPlaca { get; set; }
        public string chassi { get; set; }
        public string numeroMotor { get; set; }
        public string vinRegravado { get; set; }
        public string statusMotor { get; set; }
        public long codigoRenavam { get; set; }
        public long cpfCnpjProp { get; set; }
        public long tipoDocumento { get; set; }
        public string rg { get; set; }
        public string nomeProprietario { get; set; }
        public string endereco { get; set; }
        public string numeroEndereco { get; set; }
        public string compleImovelProp { get; set; }
        public string nomeBairro { get; set; }
        public string municipioProp { get; set; }
        public string UfProprietario { get; set; }
        public long cepImovelProp { get; set; }
        public long ddd { get; set; }
        public long numeroFoneProp { get; set; }
        public long nrVistoriaECV { get; set; }
        public long dataVistoria { get; set; }
        public long horaVistoria { get; set; }
        public long matVistoriador { get; set; }
        public string cnpjECV { get; set; }
        public string statusVistoria { get; set; }
        public long dataEmissao { get; set; }
        public long horaEmissao { get; set; }
        public long dataValidade { get; set; }
        public long codigoServico01 { get; set; }
        public long codigoServico02 { get; set; }
        public long codigoServico03 { get; set; }
        public long codigoServico04 { get; set; }
        public long codigoServico05 { get; set; }
        public long codigoServico06 { get; set; }
        public long codigoServico07 { get; set; }
        public string vistoriaLacrada { get; set; }
        public string codigoReprovado { get; set; }
        public long nossoNumero { get; set; }
        public long retornoCritica { get; set; }
        public long numeroVistoria { get; set; }
        public string placaAtribuida { get; set; }
        public long numeroProtocolo { get; set; }
        public DateTime DataHora { get; set; }

    }
}