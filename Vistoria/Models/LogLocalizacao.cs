﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Vistoria.Models
{
    public class LogLocalizacao
    {
        [Key]
        public long Id { get; set; }
        
        [Required]
        [ForeignKey("VistoriaEcv")]
        public long VistoriaId { get; set; }
        
        [Required]
        public string Latitude { get; set; }
        
        [Required]
        public string Longitude { get; set; }
        
        [Required]
        [ForeignKey("Usuario")]
        public long UsuarioId { get; set; }
        
        [Required]
        [ForeignKey("TipoLogLocalizacao")]
        public long TipoLogLocalizacaoId { get; set; }        
        
        [Required]
        public DateTime DataAcao { get; set; }

        public virtual TipoLogLocalizacao TipoLogLocalizacao { get; set; }
        public virtual VistoriaEcv VistoriaEcv { get; set; }
        public virtual Usuario Usuario { get; set; }


    }
}