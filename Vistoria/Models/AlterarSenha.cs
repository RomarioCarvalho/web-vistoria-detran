﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vistoria.Models
{
    [Serializable]
    public class AlterarSenha
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string codigoOperacao { get; set; }
        public string novaSenha { get; set; }
        public string senhaConfirmada { get; set; }
        public int codigoRetExec { get; set; }

    }
}