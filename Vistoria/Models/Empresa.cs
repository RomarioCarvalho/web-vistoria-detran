﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vistoria.Models
{
    [Serializable]
    public class Empresa : AbstractModel
    {
        [Display(Name = "Nome")]
        [Required(ErrorMessage = "Informe o {0}")]
        public string Nome { get; set; }
        
    }
}