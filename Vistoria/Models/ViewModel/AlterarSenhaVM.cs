﻿using System.ComponentModel.DataAnnotations;

namespace Vistoria.Models.ViewModel
{
    public class AlterarSenhaVM
    {
        [Display(Name = "Senha atual")]
        [Required(ErrorMessage = "Informe a senha atual", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string SenhaAtual { get; set; }

        [Display(Name = "Nova senha")]
        [Required(ErrorMessage = "Informe a nova senha", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string NovaSenha { get; set; }

        [Display(Name = "Confirmar senha")]
        [Required(ErrorMessage = "Confirme a nova senha", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string ConfirmarSenha { get; set; }
    }
}