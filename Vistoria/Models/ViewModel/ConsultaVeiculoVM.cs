﻿namespace Vistoria.Models.ViewModel
{
    public class ConsultaVeiculoVM
    {
        public long Id { get; set; }
        public string ParamChassi { get; set; }
        public string ParamPlaca { get; set; }
        public long ParamRenavam { get; set; }
        public long TaxaId { get; set; }
        public long TipoVeiculoId { get; set; }
        public long Km { get; set; }
        public long AnoFabricacao { get; set; }
        public long AnoModelo { get; set; }
        public long CorId { get; set; }
        public long CarroceriaId { get; set; }
        public long EspecieId { get; set; }
        public long CategoriaId { get; set; }
        public long CombustivelId { get; set; }
        public string StatusMotor { get; set; }
        public string IndicaNovoUsado { get; set; }
        public long NumeroEixos { get; set; }
        public long Potencia { get; set; }
        public long Cilindrada { get; set; }
        public decimal CapacidadeCarga { get; set; }
        public long Cmt { get; set; }
        public long Pbt { get; set; }
        public long NumeroHodometro { get; set; }
        public long CapacidadePassag { get; set; }
        public string Lacre { get; set; }
        public string VinRegravado { get; set; }
        public string NumeroMotor { get; set; }
        public string Apontamento { get; set; }

    }
}