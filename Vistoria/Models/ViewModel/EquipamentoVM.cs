﻿namespace Vistoria.Models.ViewModel
{
    public class EquipamentoVM
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public string NomeAmigavel { get; set; }
        public string Obrigatorio { get; set; }
        public bool Checked { get; set; }

    }
}