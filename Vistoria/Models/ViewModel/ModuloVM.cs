﻿using System;

namespace Vistoria.Models.ViewModel
{
    [Serializable]
    public class ModuloVM
    {
        public long Id { get; set; }
        public bool Ativo { get; set; }

        public string Nome { get; set; }

        public string Path { get; set; }
    }
}