﻿namespace Vistoria.Models.ViewModel
{
    public class MenuVM
    {
        public string Nome { get; set; }
        public string Projeto { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Icone { get; set; }
    }
}