﻿namespace Vistoria.Models.ViewModel
{
    public class CaracteristicaVeiculoVM
    {
        public long anoFabricacao { get; set; }
        public long anoModelo { get; set; }
        public string apontamento { get; set; }
        public decimal carga { get; set; }
        public string carroceria { get; set; }
        public string categoriaVeiculo { get; set; }
        public string chassi { get; set; }
        public string placa { get; set; }
        public long cilindrada { get; set; }
        public long cmt { get; set; }
        public long numeroHodometro { get; set; }
        public string statusMotor { get; set; }
        public string indicaNovoUsado { get; set; }
        public string combustivel { get; set; }
        public string cor { get; set; }
        public long eixos { get; set; }
        public string especie { get; set; }
        public long km { get; set; }
        public string lacre { get; set; }
        public string marcaModelo { get; set; }
        public string numeroMotor { get; set; }
        public long passageiro { get; set; }
        public long pbt { get; set; }
        public long potencia { get; set; }
        public string tipoVeiculo { get; set; }
        public string vinRegravado { get; set; }

    }
}