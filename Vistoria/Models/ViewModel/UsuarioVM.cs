﻿using System.Collections.Generic;

namespace Vistoria.Models.ViewModel
{
    public class UsuarioVM
    {
        public long Id { get; set; }
        public bool Ativo { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public long MatriculaAtendente { get; set; }
        public string Biometria { get; set; }
        public string TokenBiometria { get; set; }
        public virtual List<GrupoVM> ListGrupo { get; set; }
        public virtual List<ModuloVM> ListModulo { get; set; }
    }
}