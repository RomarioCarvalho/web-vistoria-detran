﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;

namespace Vistoria.Models.ViewModel
{
    public class ProjetoVM
    {
        public string Nome { get; set; }
        public Enumerators.Menu Posicao { get; set; }
        public string Icone { get; set; }
        public List<MenuVM> ListMenu { get; set; }

        public static List<ProjetoVM> ListProjeto()
        {
            return JsonConvert.DeserializeObject<List<ProjetoVM>>(File.ReadAllText(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["URL_MENU"])));
        }
    }
}