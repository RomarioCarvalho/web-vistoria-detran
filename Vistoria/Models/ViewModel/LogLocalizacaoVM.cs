﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Vistoria.Models.ViewModel
{
    public class LogLocalizacaoVM
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public long UsuarioId { get; set; }
        public long TipoLogLocalizacaoId { get; set; }        
        public DateTime DataAcao { get; set; }
    }
}