﻿using Vistoria.Enumerators;
using System;

namespace Vistoria.Models.ViewModel
{
    [Serializable]
    public class MessageVM
    {
        public MessageTipo MessageTipo { get; set; }
        public string Titulo { get; set; }
        public string Texto { get; set; }

        public string GetIcon()
        {
            switch (MessageTipo)
            {
                case MessageTipo.Success:
                    return "fa-check";
                case MessageTipo.Danger:
                    return "fa-ban";
                default:
                    return "fa-"+MessageTipo.ToString().ToLower();
            }
        }
    }
}