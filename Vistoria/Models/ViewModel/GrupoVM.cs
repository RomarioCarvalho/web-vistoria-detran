﻿using System;
using System.Collections.Generic;

namespace Vistoria.Models.ViewModel
{
    [Serializable]
    public class GrupoVM
    {
        public long Id { get; set; }
        public bool Ativo { get; set; }
        public string Nome { get; set; }

        public virtual List<ModuloVM> ListModulo { get; set; }

    }
}