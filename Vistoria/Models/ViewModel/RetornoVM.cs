﻿using Vistoria.Enumerators;
using System;

namespace Vistoria.Models.ViewModel
{
    public class RetornoVM
    {
        public string Message { get; set; }
        public int Code { get; set; }
        public bool Status { get; set; }
        public Object Data { get; set; }

        public RetornoVM()
        {

        }

    }
}