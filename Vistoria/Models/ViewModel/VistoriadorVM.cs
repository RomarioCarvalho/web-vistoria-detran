﻿namespace Vistoria.Models.ViewModel
{
    public class VistoriadorVM
    {
        public long Id { get; set; }
        public string Login { get; set; }
        public string Nome { get; set; }
        public string Token { get; set; }

    }
}