﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vistoria.Models
{
    [Serializable]
    public class GeraDrTaxaVistoria
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string codigoOperacao { get; set; }
        public long matriculaAtendente { get; set; }
        public string origemSolicitacao { get; set; }
        public string estacaoSolicitacao { get; set; }
        public string cnpjPagador { get; set; }
        public string chassiVeiculo { get; set; }
        public long nossoNumero { get; set; }
        public long retornoCritica { get; set; }
        public long codigoTaxa { get; set; }
        public decimal valorDR { get; set; }
        public long dataVencimento { get; set; }
        public string codigoBarras { get; set; }
        public string codBarrasEdit { get; set; }

        public DateTime DataHora { get; set; }

    }
}