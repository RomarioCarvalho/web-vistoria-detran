﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vistoria.Models
{
    [Serializable]
    public class FotoCamera : AbstractModel
    {
        [Required]
        [ForeignKey("VistoriaEcv")]
        public long VistoriaId { get; set; }

        [Display(Name = "Nome")]
        [Required(ErrorMessage = "Informe o {0}")]
        public string NomeArquivo { get; set; }

        public DateTime DataHora { get; set; }

        public virtual VistoriaEcv VistoriaEcv { get; set; }

    }
}
