﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vistoria.Models
{
    [Serializable]
    [Table("Vistoria")]
    public class VistoriaEcv : AbstractModel
    {
        [Display(Name = "Cód. Operação")]
        public string codigoOperacao { get; set; }
        [Display(Name = "Cód. Atendente")]
        public long matriculaAtendente { get; set; }
        public string origemSolicitacao { get; set; }
        public string estacaoSolicitacao { get; set; }
        [Display(Name = "Indic. Novo/Usado")]
        public string indicaNovoUsado { get; set; }
        
        [NotMapped]
        public string Hash { get; set; }
        public string ufVeiculo { get; set; }
        [Display(Name = "Placa")]
        public string placaVeiculo { get; set; }
        
        [Display(Name = "Chassi")]
        public string chassi { get; set; }
        [Display(Name = "Km")]
        public long km { get; set; }
        [Display(Name = "Marca/Modelo")]
        public string marcaModelo { get; set; }
        [Display(Name = "Nome")]
        public string nomeProprietario { get; set; }
        [Display(Name = "Ano Fabricação")]
        public long anoFabricacao { get; set; }
        [Display(Name = "Ano Modelo")]
        public long anoModelo { get; set; }
        
        
        [Display(Name = "Eixos")]
        public long numeroEixos { get; set; }
        [Display(Name = "Potência")]
        public long potencia { get; set; }
        [Display(Name = "Cilindrada")]
        public long cilindrada { get; set; }
        [Display(Name = "Capac. Carga")]
        public decimal capacidadeCarga { get; set; }
        [Display(Name = "CMT")]
        public long cmt { get; set; }
        [Display(Name = "PBT")]
        public long pbt { get; set; }
        [Display(Name = "Capac. Passageiros")]
        public long capacidadePassag { get; set; }
        [Display(Name = "Lacre")]
        public string lacre { get; set; }
        [Display(Name = "Número Motor")]
        public string numeroMotor { get; set; }
        [Display(Name = "Apontamento")]
        public string apontamento { get; set; }
        [Display(Name = "VIN Regravado")]
        public string vinRegravado { get; set; }
        [Display(Name = "Status do Motor")]
        public string statusMotor { get; set; }
        [Display(Name = "Renavam")]
        public long codigoRenavam { get; set; }
        [Display(Name = "CPF/CNPJ")]
        public string cpfCnpjProp { get; set; }

        [Display(Name = "RG")]
        public string rg { get; set; }
        [Display(Name = "Endereço")]
        public string endereco { get; set; }
        [Display(Name = "Num. Endereço")]
        public string numeroEndereco { get; set; }
        [Display(Name = "Complemento End.")]
        public string compleImovelProp { get; set; }

        [Display(Name = "Bairro")]
        public string nomeBairro { get; set; }
        
        [Display(Name = "UF")]
        public string ufProprietario { get; set; }
        [Display(Name = "CEP")]
        public long cepImovelProp { get; set; }
        
        [Display(Name = "Telefone")]
        public string numeroFoneProp { get; set; }
        
        [Display(Name = "Data Vistoria")]
        public DateTime? dataVistoria { get; set; }
        
        [Display(Name = "Número do Hodômetro")]
        public long? numeroHodometro { get; set; }
        
        [Display(Name = "Data Emissão")]
        public DateTime? dataEmissao { get; set; }
        [Display(Name = "Data Validade")]
        public DateTime? dataValidade { get; set; }
        
        [Display(Name = "Vistoria Lacrada")]
        public string vistoriaLacrada { get; set; }
        [Display(Name = "Código Reprovado")]
        public long codigoReprovado { get; set; }
        [Display(Name = "Nosso Número")]
        public long nossoNumero { get; set; }
        public long retornoCritica { get; set; }
        [Display(Name = "Nº Vistoria")]
        public long numeroVistoria { get; set; }
        [Display(Name = "OBS Vistoriador")]
        public string observacoesVistoriador { get; set; }
        [Display(Name = "Placa Acordo")]
        public string placa_acordo { get; set; }
        [Display(Name = "Placa Atribuida")]
        public string placaAtribuida { get; set; }
        [Display(Name = "Nº Protocolo")]
        public long numeroProtocolo { get; set; }
        [Display(Name = "Tipo de Serviço")]
        public string tipoServicoWeb { get; set; }

        [ForeignKey("ConsultaVeiculo")]
        public long consultaVeiculoId { get; set; }
        public virtual ConsultaVeiculo ConsultaVeiculo { get; set; }

        [ForeignKey("GeraDrTaxaVistoria")]
        public long? geraDrTaxaVistoriaId { get; set; }
        public virtual GeraDrTaxaVistoria GeraDrTaxaVistoria { get; set; }
        
        [ForeignKey("GeraVistoriaECV")]
        public long? geraVistoriaECVId { get; set; }
        public virtual GeraVistoriaECV GeraVistoriaECV { get; set; }

        [ForeignKey("Usuario")]
        [Display(Name = "Vistoriador")]
        public long vistoriadorId { get; set; }
        public virtual Usuario Usuario { get; set; }

        [ForeignKey("Sitio")]
        [Display(Name = "Sítio")]
        public long sitioId { get; set; }
        public virtual Sitio Sitio { get; set; }

        [ForeignKey("StatusVistoria")]
        [Display(Name = "Status da Vistoria")]
        public long? statusVistoriaId { get; set; }
        public virtual StatusVistoria StatusVistoria { get; set; }


        [ForeignKey("Cor")]
        [Display(Name = "Cor")]
        public long corId { get; set; }
        public virtual Cor Cor { get; set; }


        [Display(Name = "Carroceria")]
        [ForeignKey("Carroceria")]
        public long carroceriaId { get; set; }
        public virtual Carroceria Carroceria { get; set; }


        [Display(Name = "Espécie")]
        [ForeignKey("Especie")]
        public long especieId { get; set; }
        public virtual Especie Especie { get; set; }


        [Display(Name = "Combustivel")]
        [ForeignKey("Combustivel")]
        public long combustivelId { get; set; }
        public virtual Combustivel Combustivel { get; set; }


        [Display(Name = "Tipo Veículo")]
        [ForeignKey("TipoVeiculo")]
        public long tipoVeiculoId { get; set; }
        public virtual TipoVeiculo TipoVeiculo { get; set; }

        [Display(Name = "Categoria")]
        [ForeignKey("Categoria")]
        public long categoriaId { get; set; }
        public virtual Categoria Categoria { get; set; }


        [Display(Name = "Serviço de Placa")]
        [ForeignKey("TrocaPlaca")]
        public long solicTrocaPlacaId { get; set; }
        public virtual TrocaPlaca TrocaPlaca { get; set; }

        [Display(Name = "Município")]
        [ForeignKey("Municipio")]
        public long? municipioIdProp { get; set; }
        public virtual Municipio Municipio { get; set; }


        [ForeignKey("TipoVistoria")]
        [Display(Name = "Tipo de Vistoria")]
        public long? tipoVistoriaId { get; set; }
        public virtual TipoVistoria TipoVistoria { get; set; }

        [ForeignKey("TipoVistoria2")]
        [Display(Name = "Serviço Complementar 2")]
        public long? tipoVistoriaId2 { get; set; }
        public virtual TipoVistoria TipoVistoria2 { get; set; }


        [ForeignKey("TipoVistoria3")]
        [Display(Name = "Serviço Complementar 3")]
        public long? tipoVistoriaId3 { get; set; }
        public virtual TipoVistoria TipoVistoria3 { get; set; }

        [ForeignKey("TipoVistoria4")]
        [Display(Name = "Serviço Complementar 4")]
        public long? tipoVistoriaId4 { get; set; }
        public virtual TipoVistoria TipoVistoria4 { get; set; }

        [ForeignKey("TipoVistoria5")]
        [Display(Name = "Serviço Complementar 5")]
        public long? tipoVistoriaId5 { get; set; }
        public virtual TipoVistoria TipoVistoria5 { get; set; }

        [ForeignKey("TipoVistoria6")]
        [Display(Name = "Serviço Complementar 6")]
        public long? tipoVistoriaId6 { get; set; }
        public virtual TipoVistoria TipoVistoria6 { get; set; }

        [ForeignKey("TipoVistoria7")]
        [Display(Name = "Serviço Complementar 7")]
        public long? tipoVistoriaId7 { get; set; }
        public virtual TipoVistoria TipoVistoria7 { get; set; }

    }
}