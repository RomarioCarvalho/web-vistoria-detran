﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vistoria.Models
{
    public class Usuario : AbstractModel
    {
        [Display(Name = "Nome")]
        [Required(ErrorMessage = "Informe o {0}")]
        public string Nome { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Informe o {0}")]
        public string Email { get; set; }

        [Display(Name = "Login")]
        [Required(ErrorMessage = "Informe o {0}")]
        public string Login { get; set; }

        [Display(Name = "Senha")]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        [Display(Name = "Matrícula do Atendente")]
        [Required(ErrorMessage = "Informe a {0}")]
        public long MatriculaAtendente { get; set; }

        [Display(Name = "Autenticação no AD?")]
        [Required(ErrorMessage = "Informe {0}")]
        public bool AutenticaAD { get; set; }

        public string Token { get; set; }

        public string Biometria { get; set; }
        public string TokenBiometria { get; set; }
        public string HashBiometria { get; set; }

        [Display(Name = "Sitio")]
        [ForeignKey("Sitio")]
        public long SitioId { get; set; }

        public virtual Sitio Sitio { get; set; }

        public virtual List<Grupo> ListGrupo { get; set; }

        [JsonIgnore]
        [NotMapped]
        public List<Modulo> ListModulo;

        [Display(Name = "Grupos de Acesso")]
        [NotMapped]
        public List<int> GruposId { get; set; }
    }
}