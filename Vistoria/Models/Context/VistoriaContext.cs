using Newtonsoft.Json.Linq;
using Vistoria.Business;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Vistoria.Enumerators;

namespace Vistoria.Models.Context
{
    public class VistoriaContext : DbContext
    {
        public VistoriaContext() : base("name=VistoriaContext")
        {
        }
        
        public DbSet<Log> Log { get; set; }
        public DbSet<LogErro> LogErro { get; set; }
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Grupo> Grupo { get; set; }
        public DbSet<UsuarioGrupo> UsuarioGrupo { get; set; }
        public DbSet<Modulo> Modulo { get; set; }
        public DbSet<Empresa> Empresa { get; set; }
        public DbSet<Sitio> Sitio { get; set; }
        public DbSet<AlterarSenha> AlterarSenha { get; set; }
        public DbSet<ConsultaVeiculo> ConsultaVeiculo { get; set; }
        public DbSet<GeraDrTaxaVistoria> GeraDrTaxaVistoria { get; set; }
        public DbSet<GeraVistoriaECV> GeraVistoriaECV { get; set; }
        public DbSet<VistoriaEcv> VistoriaEcv { get; set; }
        public DbSet<Equipamento> Equipamento { get; set; }
        public DbSet<TipoVeiculo> TipoVeiculo { get; set; }
        public DbSet<VistoriaEquipamento> VistoriaEquipamento { get; set; }
        public DbSet<StatusVistoria> StatusVistoria { get; set; }
        public DbSet<TipoVistoria> TipoVistoria { get; set; }
        public DbSet<Cor> Cor { get; set; }
        public DbSet<Especie> Especie { get; set; }
        public DbSet<Combustivel> Combustivel { get; set; }
        public DbSet<Carroceria> Carroceria { get; set; }
        public DbSet<Categoria> Categoria { get; set; }
        public DbSet<TrocaPlaca> TrocaPlaca { get; set; }
        public DbSet<Municipio> Municipio { get; set; }
        public DbSet<TipoLogLocalizacao> TipoLogLocalizacao { get; set; }
        public DbSet<LogLocalizacao> LogLocalizacao { get; set; }
        public DbSet<FotoCamera> FotoCamera { get; set; }


        public override int SaveChanges()
        {
            OnBeforeSaving();
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync()
        {
            OnBeforeSaving();
            return await base.SaveChangesAsync();
        }

        private void OnBeforeSaving()
        {
            foreach (var entry in ChangeTracker.Entries<AbstractModel>())
            {
                string Base = entry.Entity.GetType().BaseType.Name;
                if (Base == "AbstractModel")
                {
                    Base = entry.Entity.ToString().Replace("Vistoria.Models.", "");
                }
                Log l = new Log {
                    Data = DateTime.Now,
                    Type = Base,
                    TypeId = entry.Entity.Id,
                    UsuarioId = HttpContext.Current != null && 
                                HttpContext.Current.Session != null && 
                                HttpContext.Current.Session["Usuario"] != null ? SessionHelper.Usuario?.Id : null,
                    Value =

                    Regex.Replace(
                        JObject.FromObject(entry.Entity, new Newtonsoft.Json.JsonSerializer {
                            MaxDepth = 1,
                            NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore
                        }).ToString(),
                        "[ \t\n\r]+", " ",RegexOptions.Multiline | RegexOptions.IgnoreCase
                    )
                };
                switch (entry.State)
                {
                    case EntityState.Added:
                        l.Acao = AcaoLog.Inserir;
                        entry.Entity.Ativo = true;
                        break;
                    case EntityState.Modified:
                        l.Acao = AcaoLog.Alterar;
                        break;
                    case EntityState.Deleted:
                        l.Acao = AcaoLog.Excluir;
                        entry.State = EntityState.Modified;
                        entry.Entity.Ativo = false;
                        break;
                }
                if (l.Acao != 0) {
                    Log.Add(l);
                }
            }

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Usuario>()
                .HasMany(p => p.ListGrupo)
                .WithMany(e => e.ListUsuario)
                .Map(pe =>
                    pe.ToTable("GrupoUsuario")
                        .MapLeftKey("UsuarioId")
                        .MapRightKey("GrupoId")
                );


            modelBuilder.Entity<Grupo>()
                 .HasMany(p => p.ListModulo)
                 .WithMany(e => e.ListGrupo)
                 .Map(pe =>
                     pe.ToTable("GrupoModulo")
                         .MapLeftKey("GrupoId")
                         .MapRightKey("ModuloId")
                 );


            modelBuilder.Entity<Sitio>().HasRequired(obj => obj.Empresa).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<Usuario>().HasRequired(obj => obj.Sitio).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<Equipamento>().HasRequired(obj => obj.TipoVeiculo).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEquipamento>().HasRequired(obj => obj.VistoriaEcv).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEquipamento>().HasRequired(obj => obj.Equipamento).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasRequired(obj => obj.ConsultaVeiculo).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasRequired(obj => obj.Usuario).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasRequired(obj => obj.Sitio).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasRequired(obj => obj.StatusVistoria).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasRequired(obj => obj.TipoVistoria).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasRequired(obj => obj.TipoVeiculo).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasRequired(obj => obj.Cor).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasRequired(obj => obj.Carroceria).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasRequired(obj => obj.Combustivel).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasRequired(obj => obj.Especie).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasRequired(obj => obj.Categoria).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasRequired(obj => obj.TrocaPlaca).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasRequired(obj => obj.Municipio).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasOptional(obj => obj.TipoVistoria2).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasOptional(obj => obj.TipoVistoria3).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasOptional(obj => obj.TipoVistoria4).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasOptional(obj => obj.TipoVistoria5).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasOptional(obj => obj.TipoVistoria6).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasOptional(obj => obj.TipoVistoria7).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasOptional(obj => obj.GeraDrTaxaVistoria).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<VistoriaEcv>().HasOptional(obj => obj.GeraVistoriaECV).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<ConsultaVeiculo>().HasRequired(obj => obj.Sitio).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<LogLocalizacao>().HasRequired(obj => obj.Usuario).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<LogLocalizacao>().HasRequired(obj => obj.TipoLogLocalizacao).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<UsuarioGrupo>().HasRequired(obj => obj.Usuario).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<UsuarioGrupo>().HasRequired(obj => obj.Grupo).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<FotoCamera>().HasRequired(obj => obj.VistoriaEcv).WithMany().WillCascadeOnDelete(false);
            //modelBuilder.Entity<EventoSebrae>().HasOptional(obj => obj.Instrumento).WithMany().WillCascadeOnDelete(false);


            base.OnModelCreating(modelBuilder);
        }
    }
}