using System.Text;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Vistoria.Models.Report
{
    [DataContract]
    public class ProprietarioVo
    {

        [DataMember(Name = "nome", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "nome")]
        public string Nome { get; set; }

        [DataMember(Name = "cpfCnpj", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "cpfCnpj")]
        public string CpfCnpj { get; set; }

        [DataMember(Name = "cep", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "cep")]
        public long Cep { get; set; }

        [DataMember(Name = "endereco", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "endereco")]
        public string Endereco { get; set; }

        [DataMember(Name = "municipio", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "municipio")]
        public string Municipio { get; set; }

        [DataMember(Name = "uf", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uf")]
        public string Uf { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ProprietarioVo {\n");
            sb.Append("  Nome: ").Append(Nome).Append("\n");
            sb.Append("  CpfCnpj: ").Append(CpfCnpj).Append("\n");
            sb.Append("  Cep: ").Append(Cep).Append("\n");
            sb.Append("  Endereco: ").Append(Endereco).Append("\n");
            sb.Append("  Municipio: ").Append(Municipio).Append("\n");
            sb.Append("  Uf: ").Append(Uf).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

    }
}
