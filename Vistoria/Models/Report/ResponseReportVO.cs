using System.Text;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Vistoria.Models.Report
{
    [DataContract]
    public class ResponseReportVO
    {

        [DataMember(Name = "id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [DataMember(Name = "status", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [DataMember(Name = "detalhes", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "detalhes")]
        public string Detalhes { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ResponseReportVO {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  Status: ").Append(Status).Append("\n");
            sb.Append("  Detalhes: ").Append(Detalhes).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

    }
}
