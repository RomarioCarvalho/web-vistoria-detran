using System.Text;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Vistoria.Models.Report
{
    [DataContract]
    public class VeiculoVo
    {

        [DataMember(Name = "placa", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "placa")]
        public string Placa { get; set; }

        [DataMember(Name = "marcaModelo", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "marcaModelo")]
        public string MarcaModelo { get; set; }

        [DataMember(Name = "tipo", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "tipo")]
        public string Tipo { get; set; }

        [DataMember(Name = "anoFabricacao", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "anoFabricacao")]
        public long? AnoFabricacao { get; set; }

        [DataMember(Name = "anoModelo", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "anoModelo")]
        public long? AnoModelo { get; set; }

        [DataMember(Name = "cor", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "cor")]
        public string Cor { get; set; }

        [DataMember(Name = "tipoCarroceria", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "tipoCarroceria")]
        public string TipoCarroceria { get; set; }

        [DataMember(Name = "especie", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "especie")]
        public string Especie { get; set; }

        [DataMember(Name = "categoria", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "categoria")]
        public string Categoria { get; set; }

        [DataMember(Name = "combustivel", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "combustivel")]
        public string Combustivel { get; set; }

        [DataMember(Name = "eixos", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "eixos")]
        public long Eixos { get; set; }

        [DataMember(Name = "potencia", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "potencia")]
        public long Potencia { get; set; }

        [DataMember(Name = "cilindrada", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "cilindrada")]
        public long Cilindrada { get; set; }

        [DataMember(Name = "capCarga", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "capCarga")]
        public decimal CapCarga { get; set; }

        [DataMember(Name = "cmt", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "cmt")]
        public long Cmt { get; set; }

        [DataMember(Name = "pbt", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "pbt")]
        public long Pbt { get; set; }

        [DataMember(Name = "capPass", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "capPass")]
        public long CapPass { get; set; }

        [DataMember(Name = "lacre", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "lacre")]
        public string Lacre { get; set; }

        [DataMember(Name = "km", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "km")]
        public double? Km { get; set; }

        [DataMember(Name = "chassi", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "chassi")]
        public string Chassi { get; set; }

        [DataMember(Name = "vinRegravado", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "vinRegravado")]
        public string VinRegravado { get; set; }

        [DataMember(Name = "motor", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "motor")]
        public string Motor { get; set; }

        [DataMember(Name = "apontamento", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "apontamento")]
        public string Apontamento { get; set; }

        [DataMember(Name = "fotoTraseira", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "fotoTraseira")]
        public string FotoTraseira { get; set; }

        [DataMember(Name = "fotoChassi", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "fotoChassi")]
        public string FotoChassi { get; set; }

        [DataMember(Name = "fotoMotor", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "fotoMotor")]
        public string FotoMotor { get; set; }

        [DataMember(Name = "fotoHodometro", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "fotoHodometro")]
        public string FotoHodometro { get; set; }
        
        [DataMember(Name = "dataFotoTraseira", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "dataFotoTraseira")]
        public string DataFotoTraseira { get; set; }

        [DataMember(Name = "dataFotoChassi", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "dataFotoChassi")]
        public string DataFotoChassi { get; set; }

        [DataMember(Name = "dataFotoMotor", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "dataFotoMotor")]
        public string DataFotoMotor { get; set; }

        [DataMember(Name = "dataFotoHodometro", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "dataFotoHodometro")]
        public string DataFotoHodometro { get; set; }
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class VeiculoVo {\n");
            sb.Append("  Placa: ").Append(Placa).Append("\n");
            sb.Append("  MarcaModelo: ").Append(MarcaModelo).Append("\n");
            sb.Append("  Tipo: ").Append(Tipo).Append("\n");
            sb.Append("  AnoFabricacao: ").Append(AnoFabricacao).Append("\n");
            sb.Append("  AnoModelo: ").Append(AnoModelo).Append("\n");
            sb.Append("  Cor: ").Append(Cor).Append("\n");
            sb.Append("  TipoCarroceria: ").Append(TipoCarroceria).Append("\n");
            sb.Append("  Especie: ").Append(Especie).Append("\n");
            sb.Append("  Categoria: ").Append(Categoria).Append("\n");
            sb.Append("  Combustivel: ").Append(Combustivel).Append("\n");
            sb.Append("  Eixos: ").Append(Eixos).Append("\n");
            sb.Append("  Potencia: ").Append(Potencia).Append("\n");
            sb.Append("  Cilindrada: ").Append(Cilindrada).Append("\n");
            sb.Append("  CapCarga: ").Append(CapCarga).Append("\n");
            sb.Append("  Cmt: ").Append(Cmt).Append("\n");
            sb.Append("  Pbt: ").Append(Pbt).Append("\n");
            sb.Append("  CapPass: ").Append(CapPass).Append("\n");
            sb.Append("  Lacre: ").Append(Lacre).Append("\n");
            sb.Append("  Km: ").Append(Km).Append("\n");
            sb.Append("  Chassi: ").Append(Chassi).Append("\n");
            sb.Append("  VinRegravado: ").Append(VinRegravado).Append("\n");
            sb.Append("  Motor: ").Append(Motor).Append("\n");
            sb.Append("  Apontamento: ").Append(Apontamento).Append("\n");
            sb.Append("  FotoTraseira: ").Append(FotoTraseira).Append("\n");
            sb.Append("  DataFotoHodometro: ").Append(DataFotoHodometro).Append("\n");
            sb.Append("  DataFotoChassi: ").Append(DataFotoChassi).Append("\n");
            sb.Append("  DataFotoMotor: ").Append(DataFotoMotor).Append("\n");
            sb.Append("  DataFotoTraseira: ").Append(DataFotoTraseira).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

    }
}
