using System.Text;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Vistoria.Models.Report
{
    [DataContract]
    public class VistoriadorVo
    {

        [DataMember(Name = "codigo", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "codigo")]
        public string Codigo { get; set; }

        [DataMember(Name = "nome", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "nome")]
        public string Nome { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class VistoriadorVo {\n");
            sb.Append("  Codigo: ").Append(Codigo).Append("\n");
            sb.Append("  Nome: ").Append(Nome).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

    }
}
