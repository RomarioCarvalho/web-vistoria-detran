using System.Text;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Vistoria.Models.Report
{
    [DataContract]
    public class ItensReprovacaoVo
    {

        [DataMember(Name = "aprovado", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "aprovado")]
        public bool? Aprovado { get; set; }

        [DataMember(Name = "reprovado", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "reprovado")]
        public bool? Reprovado { get; set; }

        [DataMember(Name = "na", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "na")]
        public bool? Na { get; set; }

        [DataMember(Name = "nomeItem", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "nomeItem")]
        public string NomeItem { get; set; }

        [DataMember(Name = "condicao", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "condicao")]
        public string Condicao { get; set; }

        [DataMember(Name = "id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class VistoriaVo {\n");
            sb.Append("  Aprovado: ").Append(Aprovado).Append("\n");
            sb.Append("  Reprovado: ").Append(Reprovado).Append("\n");
            sb.Append("  Na: ").Append(Na).Append("\n");
            sb.Append("  NomeItem: ").Append(NomeItem).Append("\n");
            sb.Append("  Condicao: ").Append(Condicao).Append("\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

    }
}
