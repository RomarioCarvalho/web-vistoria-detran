using System.Text;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Vistoria.Models.Report
{
    [DataContract]
    public class VistoriaVo
    {

        [DataMember(Name = "aprovado", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "aprovado")]
        public bool? Aprovado { get; set; }

        [DataMember(Name = "laudo", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "laudo")]
        public string Laudo { get; set; }

        [DataMember(Name = "sclv", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "sclv")]
        public string Sclv { get; set; }

        [DataMember(Name = "data", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "data")]
        public string Data { get; set; }

        [DataMember(Name = "validade", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "validade")]
        public string Validade { get; set; }


        [DataMember(Name = "servicoPrincipal", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "servicoPrincipal")]
        public string ServicoPrincipal { get; set; }


        [DataMember(Name = "servicosAlternativos", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "servicosAlternativos")]
        public string ServicosAlternativos { get; set; }


        [DataMember(Name = "observacoes", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "observacoes")]
        public string Observacoes { get; set; }

        [DataMember(Name = "ugc", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "ugc")]
        public string Ugc { get; set; }

        [DataMember(Name = "itensReprovacao", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "itensReprovacao")]
        public List<ItensReprovacaoVo> ItensReprovacao { get; set; }

        [DataMember(Name = "proprietario", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "proprietario")]
        public ProprietarioVo Proprietario { get; set; }

        [DataMember(Name = "veiculo", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "veiculo")]
        public VeiculoVo Veiculo { get; set; }

        [DataMember(Name = "credenciada", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "credenciada")]
        public CredenciadaVo Credenciada { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class VistoriaVo {\n");
            sb.Append("  Aprovado: ").Append(Aprovado).Append("\n");
            sb.Append("  Laudo: ").Append(Laudo).Append("\n");
            sb.Append("  Sclv: ").Append(Sclv).Append("\n");
            sb.Append("  Data: ").Append(Data).Append("\n");
            sb.Append("  Validade: ").Append(Validade).Append("\n");
            sb.Append("  ServicoPrincipal: ").Append(ServicoPrincipal).Append("\n");
            sb.Append("  ServicosAlternativos: ").Append(ServicosAlternativos).Append("\n");
            sb.Append("  Observacoes: ").Append(Observacoes).Append("\n");
            sb.Append("  Ugc: ").Append(Ugc).Append("\n");
            sb.Append("  ItensReprovacao: ").Append(ItensReprovacao.ToString()).Append("\n");
            sb.Append("  Proprietario: ").Append(Proprietario).Append("\n");
            sb.Append("  Veiculo: ").Append(Veiculo).Append("\n");
            sb.Append("  Credenciada: ").Append(Credenciada).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

    }
}
