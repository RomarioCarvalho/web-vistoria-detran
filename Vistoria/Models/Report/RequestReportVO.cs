using System;
using System.Text;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Vistoria.Models.Report
{
    [DataContract]
    public class RequestReportVO
    {

        [DataMember(Name = "modelosLaudoVistoria", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "modelosLaudoVistoria")]
        public string ModelosLaudoVistoria { get; set; }

        [DataMember(Name = "parameters", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "parameters")]
        public Dictionary<string, Object> Parameters { get; set; }

        [DataMember(Name = "filters", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "filters")]
        public Dictionary<string, Object> Filters { get; set; }

        [DataMember(Name = "vistoriaId", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "vistoriaId")]
        public string VistoriaId { get; set; }

        [DataMember(Name = "vistoria", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "vistoria")]
        public VistoriaVo Vistoria { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class RequestReportVO {\n");
            sb.Append("  ModelosLaudoVistoria: ").Append(ModelosLaudoVistoria).Append("\n");
            sb.Append("  Parameters: ").Append(Parameters).Append("\n");
            sb.Append("  Filters: ").Append(Filters).Append("\n");
            sb.Append("  VistoriaId: ").Append(VistoriaId).Append("\n");
            sb.Append("  Vistoria: ").Append(Vistoria).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

    }
}
