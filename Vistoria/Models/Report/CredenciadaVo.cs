using System.Text;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Vistoria.Models.Report
{
    [DataContract]
    public class CredenciadaVo
    {

        [DataMember(Name = "ecv", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "ecv")]
        public string Ecv { get; set; }

        [DataMember(Name = "razaoSocial", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "razaoSocial")]
        public string RazaoSocial { get; set; }

        [DataMember(Name = "cnpj", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "cnpj")]
        public string Cnpj { get; set; }

        [DataMember(Name = "logo", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "logo")]
        public string Logo { get; set; }

        [DataMember(Name = "endereco", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "endereco")]
        public string Endereco { get; set; }

        [DataMember(Name = "portariaDenatran", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "portariaDenatran")]
        public string PortariaDenatran { get; set; }

        [DataMember(Name = "status", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [DataMember(Name = "municipio", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "municipio")]
        public string Municipio { get; set; }

        [DataMember(Name = "uf", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uf")]
        public string Uf { get; set; }

        [DataMember(Name = "cep", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "cep")]
        public string Cep { get; set; }

        [DataMember(Name = "telefone", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "telefone")]
        public string Telefone { get; set; }

        [DataMember(Name = "vistoriador", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "vistoriador")]
        public VistoriadorVo Vistoriador { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class CredenciadaVo {\n");
            sb.Append("  Ecv: ").Append(Ecv).Append("\n");
            sb.Append("  RazaoSocial: ").Append(RazaoSocial).Append("\n");
            sb.Append("  Cnpj: ").Append(Cnpj).Append("\n");
            sb.Append("  Logo: ").Append(Logo).Append("\n");
            sb.Append("  Endereco: ").Append(Endereco).Append("\n");
            sb.Append("  PortariaDenatran: ").Append(PortariaDenatran).Append("\n");
            sb.Append("  Status: ").Append(Status).Append("\n");
            sb.Append("  Municipio: ").Append(Municipio).Append("\n");
            sb.Append("  Uf: ").Append(Uf).Append("\n");
            sb.Append("  Cep: ").Append(Cep).Append("\n");
            sb.Append("  Telefone: ").Append(Telefone).Append("\n");
            sb.Append("  Vistoriador: ").Append(Vistoriador).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

    }
}
