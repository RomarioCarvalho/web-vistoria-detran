﻿using Vistoria.Enumerators;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vistoria.Models
{
    public class Log
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public string Type { get; set; }
        public long? TypeId { get; set; }
        public string Value { get; set; }
        public DateTime Data { get; set; }
        public long? UsuarioId { get; set; }
        public AcaoLog Acao { get; set; } 
    }
}