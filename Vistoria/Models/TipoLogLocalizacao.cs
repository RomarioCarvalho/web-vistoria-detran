﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Vistoria.Models
{
    [Serializable]
    public class TipoLogLocalizacao : AbstractModel
    {
        [Display(Name = "Descrição")]
        [Required(ErrorMessage = "Informe o {0}")]
        public string Descricao { get; set; }
        
    }
}