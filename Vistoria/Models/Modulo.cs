﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vistoria.Models
{
    [Serializable]
    public class Modulo : AbstractModel
    {
        [Display(Name = "Nome")]
        [Required(ErrorMessage = "Informe o {0}")]
        public string Nome { get; set; }

        [Display(Name = "Path")]
        [Required(ErrorMessage = "Informe o {0}")]
        public string Path { get; set; }

        [JsonIgnore]
        public virtual ICollection<Grupo> ListGrupo { get; set; }

        [Display(Name = "Grupos de Acesso")]
        [NotMapped]
        public List<long> GruposId { get; set; }
    }
}