﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vistoria.Models
{
    [Serializable]
    public class ConsultaVeiculo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public bool BaseEst { get; set; }
        public string codigoOperacao { get; set; }
        public string paramPlaca { get; set; }
        public string paramUF { get; set; }
        public string paramChassi { get; set; }
        public long paramRenavam { get; set; }
        public string Numero_CNPJ { get; set; }
        public long codigoRetExec { get; set; }
        public string nomeProprietArre { get; set; }
        public long codigoMunicipio { get; set; }
        public string nomeMunicipio { get; set; }
        public string unidadeFederacao { get; set; }
        public string nomeUf { get; set; }
        public long codigoMarcaMod { get; set; }
        public string marcaModelo { get; set; }
        public long anoFabricacao { get; set; }
        public long codigoTipVeic { get; set; }
        public string tipoVeiculo { get; set; }
        public long codigoCarroceria { get; set; }
        public string carroceria { get; set; }
        public long codigoCor { get; set; }
        public string cor { get; set; }
        public long codigoCategoria { get; set; }
        public string categoriaVeiculo { get; set; }
        public long codigoEspecie { get; set; }
        public string especie { get; set; }
        public long anoModelo { get; set; }
        public long potencia { get; set; }
        public long cilindrada { get; set; }
        public long codigoCombustive { get; set; }
        public string combustivel { get; set; }
        public string numeroMotor { get; set; }
        public decimal tracaoMax { get; set; }
        public decimal pesoBrutoTotal { get; set; }
        public decimal capacidadeCarga { get; set; }
        public string procedencia { get; set; }
        public long capacidadePassag { get; set; }
        public long numeroEixos { get; set; }
        public string restricao1 { get; set; }
        public string restricao2 { get; set; }
        public string restricao3 { get; set; }
        public string restricao4 { get; set; }
        public string restricao5 { get; set; }
        public string restricao6 { get; set; }

        [ForeignKey("Sitio")]
        public long sitioId { get; set; }
        public virtual Sitio Sitio { get; set; }

    }
}