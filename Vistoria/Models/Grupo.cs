﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vistoria.Models
{
    [Serializable]
    public class Grupo : AbstractModel
    {
        [Display(Name = "Nome")]
        [Required(ErrorMessage = "Informe o {0}")]
        public string Nome { get; set; }

        [JsonIgnore]
        public virtual ICollection<Usuario> ListUsuario { get; set; }

        //[JsonIgnore]
        public virtual List<Modulo> ListModulo { get; set; }

        [Display(Name = "Modulos")]
        [NotMapped]
        public List<long> ModulosId { get; set; }
    }
}