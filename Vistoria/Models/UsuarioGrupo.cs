﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vistoria.Models
{
    [Serializable]
    public class UsuarioGrupo
    {
        [Key]
        public long Id { get; set; }
        
        [ForeignKey("Usuario")]
        [Required]
        public long UsuarioId { get; set; }
        
        [ForeignKey("Grupo")]
        public long GrupoId { get; set; }

        public virtual Usuario Usuario { get; set; }
        public virtual Grupo Grupo { get; set; }

    }
}