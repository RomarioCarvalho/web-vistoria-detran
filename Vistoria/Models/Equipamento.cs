﻿using Vistoria.Enumerators;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vistoria.Models
{
    public class Equipamento : AbstractModel
    {
        public string Nome { get; set; }
        public string NomeAmigavel { get; set; }
        public int Ordem { get; set; }
        public string Obrigatorio { get; set; }

        [Display(Name = "TipoVeiculo")]
        [ForeignKey("TipoVeiculo")]
        public long TipoVeiculoId { get; set; }

        public virtual TipoVeiculo TipoVeiculo { get; set; }

    }
}