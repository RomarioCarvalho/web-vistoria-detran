﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vistoria.Models
{
    [Serializable]
    public class Sitio : AbstractModel
    {
        [Display(Name = "Nome")]
        [Required(ErrorMessage = "Informe o {0}")]
        public string Nome { get; set; }

        [Display(Name = "Razão Social")]
        public string RazaoSocial { get; set; }

        [Display(Name = "CNPJ")]
        public string Cnpj { get; set; }
        public string Logo { get; set; }

        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public long Raio { get; set; }

        [Display(Name = "Endereço")]
        public string Endereco { get; set; }

        [Display(Name = "Portaria Denatran")]
        public string PortariaDenatran { get; set; }
        public string Status { get; set; }

        [Display(Name = "Município")]
        public string Municipio { get; set; }
        public string Uf { get; set; }

        [Display(Name = "CEP")]
        public string Cep { get; set; }
        public string Telefone { get; set; }

        [Display(Name = "Empresa")]
        [ForeignKey("Empresa")]
        public long EmpresaId { get; set; }

        public virtual Empresa Empresa { get; set; }

    }
}