﻿using Vistoria.Models;
using Vistoria.Models.Context;
using Vistoria.Business;
using Vistoria.Enumerators;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Vistoria.Controllers
{
    public class SitioController : AbstractController<VistoriaContext>
    {
        // GET: Sitio
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [OutputCache(Duration = 0)]
        public virtual ActionResult Pagination(string sEcho, int iDisplayStart, int iColumns, int iDisplayLength, string sSearch, int iSortCol_0, string sSortDir_0)
        {
            string controller = ControllerContext.RouteData.Values["Controller"].ToString();
            sSearch = $"%{sSearch}%";
            var query = db.Sitio.Ativos().Where(x => DbFunctions.Like(x.Nome, sSearch) || DbFunctions.Like(x.Empresa.Nome, sSearch));
            if(sSortDir_0.ToLower() == "asc") {
                switch(iSortCol_0)
                {
                    case 0:
                        query = query.OrderBy(x => x.Id);
                        break;
                    case 1:
                        query = query.OrderBy(x => x.Nome);
                        break;
                    case 2:
                        query = query.OrderBy(x => x.Cnpj);
                        break;
                }
            }
            else
            {
                switch (iSortCol_0)
                {
                    case 0:
                        query = query.OrderByDescending(x => x.Id);
                        break;
                    case 1:
                        query = query.OrderByDescending(x => x.Nome);
                        break;
                    case 2:
                        query = query.OrderByDescending(x => x.Cnpj);
                        break;
                }
            }
            int recordsTotal = query.Count();
            var aList = query.Skip(iDisplayStart).Take(iDisplayLength);
            var data = aList.ToList().Select(x => new
            {
                x.Id,
                x.Nome,
                Empresa = x.Empresa.Nome,
                Cnpj = Utils.FormatarCpfCnpj(x.Cnpj),
                acoes = $"<a href='{Url.Action("Edit", controller)}/{x.Id}'><i class='glyphicon glyphicon-pencil'></i> Editar</a><button class='btn-link' onClick=\"excluir('{controller}', {x.Id}, '{x.Nome}')\"><i class='glyphicon glyphicon-trash'></i> Excluir</button>"
            }).ToArray();
            return Json(new
            {
                iDraw = 1,
                sEcho,
                iTotalRecords = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                aaData = data
            }, JsonRequestBehavior.AllowGet);
        }

        // GET: Sitio/Edit/5
        public ActionResult Edit(long? id)
        {
            Sitio sitio = new Sitio();
            
            if (id != null)
            {
                sitio = db.Sitio.Find(id);
                ViewBag.EmpresaId = new SelectList(db.Empresa.Ativos().OrderBy(x => x.Nome), "Id", "Nome", sitio?.EmpresaId);
            }
            else
            {
                ViewBag.EmpresaId = new SelectList(db.Empresa.Ativos().OrderBy(x => x.Nome), "Id", "Nome");
            }
            
            return View(sitio);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Sitio _sitio)
        {
            bool inserir = _sitio.Id == 0;
            try
            {

                db.Database.BeginTransaction(IsolationLevel.ReadUncommitted);
                var nome = _sitio.Nome.ToLower();

                if (inserir)
                {
                    //Validar se o nome do Sitio já existe                    
                    var exist = db.Sitio.Any(n => n.Nome.ToLower() == nome);
                    if (exist)
                    {
                        throw new Exception($"A sitio {nome} já está cadastrada.");
                    }
                    db.Sitio.Add(_sitio);
                }
                else
                {
                    //Validar se o nome do Sitio ja existe
                    var exist = db.Sitio.Any(n => n.Nome.ToLower() == nome && n.Id != _sitio.Id);
                    if (exist)
                    {
                        throw new Exception($"A sitio {nome} já está cadastrada.");
                    }

                    Sitio banco = db.Sitio.Find(_sitio.Id);
                    banco.Nome = _sitio.Nome;
                    banco.EmpresaId = _sitio.EmpresaId;
                    banco.RazaoSocial = _sitio.RazaoSocial;
                    banco.Cnpj = Utils.RemoveSpecialCharacters(_sitio.Cnpj);
                    banco.Endereco = _sitio.Endereco;
                    banco.Cep = _sitio.Cep;
                    banco.Telefone = Utils.RemoveSpecialCharacters(_sitio.Telefone);
                    banco.PortariaDenatran = _sitio.PortariaDenatran;
                    banco.Raio = _sitio.Raio;
                    banco.Latitude = _sitio.Latitude;
                    banco.Longitude = _sitio.Longitude;

                    _sitio = banco;
                }
                db.SaveChanges();

                db.Database.CurrentTransaction.Commit();
                SessionHelper.AddMessage(MessageTipo.Success, "Cadastro de Sitio", "Sitio "+ (inserir ? "Inserida":"Alterada") + " com sucesso");

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                db.Database.CurrentTransaction.Rollback();
                SessionHelper.AddMessage(MessageTipo.Danger, "Cadastro de Sitio", "Ocorreu um erro ao salvar a Sitio: " + e.Message);
                
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            Sitio sitio = db.Sitio.Find(id);
            db.Sitio.Remove(sitio);
            db.SaveChanges();
            SessionHelper.AddMessage(MessageTipo.Success, "Cadastro de Sitio", "Sitio excluída com sucesso");
            return RedirectToAction("Index");
        }

    }
}
