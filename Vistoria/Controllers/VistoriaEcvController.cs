﻿using Vistoria.Models;
using Vistoria.Models.Context;
using Vistoria.Business;
using Vistoria.Enumerators;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Configuration;
using Vistoria.Models.Report;
using Newtonsoft.Json;
using RestSharp;
using System.IO;
using System.Dynamic;

namespace Vistoria.Controllers
{
    public class VistoriaEcvController : AbstractController<VistoriaContext>
    {
        // GET: VistoriaEcv
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [OutputCache(Duration = 0)]
        public virtual ActionResult Pagination(string sEcho, int iDisplayStart, int iColumns, int iDisplayLength, string sSearch, int iSortCol_0, string sSortDir_0)
        {
            string controller = ControllerContext.RouteData.Values["Controller"].ToString();
            sSearch = $"%{sSearch}%";
            var query = db.VistoriaEcv.Ativos().Where(x => DbFunctions.Like(x.placaVeiculo.ToLower(), sSearch.ToLower()) ||
                                                            DbFunctions.Like(x.chassi.ToLower(), sSearch.ToLower()) ||
                                                            DbFunctions.Like(x.StatusVistoria.Nome.ToLower(), sSearch.ToLower()));
            if (sSortDir_0.ToLower() == "asc")
            {
                switch (iSortCol_0)
                {
                    case 0:
                        query = query.OrderBy(x => x.placaVeiculo);
                        break;
                    case 1:
                        query = query.OrderBy(x => x.dataVistoria);
                        break;
                    case 2:
                        query = query.OrderBy(x => x.StatusVistoria.Nome);
                        break;
                    case 3:
                        query = query.OrderBy(x => x.TipoVistoria.Nome);
                        break;
                }
            }
            else
            {
                switch (iSortCol_0)
                {
                    case 0:
                        query = query.OrderByDescending(x => x.placaVeiculo);
                        break;
                    case 1:
                        query = query.OrderByDescending(x => x.dataVistoria);
                        break;
                    case 2:
                        query = query.OrderByDescending(x => x.StatusVistoria.Nome);
                        break;
                    case 3:
                        query = query.OrderByDescending(x => x.TipoVistoria.Nome);
                        break;
                }
            }

            int recordsTotal = query.Count();
            var aList = query.Skip(iDisplayStart).Take(iDisplayLength);
            var data = aList.ToList().Select(x => new
            {
                placaVeiculo = x.placaVeiculo + "<br>" + x.chassi,
                dataVistoria = x.dataVistoria?.ToString("dd/MM/yyyy HH:mm:ss"),
                statusVistoria = x.StatusVistoria?.Nome,
                tipoVistoria = x.TipoVistoria?.Nome,
                acoes = $"<a class='btn btn-primary' href='{Url.Action("Edit", controller)}/{x.Id}'><i class='glyphicon glyphicon-eye-open'></i> Visualizar</a>"
            }).ToArray();
            return Json(new
            {
                iDraw = 1,
                sEcho,
                iTotalRecords = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                aaData = data
            }, JsonRequestBehavior.AllowGet);
        }

        // GET: Vistoria/Edit/5
        public ActionResult Edit(long? Id)
        {
            VistoriaEcv vistoriaEcv = new VistoriaEcv();
            
            if (Id != null)
            {
                vistoriaEcv = db.VistoriaEcv.Find(Id);

                ViewBag.corId = new SelectList(db.Cor.Ativos().OrderBy(x => x.Nome), "Id", "Nome", vistoriaEcv.corId);
                ViewBag.especieId = new SelectList(db.Especie.Ativos().OrderBy(x => x.Nome), "Id", "Nome", vistoriaEcv.especieId);
                ViewBag.combustivelId = new SelectList(db.Combustivel.Ativos().OrderBy(x => x.Nome), "Id", "Nome", vistoriaEcv.combustivelId);
                ViewBag.carroceriaId = new SelectList(db.Carroceria.Ativos().OrderBy(x => x.Nome), "Id", "Nome", vistoriaEcv.carroceriaId);
                ViewBag.tipoVeiculoId = new SelectList(db.TipoVeiculo.OrderBy(x => x.Nome), "Id", "Nome", vistoriaEcv.tipoVeiculoId);
                ViewBag.categoriaId = new SelectList(db.Categoria.OrderBy(x => x.Nome), "Id", "Nome", vistoriaEcv.categoriaId);
                ViewBag.statusVistoriaId = new SelectList(db.StatusVistoria.OrderBy(x => x.Nome), "Id", "Nome", vistoriaEcv.statusVistoriaId);
                ViewBag.solicTrocaPlacaId = new SelectList(db.TrocaPlaca.OrderBy(x => x.Nome), "Id", "Nome", vistoriaEcv.solicTrocaPlacaId);
                ViewBag.municipioIdProp = new SelectList(db.Municipio.OrderBy(x => x.Nome), "Id", "Nome", vistoriaEcv.municipioIdProp);
                ViewBag.tipoVistoriaId = new SelectList(db.TipoVistoria.Ativos().OrderBy(x => x.Nome), "Id", "Nome", vistoriaEcv.tipoVistoriaId);
                ViewBag.tipoVistoriaId2 = new SelectList(db.TipoVistoria.Ativos().OrderBy(x => x.Nome), "Id", "Nome", vistoriaEcv.tipoVistoriaId2);
                ViewBag.tipoVistoriaId3 = new SelectList(db.TipoVistoria.Ativos().OrderBy(x => x.Nome), "Id", "Nome", vistoriaEcv.tipoVistoriaId3);
                ViewBag.tipoVistoriaId4 = new SelectList(db.TipoVistoria.Ativos().OrderBy(x => x.Nome), "Id", "Nome", vistoriaEcv.tipoVistoriaId4);
                ViewBag.tipoVistoriaId5 = new SelectList(db.TipoVistoria.Ativos().OrderBy(x => x.Nome), "Id", "Nome", vistoriaEcv.tipoVistoriaId5);
                ViewBag.tipoVistoriaId6 = new SelectList(db.TipoVistoria.Ativos().OrderBy(x => x.Nome), "Id", "Nome", vistoriaEcv.tipoVistoriaId6);
                ViewBag.tipoVistoriaId7 = new SelectList(db.TipoVistoria.Ativos().OrderBy(x => x.Nome), "Id", "Nome", vistoriaEcv.tipoVistoriaId7);
                ViewBag.UsuarioId = SessionHelper.Usuario.Id;
                ViewBag.Biometria = SessionHelper.Usuario.Biometria;
                ViewBag.TokenBiometria = SessionHelper.Usuario.TokenBiometria;

                ViewBag.LogLocalizacao = db.LogLocalizacao.Where(x => x.VistoriaId == Id).OrderBy(x => x.DataAcao).ToList();
                ViewBag.Localizacao = db.LogLocalizacao.Where(l => l.VistoriaId == Id).FirstOrDefault();

                ViewBag.FotoCamera = db.FotoCamera.Where(x => x.VistoriaId == Id).ToList();
            }
            
            return View(vistoriaEcv);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(VistoriaEcv _vistoriaEcv)
        {
            bool inserir = _vistoriaEcv.Id == 0 ? true : false;
            try
            {

                db.Database.BeginTransaction(IsolationLevel.ReadUncommitted);
                var id = _vistoriaEcv.Id;

                if (inserir)
                {
                    //Validar se o nome do Vistoria já existe                    
                    var exist = db.VistoriaEcv.Any(n => n.Id == id);
                    if (exist)
                    {
                        throw new Exception($"A vistoriaECV {id} já está cadastrada.");
                    }
                    db.VistoriaEcv.Add(_vistoriaEcv);
                }
                else
                {

                    //Validar se o nome do Vistoria ja existe
                    var exist = db.VistoriaEcv.Any(n => n.Id == _vistoriaEcv.Id);
                    if (!exist)
                    {
                        throw new Exception($"A vistoriaECV {id} já não pode ser editado.");
                    }

                    VistoriaEcv banco = db.VistoriaEcv.Find(_vistoriaEcv.Id);

                    if (_vistoriaEcv.statusVistoriaId == (long)Enumerators.StatusVistoria.Aprovada && _vistoriaEcv.chassi.Equals(banco.ConsultaVeiculo.paramChassi) == false)
                    {
                        throw new Exception("Chassi informado é diferente da base do Detran!");
                    }

                    if (_vistoriaEcv.statusVistoriaId == (long)Enumerators.StatusVistoria.Aprovada && _vistoriaEcv.numeroMotor.Equals(banco.ConsultaVeiculo.numeroMotor) == false)
                    {
                        throw new Exception("Número do Motor informado é diferente da base do Detran!");
                    }

                    banco.nomeProprietario = _vistoriaEcv.nomeProprietario;
                    banco.cpfCnpjProp = Utils.RemoveSpecialCharacters(_vistoriaEcv.cpfCnpjProp.ToString());
                    banco.anoFabricacao = _vistoriaEcv.anoFabricacao;
                    banco.corId = _vistoriaEcv.corId;
                    banco.categoriaId = _vistoriaEcv.categoriaId;
                    banco.especieId = _vistoriaEcv.especieId;
                    banco.solicTrocaPlacaId = _vistoriaEcv.solicTrocaPlacaId;
                    banco.combustivelId = _vistoriaEcv.combustivelId;
                    banco.capacidadePassag = _vistoriaEcv.capacidadePassag;
                    banco.pbt = _vistoriaEcv.pbt;
                    banco.cmt = _vistoriaEcv.cmt;
                    banco.capacidadeCarga = _vistoriaEcv.capacidadeCarga;
                    banco.tipoVistoriaId = _vistoriaEcv.tipoVistoriaId;
                    banco.tipoVistoriaId2 = _vistoriaEcv.tipoVistoriaId2;
                    banco.tipoVistoriaId3 = _vistoriaEcv.tipoVistoriaId3;
                    banco.tipoVistoriaId4 = _vistoriaEcv.tipoVistoriaId4;
                    banco.tipoVistoriaId5 = _vistoriaEcv.tipoVistoriaId5;
                    banco.tipoVistoriaId6 = _vistoriaEcv.tipoVistoriaId6;
                    banco.tipoVistoriaId7 = _vistoriaEcv.tipoVistoriaId7;
                    banco.carroceriaId = _vistoriaEcv.carroceriaId;
                    banco.numeroEixos = _vistoriaEcv.numeroEixos;
                    banco.tipoVeiculoId = _vistoriaEcv.tipoVeiculoId;
                    banco.apontamento = _vistoriaEcv.apontamento;
                    banco.potencia = _vistoriaEcv.potencia;
                    banco.vinRegravado = _vistoriaEcv.vinRegravado;
                    banco.cilindrada = _vistoriaEcv.cilindrada;
                    banco.chassi = _vistoriaEcv.chassi;
                    banco.numeroMotor = _vistoriaEcv.numeroMotor;
                    banco.km = _vistoriaEcv.km;
                    banco.lacre = _vistoriaEcv.lacre;
                    banco.statusMotor = _vistoriaEcv.statusMotor;
                    banco.codigoRenavam = _vistoriaEcv.codigoRenavam;
                    banco.rg = _vistoriaEcv.rg;
                    banco.endereco = _vistoriaEcv.endereco;
                    banco.numeroEndereco = _vistoriaEcv.numeroEndereco;
                    banco.compleImovelProp = _vistoriaEcv.compleImovelProp;
                    banco.nomeBairro = _vistoriaEcv.nomeBairro;
                    banco.municipioIdProp = _vistoriaEcv.municipioIdProp;
                    banco.ufProprietario = "BA";
                    banco.cepImovelProp = _vistoriaEcv.cepImovelProp;
                    banco.numeroFoneProp = Utils.RemoveSpecialCharacters(_vistoriaEcv.numeroFoneProp.ToString());
                    banco.vistoriaLacrada = _vistoriaEcv.vistoriaLacrada;
                    banco.observacoesVistoriador = _vistoriaEcv.observacoesVistoriador;
                    //banco.placa_acordo = _vistoriaEcv.placa_acordo;
                    banco.placaAtribuida = _vistoriaEcv.placaAtribuida;
                    banco.numeroProtocolo = _vistoriaEcv.numeroProtocolo;
                    banco.placaVeiculo = _vistoriaEcv.placaVeiculo;
                    banco.anoModelo = _vistoriaEcv.anoModelo;
                    banco.indicaNovoUsado = _vistoriaEcv.indicaNovoUsado;
                    banco.numeroHodometro = _vistoriaEcv.numeroHodometro;
                    if(banco.geraDrTaxaVistoriaId != null)
                    {
                        banco.statusVistoriaId = _vistoriaEcv.statusVistoriaId;
                    }
                }
                db.SaveChanges();

                db.Database.CurrentTransaction.Commit();
                SessionHelper.AddMessage(MessageTipo.Success, "Cadastro de Vistoria", "Vistoria "+ (inserir ? "Inserida":"Alterada") + " com sucesso");

                return RedirectToAction("Edit");
            }
            catch (Exception e)
            {
                db.Database.CurrentTransaction.Rollback();
                SessionHelper.AddMessage(MessageTipo.Danger, "Cadastro de Vistoria", "Ocorreu um erro ao salvar a Vistoria: " + e.Message);

                ViewBag.corId = new SelectList(db.Cor.Ativos().OrderBy(x => x.Nome), "Id", "Nome", _vistoriaEcv.corId);
                ViewBag.especieId = new SelectList(db.Especie.Ativos().OrderBy(x => x.Nome), "Id", "Nome", _vistoriaEcv.especieId);
                ViewBag.combustivelId = new SelectList(db.Combustivel.Ativos().OrderBy(x => x.Nome), "Id", "Nome", _vistoriaEcv.combustivelId);
                ViewBag.carroceriaId = new SelectList(db.Carroceria.Ativos().OrderBy(x => x.Nome), "Id", "Nome", _vistoriaEcv.carroceriaId);
                ViewBag.tipoVeiculoId = new SelectList(db.TipoVeiculo.OrderBy(x => x.Nome), "Id", "Nome", _vistoriaEcv.tipoVeiculoId);
                ViewBag.categoriaId = new SelectList(db.Categoria.OrderBy(x => x.Nome), "Id", "Nome", _vistoriaEcv.categoriaId);
                ViewBag.statusVistoriaId = new SelectList(db.StatusVistoria.OrderBy(x => x.Nome), "Id", "Nome", _vistoriaEcv.statusVistoriaId);
                ViewBag.solicTrocaPlacaId = new SelectList(db.TrocaPlaca.OrderBy(x => x.Nome), "Id", "Nome", _vistoriaEcv.solicTrocaPlacaId);
                ViewBag.municipioIdProp = new SelectList(db.Municipio.OrderBy(x => x.Nome), "Id", "Nome", _vistoriaEcv.municipioIdProp);
                ViewBag.tipoVistoriaId = new SelectList(db.TipoVistoria.OrderBy(x => x.Nome), "Id", "Nome", _vistoriaEcv.tipoVistoriaId);
                ViewBag.tipoVistoriaId2 = new SelectList(db.TipoVistoria.Ativos().OrderBy(x => x.Nome), "Id", "Nome", _vistoriaEcv.tipoVistoriaId2);
                ViewBag.tipoVistoriaId3 = new SelectList(db.TipoVistoria.Ativos().OrderBy(x => x.Nome), "Id", "Nome", _vistoriaEcv.tipoVistoriaId3);
                ViewBag.tipoVistoriaId4 = new SelectList(db.TipoVistoria.Ativos().OrderBy(x => x.Nome), "Id", "Nome", _vistoriaEcv.tipoVistoriaId4);
                ViewBag.tipoVistoriaId5 = new SelectList(db.TipoVistoria.Ativos().OrderBy(x => x.Nome), "Id", "Nome", _vistoriaEcv.tipoVistoriaId5);
                ViewBag.tipoVistoriaId6 = new SelectList(db.TipoVistoria.Ativos().OrderBy(x => x.Nome), "Id", "Nome", _vistoriaEcv.tipoVistoriaId6);
                ViewBag.tipoVistoriaId7 = new SelectList(db.TipoVistoria.Ativos().OrderBy(x => x.Nome), "Id", "Nome", _vistoriaEcv.tipoVistoriaId7);

                ViewBag.UsuarioId = SessionHelper.Usuario.Id;
                ViewBag.Biometria = SessionHelper.Usuario.Biometria;
                ViewBag.TokenBiometria = SessionHelper.Usuario.TokenBiometria;

                ViewBag.LogLocalizacao = db.LogLocalizacao.Where(x => x.VistoriaId == _vistoriaEcv.Id).OrderBy(x => x.DataAcao).ToList();
                ViewBag.Localizacao = db.LogLocalizacao.Where(l => l.VistoriaId == _vistoriaEcv.Id).FirstOrDefault();

                ViewBag.FotoCamera = db.FotoCamera.Where(x => x.VistoriaId == _vistoriaEcv.Id).ToList();

                VistoriaEcv banco = db.VistoriaEcv.Find(_vistoriaEcv.Id);
                _vistoriaEcv.GeraDrTaxaVistoria = banco.GeraDrTaxaVistoria;
                _vistoriaEcv.geraDrTaxaVistoriaId = banco.geraDrTaxaVistoriaId;

                return View(_vistoriaEcv);
            }
        }

        public ActionResult Delete(int Id)
        {
            VistoriaEcv vistoriaEcv = db.VistoriaEcv.Find(Id);
            db.VistoriaEcv.Remove(vistoriaEcv);
            db.SaveChanges();
            SessionHelper.AddMessage(MessageTipo.Success, "Cadastro de Vistoria", "Vistoria excluída com sucesso");
            return RedirectToAction("Index");
        }

        [HttpPost]
        public string GerarTaxa(VistoriaEcv _vistoriaEcv)
        {
            dynamic retorno = new ExpandoObject();
            try
            {
                db.Database.BeginTransaction(IsolationLevel.ReadUncommitted);

                VistoriaEcv vistoriaEcv = db.VistoriaEcv.Find(_vistoriaEcv.Id);

                GeraDrTaxaVistoria(vistoriaEcv.Id);

                db.Database.CurrentTransaction.Commit();

                retorno.msg = "Taxa gerada com sucesso!";
                return JsonConvert.SerializeObject(retorno);
            }
            catch (Exception e)
            {
                db.Database.CurrentTransaction.Rollback();

                retorno.msg = e.Message;
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [HttpPost]
        public string EnviarVistoria(VistoriaEcv _vistoriaEcv)
        {
            dynamic retorno = new ExpandoObject();
            try
            {
                db.Database.BeginTransaction(IsolationLevel.ReadUncommitted);

                long id_usuario = SessionHelper.Usuario.Id;
                Usuario usuario = db.Usuario.Find(id_usuario);
                
                if(!usuario.HashBiometria.Equals(_vistoriaEcv.Hash))
                {
                    throw new Exception("Hash de Biometria inválido!");
                }

                VistoriaEcv vistoriaEcv = db.VistoriaEcv.Find(_vistoriaEcv.Id);

                if(vistoriaEcv.statusVistoriaId == (long)Enumerators.StatusVistoria.Aprovada && vistoriaEcv.chassi.Equals(vistoriaEcv.ConsultaVeiculo.paramChassi) == false)
                {
                    throw new Exception("Chassi informado é diferente da base do Detran!");
                }

                if (vistoriaEcv.statusVistoriaId == (long)Enumerators.StatusVistoria.Aprovada && vistoriaEcv.numeroMotor.Equals(vistoriaEcv.ConsultaVeiculo.numeroMotor) == false)
                {
                    throw new Exception("Número do Motor informado é diferente da base do Detran!");
                }

                GeraVistoria(vistoriaEcv.Id);

                db.Database.CurrentTransaction.Commit();

                retorno.Mensagem = "Vistoria enviada com sucesso!";
                return JsonConvert.SerializeObject(retorno);

            }
            catch (Exception e)
            {
                db.Database.CurrentTransaction.Rollback();

                retorno.Mensagem = e.Message;
                return JsonConvert.SerializeObject(retorno);
            }
        }

        public ActionResult GerarLaudoPDF(long Id)
        {
            try
            {
                VistoriaEcv vistoriaEcv = db.VistoriaEcv.Where(x => x.Id == Id)
                                            .Include(x => x.Usuario)
                                            .FirstOrDefault();

                ProprietarioVo proprietarioVo = new ProprietarioVo
                {
                    Nome = vistoriaEcv.nomeProprietario,
                    CpfCnpj = Utils.FormatarCpfCnpj(vistoriaEcv.cpfCnpjProp),
                    Cep = vistoriaEcv.cepImovelProp,
                    Endereco = vistoriaEcv.endereco,
                    Municipio = vistoriaEcv.Municipio.Nome,
                    Uf = vistoriaEcv.ufProprietario
                };

                string mesAno = vistoriaEcv.dataVistoria?.ToString(@"yyyyMM");
                String path = ConfigurationManager.AppSettings["CAMINHO_FOTOS"] + "\\" + mesAno + "\\" + vistoriaEcv.Id;
                List<LogLocalizacao> logLocalizacao = db.LogLocalizacao.Where(x => x.VistoriaId == Id).ToList();
                VeiculoVo veiculoVo = new VeiculoVo
                {
                    Placa = vistoriaEcv.placaVeiculo,
                    MarcaModelo = vistoriaEcv.ConsultaVeiculo.marcaModelo,
                    Tipo = Utils.NomeTipoVeiculo(vistoriaEcv.tipoVeiculoId.ToString()),
                    AnoFabricacao = vistoriaEcv.anoFabricacao,
                    AnoModelo = vistoriaEcv.anoModelo,
                    Cor = Utils.CorVeiculo(vistoriaEcv.corId.ToString()),
                    TipoCarroceria = Utils.TipoCarroceria(vistoriaEcv.carroceriaId.ToString()),
                    Especie = Utils.TipoEspecie(vistoriaEcv.especieId.ToString()),
                    Categoria = Utils.CategoriaVeiculo(vistoriaEcv.categoriaId.ToString()),
                    Combustivel = Utils.TipoCombustivel(vistoriaEcv.combustivelId.ToString()),
                    Eixos = vistoriaEcv.numeroEixos,
                    Potencia = vistoriaEcv.potencia,
                    Cilindrada = vistoriaEcv.cilindrada,
                    CapCarga = vistoriaEcv.capacidadeCarga,
                    Cmt = vistoriaEcv.cmt,
                    Pbt = vistoriaEcv.pbt,
                    CapPass = vistoriaEcv.capacidadePassag,
                    Lacre = vistoriaEcv.lacre,
                    Km = vistoriaEcv.km,
                    Chassi = vistoriaEcv.chassi,
                    VinRegravado = vistoriaEcv.vinRegravado == "1" ? "Sim" : "Não",
                    Motor = vistoriaEcv.numeroMotor,
                    Apontamento = vistoriaEcv.apontamento,
                    FotoChassi = Path.Combine(path, "foto_chassi.png"),
                    FotoTraseira = Path.Combine(path, "foto_fundo.png"),
                    FotoMotor = Path.Combine(path, "foto_motor.png"),
                    FotoHodometro = Path.Combine(path, "foto_hodometro.png"),
                    DataFotoChassi = logLocalizacao.Where(x => x.TipoLogLocalizacaoId == 4).FirstOrDefault().DataAcao.ToString(@"dd/MM/yyyy HH:mm:ss"),
                    DataFotoTraseira = logLocalizacao.Where(x => x.TipoLogLocalizacaoId == 13).FirstOrDefault().DataAcao.ToString(@"dd/MM/yyyy HH:mm:ss"),
                    DataFotoMotor = logLocalizacao.Where(x => x.TipoLogLocalizacaoId == 8).FirstOrDefault().DataAcao.ToString(@"dd/MM/yyyy HH:mm:ss"),
                    DataFotoHodometro = logLocalizacao.Where(x => x.TipoLogLocalizacaoId == 9).FirstOrDefault().DataAcao.ToString(@"dd/MM/yyyy HH:mm:ss")
                };

                VistoriadorVo vistoriadorVo = new VistoriadorVo
                {
                    Nome = vistoriaEcv.Usuario.Nome,
                    Codigo = vistoriaEcv.Usuario.MatriculaAtendente.ToString()
                };

                CredenciadaVo credenciadaVo = new CredenciadaVo
                {
                    Ecv = vistoriaEcv.Sitio.Id + " - " + vistoriaEcv.Sitio.Nome + " " + vistoriaEcv.Sitio.Cnpj,
                    RazaoSocial = vistoriaEcv.Sitio.RazaoSocial,
                    Cnpj = Utils.FormatarCpfCnpj(vistoriaEcv.Sitio.Cnpj),
                    Logo = vistoriaEcv.Sitio.Logo,
                    Endereco = vistoriaEcv.Sitio.Endereco,
                    PortariaDenatran = vistoriaEcv.Sitio.PortariaDenatran,
                    Status = vistoriaEcv.Sitio.Status == "A" ? "Ativa" : "Desativada",
                    Municipio = vistoriaEcv.Sitio.Municipio,
                    Uf = vistoriaEcv.Sitio.Uf,
                    Cep = vistoriaEcv.Sitio.Cep,
                    Telefone = Utils.FormatarTelefoneDDD(vistoriaEcv.Sitio.Telefone),
                    Vistoriador = vistoriadorVo
                };

                List<VistoriaEquipamento> listVistoriaEquipamento = db.VistoriaEquipamento
                    .Where(x => x.VistoriaId == Id)
                    .Where(x => x.Ativo == false)
                    //.Select(x => x.Equipamento.Nome)
                    .ToList();

                List<ItensReprovacaoVo> itensReprovacaoVo = new List<ItensReprovacaoVo>();
                
                if (vistoriaEcv.chassi.Equals(vistoriaEcv.ConsultaVeiculo.paramChassi) == false)
                {
                    itensReprovacaoVo.Add(new ItensReprovacaoVo()
                    {
                        Id = "1",
                        Reprovado = false,
                        Aprovado = false,
                        Na = false,
                        NomeItem = "Chassi",
                        Condicao = ""
                    });

                    itensReprovacaoVo.Add(new ItensReprovacaoVo()
                    {
                        Id = "1.1",
                        Reprovado = true,
                        Aprovado = false,
                        Na = false,
                        NomeItem = "Número Chassi divergente",
                        Condicao = "Chassi informado é diferente da base do Detran"
                    });
                }

                if (vistoriaEcv.numeroMotor.Equals(vistoriaEcv.ConsultaVeiculo.numeroMotor) == false)
                {
                    itensReprovacaoVo.Add(new ItensReprovacaoVo()
                    {
                        Id = "2",
                        Reprovado = false,
                        Aprovado = false,
                        Na = false,
                        NomeItem = "Número do Motor",
                        Condicao = ""
                    });

                    itensReprovacaoVo.Add(new ItensReprovacaoVo()
                    {
                        Id = "2.1",
                        Reprovado = true,
                        Aprovado = false,
                        Na = false,
                        NomeItem = "Número do Motor divergente",
                        Condicao = "Número do Motor informado é diferente da base do Detran"
                    });
                }

                if(listVistoriaEquipamento.Count > 0)
                {
                    itensReprovacaoVo.Add(new ItensReprovacaoVo()
                    {
                        Id = "3",
                        Reprovado = false,
                        Aprovado = false,
                        Na = false,
                        NomeItem = "Itens Obrigatórios",
                        Condicao = ""
                    });
                }

                int index = 1;
                foreach (VistoriaEquipamento item in listVistoriaEquipamento)
                {
                    itensReprovacaoVo.Add(new ItensReprovacaoVo()
                    {
                        Id = "3." + index.ToString(),
                        Reprovado = true,
                        Aprovado = false,
                        Na = false,
                        NomeItem = item.Equipamento.Nome,
                        Condicao = "Item não conforme de acordo com a Legislação"
                    });

                    index++;
                }

                List<string> servicosComplementar = new List<string>();

                if (vistoriaEcv.tipoVistoriaId2 != null)
                {
                    servicosComplementar.Add(vistoriaEcv.TipoVistoria2.Nome);
                }

                if (vistoriaEcv.tipoVistoriaId3 != null)
                {
                    servicosComplementar.Add(vistoriaEcv.TipoVistoria3.Nome);
                }

                if (vistoriaEcv.tipoVistoriaId4 != null)
                {
                    servicosComplementar.Add(vistoriaEcv.TipoVistoria4.Nome);
                }

                if (vistoriaEcv.tipoVistoriaId5 != null)
                {
                    servicosComplementar.Add(vistoriaEcv.TipoVistoria5.Nome);
                }

                if (vistoriaEcv.tipoVistoriaId6 != null)
                {
                    servicosComplementar.Add(vistoriaEcv.TipoVistoria6.Nome);
                }

                if (vistoriaEcv.tipoVistoriaId7 != null)
                {
                    servicosComplementar.Add(vistoriaEcv.TipoVistoria7.Nome);
                }

                VistoriaVo vistoriaVo = new VistoriaVo
                {
                    Aprovado = vistoriaEcv.statusVistoriaId == (long)Enumerators.StatusVistoria.Aprovada ? true : false,
                    Laudo = vistoriaEcv.Id.ToString(),
                    Sclv = "",
                    Data = vistoriaEcv.dataVistoria?.ToString(@"dd/MM/yyyy HH:mm:ss"),
                    Validade = vistoriaEcv.dataValidade?.ToString(@"dd/MM/yyyy"),
                    ServicoPrincipal = vistoriaEcv.TipoVistoria.Nome,
                    ServicosAlternativos = String.Join(", ", servicosComplementar),
                    Observacoes = vistoriaEcv.observacoesVistoriador,
                    Ugc = "",
                    ItensReprovacao = itensReprovacaoVo,
                    Credenciada = credenciadaVo,
                    Proprietario = proprietarioVo,
                    Veiculo = veiculoVo
                };

                RequestReportVO requestVo = new RequestReportVO
                {
                    ModelosLaudoVistoria = "TRANSFERENCIA_PROPRIEDADE",
                    Parameters = null,
                    Filters = null,
                    VistoriaId = vistoriaEcv.Id.ToString(),
                    Vistoria = vistoriaVo
                };

                var json = JsonConvert.SerializeObject(requestVo);

                var client = new RestClient(ConfigurationManager.AppSettings["URL_SERVICO_LAUDO"]);
                var request = new RestRequest($"api/v1/laudo", Method.POST);
                
                request.AddJsonBody(JsonConvert.SerializeObject(requestVo));
                var response = client.Execute<ResponseReportVO>(request);

                SessionHelper.AddMessage(MessageTipo.Success, "Gerar Laudo", "Laudo Gerado com Sucesso");
                
                return File(ConfigurationManager.AppSettings["PATH_LAUDO"] + vistoriaEcv.Id + ".pdf", "application/pdf");


            }
            catch (Exception e)
            {
                SessionHelper.AddMessage(MessageTipo.Danger, "Gerar Laudo", "Ocorreu um erro ao Gerar Laudo: " + e.Message);
                return null;
            }
        }

        private void GeraVistoria(long Id)
        {
            VistoriaEcv vistoriaEcv = db.VistoriaEcv.Find(Id);

            DetranVistoriaWS.geraVistoriaECV_Input input = new DetranVistoriaWS.geraVistoriaECV_Input()
            {
                codigoUsuario = long.Parse(ConfigurationManager.AppSettings["USER_WS_DETRAN"]),
                senhaUsuario = ConfigurationManager.AppSettings["PASS_WS_DETRAN"],
                codigoOperacao = "INC",
                matricAtendente = SessionHelper.Usuario.MatriculaAtendente,
                tipoServicoWeb = "V",
                indicaNovoUsado = vistoriaEcv.indicaNovoUsado,
                codigoEstacao = vistoriaEcv.sitioId.ToString(),
               // ufVeiculo = vistoriaEcv.ConsultaVeiculo.paramUF,
                ufVeiculo = "BA",
                solicTrocaPlaca = vistoriaEcv.solicTrocaPlacaId == 0 ? "N" : vistoriaEcv.solicTrocaPlacaId.ToString(),
                placaVeiculo = !String.IsNullOrEmpty(vistoriaEcv.placaVeiculo) ? Utils.RemoveSpecialCharacters(vistoriaEcv.placaVeiculo) : null,
                chassi = vistoriaEcv.ConsultaVeiculo.paramChassi,
                numeroMotor = vistoriaEcv.ConsultaVeiculo.numeroMotor,
                vinRegravado = vistoriaEcv.vinRegravado,
                statusMotor = vistoriaEcv.statusMotor,
                codigoRenavam = vistoriaEcv.codigoRenavam,
                nossoNumero = vistoriaEcv.GeraDrTaxaVistoria.nossoNumero,
                cpfCnpjProp = long.Parse(vistoriaEcv.cpfCnpjProp),
                tipoDocumento = (long)(vistoriaEcv.cpfCnpjProp.Length <= 11 ? TipoDocumento.CPF : TipoDocumento.CNPJ),
                rg = vistoriaEcv.rg,
                nomeProprietario = vistoriaEcv.nomeProprietario.ToUpper(),
                endereco = vistoriaEcv.endereco.ToUpper(),
                numeroEndereco = vistoriaEcv.numeroEndereco,
                compleImovelProp = vistoriaEcv.compleImovelProp,
                nomeBairro = vistoriaEcv.nomeBairro.ToUpper(),
                municipioProp = (long)vistoriaEcv.municipioIdProp,
                ufProprietario = vistoriaEcv.ufProprietario,
                cepImovelProp = vistoriaEcv.cepImovelProp,
                ddd = long.Parse(vistoriaEcv.numeroFoneProp.Substring(0,2)),
                numeroFoneProp = long.Parse(vistoriaEcv.numeroFoneProp.Substring(2, vistoriaEcv.numeroFoneProp.Length-2)),
                nrVistoriaECV = vistoriaEcv.Id, //////////// ??????
                dataVistoria = long.Parse(vistoriaEcv.dataVistoria?.ToString(@"yyyyMMdd")),
                horaVistoria = long.Parse(vistoriaEcv.dataVistoria?.ToString(@"HHmm")),
                matVistoriador = vistoriaEcv.Usuario.MatriculaAtendente,
                cnpjECV = vistoriaEcv.Sitio.Cnpj,
                statusVistoria = vistoriaEcv.StatusVistoria.Sigla,
                dataEmissao = long.Parse(vistoriaEcv.dataEmissao?.ToString(@"yyyyMMdd")),
                horaEmissao = long.Parse(vistoriaEcv.dataEmissao?.ToString(@"HHmm")),
                dataValidade = long.Parse(vistoriaEcv.dataValidade?.ToString(@"yyyyMMdd")),
                codigoServico01 = (long)vistoriaEcv.tipoVistoriaId,
                //codigoServico01 = 27,
                codigoServico02 = vistoriaEcv.tipoVistoriaId2 == null ? 0 : (long)vistoriaEcv.tipoVistoriaId2,
                codigoServico03 = vistoriaEcv.tipoVistoriaId3 == null ? 0 : (long)vistoriaEcv.tipoVistoriaId3,
                codigoServico04 = vistoriaEcv.tipoVistoriaId4 == null ? 0 : (long)vistoriaEcv.tipoVistoriaId4,
                codigoServico05 = vistoriaEcv.tipoVistoriaId5 == null ? 0 : (long)vistoriaEcv.tipoVistoriaId5,
                codigoServico06 = vistoriaEcv.tipoVistoriaId6 == null ? 0 : (long)vistoriaEcv.tipoVistoriaId6,
                codigoServico07 = vistoriaEcv.tipoVistoriaId7 == null ? 0 : (long)vistoriaEcv.tipoVistoriaId7,
                vistoriaLacrada = vistoriaEcv.vistoriaLacrada,
                numeroHodometro = (long)vistoriaEcv.numeroHodometro
            };

            if(input.statusVistoria == "R")
            {
                input.codigoReprovado = "1";
            }

            var json = JsonConvert.SerializeObject(input);

            DetranVistoriaWS.wsdetranvistoriaSoapClient ws = new DetranVistoriaWS.wsdetranvistoriaSoapClient();
            DetranVistoriaWS.geraVistoriaECV_Output response = ws.geraVistoriaECV(input);

            var json2 = JsonConvert.SerializeObject(response);

            if (response.retornoCritica != 0)
            {
                throw new Exception("Mensagem de retorno: " + response.retornoCritica.ToString() + " - " + Utils.RetornoWSDetran(response.retornoCritica.ToString()));
            }

            GeraVistoriaECV geraVistoria = db.GeraVistoriaECV.Add(new GeraVistoriaECV
            {
                codigoOperacao = response.codigoOperacao,
                matriculaAtendente = response.matricAtendente,
                indicaNovoUsado = response.indicaNovoUsado,
                ufVeiculo = response.ufVeiculo,
                placaVeiculo = response.placaVeiculo,
                solicTrocaPlaca = response.solicTrocaPlaca,
                chassi = response.chassi,
                numeroMotor = response.numeroMotor,
                vinRegravado = response.vinRegravado,
                statusMotor = response.statusMotor,
                codigoRenavam = response.codigoRenavam,
                cpfCnpjProp = response.cpfCnpjProp,
                tipoDocumento = response.tipoDocumento,
                rg = response.rg,
                nomeProprietario = response.nomeProprietario,
                endereco = response.endereco,
                numeroEndereco = response.numeroEndereco,
                compleImovelProp = response.compleImovelProp,
                nomeBairro = response.nomeBairro,
                municipioProp = response.municipioProp.ToString(),
                UfProprietario = response.ufProprietario,
                cepImovelProp = response.cepImovelProp,
                ddd = response.ddd,
                numeroFoneProp = response.numeroFoneProp,
                nrVistoriaECV = response.nrVistoriaECV,
                dataVistoria = response.dataVistoria,
                horaVistoria = response.horaVistoria,
                matVistoriador = response.matVistoriador,
                cnpjECV = response.cnpjECV,
                statusVistoria = response.statusVistoria,
                dataEmissao = response.dataEmissao,
                horaEmissao = response.horaEmissao,
                dataValidade = response.dataValidade,
                codigoServico01 = response.codigoServico01,
                codigoServico02 = response.codigoServico02,
                codigoServico03 = response.codigoServico03,
                codigoServico04 = response.codigoServico04,
                codigoServico05 = response.codigoServico05,
                codigoServico06 = response.codigoServico06,
                codigoServico07 = response.codigoServico07,
                vistoriaLacrada = response.vistoriaLacrada,
                codigoReprovado = response.codigoReprovado,
                nossoNumero = response.nossoNumero,
                retornoCritica = response.retornoCritica,
                numeroVistoria = response.numeroVistoria,
                placaAtribuida = response.placaAtribuida,
                numeroProtocolo = response.numeroProtocolo,
                DataHora = DateTime.Now
            });

            db.SaveChanges();

            vistoriaEcv.geraVistoriaECVId = geraVistoria.Id;
            db.SaveChanges();

        }

        private void GeraDrTaxaVistoria(long Id)
        {
            VistoriaEcv vistoriaEcv = db.VistoriaEcv.Find(Id);

            DetranVistoriaWS.geraDrTaxaVistoria_Input input = new DetranVistoriaWS.geraDrTaxaVistoria_Input()
            {
                codigoUsuario = long.Parse(ConfigurationManager.AppSettings["USER_WS_DETRAN"]),
                senhaUsuario = ConfigurationManager.AppSettings["PASS_WS_DETRAN"],
                codigoOperacao = "INC",
                chassiVeiculo = vistoriaEcv.chassi,
                CnpjPagador = vistoriaEcv.Sitio.Cnpj,
                matricAtendente = vistoriaEcv.Usuario.MatriculaAtendente,
                tipoServicoWeb = "V",
            };

            DetranVistoriaWS.wsdetranvistoriaSoapClient ws = new DetranVistoriaWS.wsdetranvistoriaSoapClient();
            DetranVistoriaWS.geraDrTaxaVistoria_Output response = ws.geraDrTaxaVistoria(input);

            if(response.retornoCritica != 0) {
                throw new Exception("Mensagem de retorno: " + response.retornoCritica.ToString() + " - " + Utils.RetornoWSDetran(response.retornoCritica.ToString()));
            }

            GeraDrTaxaVistoria taxa = db.GeraDrTaxaVistoria.Add(new GeraDrTaxaVistoria
            {
                codigoOperacao = response.codigoOperacao,
                codigoBarras = response.codigoBarras,
                cnpjPagador = response.CnpjPagador,
                chassiVeiculo = response.chassiVeiculo,
                nossoNumero = response.nossoNumero,
                retornoCritica = response.retornoCritica,
                codigoTaxa = response.codigoTaxa,
                valorDR = response.valorDR,
                dataVencimento = response.dataVencimento,
                codBarrasEdit = response.codigoBarras,
                DataHora = DateTime.Now
            });

            db.SaveChanges();

            vistoriaEcv.geraDrTaxaVistoriaId = taxa.Id;
            db.SaveChanges();

        }

        public void ObterFoto(int id_vistoria, string nome_foto)
        {
            try
            {
                string mesAno = db.VistoriaEcv.Find(id_vistoria).dataVistoria?.ToString(@"yyyyMM");
                string fileName = ConfigurationManager.AppSettings["CAMINHO_FOTOS"] + "\\" + mesAno + "\\" + id_vistoria + "\\" + nome_foto + ".png";

                byte[] buffer = null;
                using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    buffer = new byte[fs.Length];
                    fs.Read(buffer, 0, (int)fs.Length);

                    using (MemoryStream ms = new MemoryStream(buffer))
                    {
                        ms.WriteTo(Response.OutputStream);
                    }

                }
            }
            catch (Exception)
            {

            }

        }

        public void ObterFoto2()
        {
            try
            {
                string mesAno = "202212";
                string fileName = ConfigurationManager.AppSettings["CAMINHO_FOTOS"] + "\\" + mesAno + "\\-1\\" + mesAno + ".png";

                byte[] buffer = null;
                using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    buffer = new byte[fs.Length];
                    fs.Read(buffer, 0, (int)fs.Length);

                    using (MemoryStream ms = new MemoryStream(buffer))
                    {
                        ms.WriteTo(Response.OutputStream);
                    }

                }
            }
            catch (Exception)
            {

            }

        }


    }
}
