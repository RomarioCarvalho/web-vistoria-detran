﻿using Vistoria.Models;
using Vistoria.Models.Context;
using Vistoria.Business;
using Vistoria.Enumerators;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using static Vistoria.Business.Utils;

namespace Vistoria.Controllers
{
    public class ModuloController : AbstractController<VistoriaContext>
    {
        // GET: Modulos
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [OutputCache(Duration = 0)]
        public virtual ActionResult Pagination(string sEcho, int iDisplayStart, int iColumns, int iDisplayLength, string sSearch, int iSortCol_0, string sSortDir_0)
        {
            string controller = ControllerContext.RouteData.Values["Controller"].ToString();
            sSearch = $"%{sSearch}%";
            var query = db.Modulo.Ativos().Where(x => DbFunctions.Like(x.Nome, sSearch));
            if (sSortDir_0.ToLower() == "asc")
            {
                switch (iSortCol_0)
                {
                    case 0:
                        query = query.OrderBy(x => x.Id);
                        break;
                    case 1:
                        query = query.OrderBy(x => x.Nome);
                        break;
                    case 2:
                        query = query.OrderBy(x => x.Path);
                        break;
                }
            }
            else
            {
                switch (iSortCol_0)
                {
                    case 0:
                        query = query.OrderByDescending(x => x.Id);
                        break;
                    case 1:
                        query = query.OrderByDescending(x => x.Nome);
                        break;
                    case 2:
                        query = query.OrderByDescending(x => x.Path);
                        break;
                }
            }
            int recordsTotal = query.Count();
            var aList = query.Skip(iDisplayStart).Take(iDisplayLength);
            var data = aList.ToList().Select(x => new
            {
                x.Id,
                x.Nome,
                x.Path,
                acoes = $"<a href='{Url.Action("Edit", controller)}/{x.Id}'><i class='glyphicon glyphicon-pencil'></i> Editar</a><button class='btn-link' onClick=\"excluir('{controller}', {x.Id}, '{x.Nome}')\"><i class='glyphicon glyphicon-trash'></i> Excluir</button>"
            }).ToArray();
            return Json(new
            {
                iDraw = 1,
                sEcho,
                iTotalRecords = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                aaData = data
            }, JsonRequestBehavior.AllowGet);
        }

        // GET: Modulos/Edit/5
        public ActionResult Edit(long? id)
        {
            Modulo modulo = new Modulo();
            ViewBag.Grupos = db.Grupo
                .Ativos()
                .OrderBy(x => x.Nome)
                .ToList();
            ViewBag.GruposChecked = new List<Grupo>();
            if (id != null)
            {
                modulo = db.Modulo.Find(id);
                ViewBag.GruposChecked = db.Modulo.Find(id).ListGrupo.ToList();
            }
            var selectListItems = new List<SelectListItem>();
            return View(modulo);
        }

        // POST: Modulos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Modulo _modulo)
        {
            bool inserir = _modulo.Id == 0;
            try
            {
                db.Database.BeginTransaction(IsolationLevel.ReadUncommitted);
                if (inserir)
                {
                    _modulo.ListGrupo = new List<Grupo>();
                    db.Modulo.Add(_modulo);
                }
                else
                {
                    Modulo banco = db.Modulo.Find(_modulo.Id);
                    banco.Nome = _modulo.Nome;
                    banco.Path = _modulo.Path;
                    banco.GruposId = _modulo.GruposId;
                    _modulo = banco;
                }
                db.SaveChanges();

                _modulo.ListGrupo.Clear();
                _modulo.GruposId.ForEach(x =>
                   _modulo.ListGrupo.Add(db.Grupo.Find(x))
                );

                db.SaveChanges();
                db.Database.CurrentTransaction.Commit();
                SessionHelper.AddMessage(MessageTipo.Success, "Cadastro de Módulo", "Módulo "+ (inserir ? "Inserido":"Alterado") + " com sucesso");
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                db.Database.CurrentTransaction.Rollback();
                SessionHelper.AddMessage(MessageTipo.Danger, "Cadastro de Módulo", "Ocorreu um erro ao salvar o Módulo: " + e.Message);

                ViewBag.Grupos = db.Grupo
                    .Ativos()
                    .OrderBy(x => x.Nome)
                    .ToList();
                ViewBag.GruposChecked = new List<Grupo>();
                if (!inserir)
                {
                    ViewBag.GruposChecked = db.Modulo.Find(_modulo.Id).ListGrupo.ToList();
                }
                return View(_modulo);
            }
        }

        public ActionResult Delete(int id)
        {
            Modulo modulo = db.Modulo.Find(id);
            db.Modulo.Remove(modulo);
            db.SaveChanges();
            SessionHelper.AddMessage(MessageTipo.Success, "Cadastro de Módulo", "Módulo excluído com sucesso");
            return RedirectToAction("Index");
        }

    }
}
