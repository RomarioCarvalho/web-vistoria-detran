﻿using Vistoria.Business;
using Vistoria.Enumerators;
using Vistoria.Filters;
using System;
using System.Data.Entity;
using System.Web.Mvc;

namespace Vistoria.Controllers
{
    [Authorize]
    [ValidarAcessoFilter]
    public abstract class AbstractController<T> : Controller where T : DbContext
    {
        public T db;

        protected AbstractController()
        {
            db = Activator.CreateInstance<T>();
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = false;
            SessionHelper.AddMessage(MessageTipo.Danger, "Erro no Sistema", "Ocorreu um erro: " + filterContext.Exception.Message);
         
            //_Logger.Error(filterContext.Exception);

            filterContext.Result = new ViewResult
            {
                ViewName = "~/Views/Shared/Error.cshtml"
            };
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}