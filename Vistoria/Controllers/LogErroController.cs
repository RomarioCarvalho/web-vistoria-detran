﻿using Vistoria.Business;
using Vistoria.Controllers;
using Vistoria.Enumerators;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Vistoria.Models;
using Vistoria.Models.Context;

namespace Vistoria.Controllers
{
    public class LogErroController : AbstractController<VistoriaContext>
    {

        public ActionResult Index()
        {
			return View();
        }

		[HttpPost]
        public virtual ActionResult Pagination(string sEcho, int iDisplayStart, int iColumns, int iDisplayLength, string sSearch, int iSortCol_0, string sSortDir_0)
        {
            sSearch = $"%{sSearch}%";
            var query = db.LogErro.Where(x => DbFunctions.Like(x.Acao, sSearch)).OrderByDescending(x => x.Data);
            if (sSortDir_0.ToLower() == "asc")
            {
                switch (iSortCol_0)
                {
                    case 0:
                        query = query.OrderBy(x => x.Id);
                        break;
                    case 1:
                        query = query.OrderBy(x => x.Acao);
                        break;
                    case 2:
                        query = query.OrderBy(x => x.StackTrace);
                        break;
                    case 3:
                        query = query.OrderBy(x => x.Message);
                        break;
                    case 4:
                        query = query.OrderBy(x => x.ContentApiString);
                        break;
                    case 5:
                        query = query.OrderBy(x => x.Data);
                        break;
                }
            }
            else
            {
                switch (iSortCol_0)
                {
                    case 0:
                        query = query.OrderByDescending(x => x.Id);
                        break;
                    case 1:
                        query = query.OrderByDescending(x => x.Acao);
                        break;
                    case 2:
                        query = query.OrderByDescending(x => x.StackTrace);
                        break;
                    case 3:
                        query = query.OrderByDescending(x => x.Message);
                        break;
                    case 4:
                        query = query.OrderByDescending(x => x.ContentApiString);
                        break;
                    case 5:
                        query = query.OrderByDescending(x => x.Data);
                        break;
                }
            }

            int recordsTotal = query.Count();
            var aList = query.Skip(iDisplayStart).Take(iDisplayLength);
            var data = aList.ToList().Select(x => new
            {
				x.Id,
				x.Acao,
				x.StackTrace,
				x.Message,
				x.ContentApiString,
                DataHora = x.Data.ToString("dd/MM/yyyy HH:mm:ss"),
            }).ToArray();
            return Json(new
            {
                iDraw = 1,
                sEcho,
                iTotalRecords = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                aaData = data
            }, JsonRequestBehavior.AllowGet);
        }

    }
}
