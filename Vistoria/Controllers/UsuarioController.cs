﻿using Vistoria.Business;
using Vistoria.Enumerators;
using Vistoria.Models;
using Vistoria.Models.Context;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;

namespace Vistoria.Controllers
{
    public class UsuarioController : AbstractController<VistoriaContext>
    {
        // GET: Usuarios
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [OutputCache(Duration = 0)]
        public virtual ActionResult Pagination(string sEcho, int iDisplayStart, int iColumns, int iDisplayLength, string sSearch, int iSortCol_0, string sSortDir_0)
        {
            string controller = ControllerContext.RouteData.Values["Controller"].ToString();
            sSearch = $"%{sSearch}%";
            var query = db.Usuario.Ativos().Where(x => DbFunctions.Like(x.Nome.ToLower(), sSearch.ToLower()) || 
                                                       DbFunctions.Like(x.Login.ToLower(), sSearch.ToLower()) ||
                                                       DbFunctions.Like(x.Email.ToLower(), sSearch.ToLower()) ||
                                                       DbFunctions.Like(x.MatriculaAtendente.ToString(), sSearch.ToLower()));
            if (sSortDir_0.ToLower() == "asc")
            {
                switch (iSortCol_0)
                {
                    case 0:
                        query = query.OrderBy(x => x.Id);
                        break;
                    case 1:
                        query = query.OrderBy(x => x.Nome);
                        break;
                    case 2:
                        query = query.OrderBy(x => x.Login);
                        break;
                    case 3:
                        query = query.OrderBy(x => x.Email);
                        break;
                    case 4:
                        query = query.OrderBy(x => x.MatriculaAtendente);
                        break;
                }
            }
            else
            {
                switch (iSortCol_0)
                {
                    case 0:
                        query = query.OrderByDescending(x => x.Id);
                        break;
                    case 1:
                        query = query.OrderByDescending(x => x.Nome);
                        break;
                    case 2:
                        query = query.OrderByDescending(x => x.Login);
                        break;
                    case 3:
                        query = query.OrderByDescending(x => x.Email);
                        break;
                    case 4:
                        query = query.OrderByDescending(x => x.MatriculaAtendente);
                        break;
                }
            }
            int recordsTotal = query.Count();
            var aList = query.Skip(iDisplayStart).Take(iDisplayLength);
            var data = aList.ToList().Select(x => new
            {
                x.Id,
                x.Nome,
                x.Login,
                x.Email,
                x.MatriculaAtendente,
                acoes = $"<a href='{Url.Action("Edit", controller)}/{x.Id}'><i class='glyphicon glyphicon-pencil'></i> Editar</a><button class='btn-link' onClick=\"excluir('{controller}', {x.Id}, '{x.Nome}')\"><i class='glyphicon glyphicon-trash'></i> Excluir</button>"
            }).ToArray();
            return Json(new
            {
                iDraw = 1,
                sEcho,
                iTotalRecords = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                aaData = data
            }, JsonRequestBehavior.AllowGet);
        }

        // GET: Usuarios/Edit/5
        public ActionResult Edit(long? id)
        {
            Usuario usuario = new Usuario();
            ViewBag.Grupos = db.Grupo
                .Ativos()
                .OrderBy(x => x.Nome)
                .ToList();
            ViewBag.GruposChecked = new List<Grupo>();
            if (id != null)
            {
                usuario = db.Usuario.Find(id);
                ViewBag.GruposChecked = db.Usuario.Find(usuario.Id).ListGrupo.ToList();
                ViewBag.SitioId = new SelectList(db.Sitio.Ativos().OrderBy(x => x.Nome), "Id", "Nome", usuario?.SitioId);
            }
            else
            {
                ViewBag.SitioId = new SelectList(db.Sitio.Ativos().OrderBy(x => x.Nome), "Id", "Nome");
            }
            usuario.Senha = string.Empty;
            var selectListItems = new List<SelectListItem>();
            return View(usuario);
        }

        public string  ResetSenha()
        {
            return Membership.GeneratePassword(8, 4);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Usuario _usuario)
        {
            bool inserir = _usuario.Id == 0;
            try
            {

                var dic = new Dictionary<string, string>();
                dic.Add("{#Login}", _usuario.Login);
                string html;
                if (_usuario.AutenticaAD == false)
                {
                    if(string.IsNullOrEmpty(_usuario.Senha) && inserir == true)
                    {
                        SessionHelper.AddMessage(MessageTipo.Danger, "Cadastro de Usuário", "Campo Senha é Obrigatório");
                    }

                    dic.Add("{#Senha}", _usuario.Senha);
                    html = Utils.LerTemplateEsubstituirTags(Server.MapPath("~/Templates/usuarioSenha.html"), dic);
                }
                else
                {
                    html = Utils.LerTemplateEsubstituirTags(Server.MapPath("~/Templates/usuario.html"), dic);
                }

                db.Database.BeginTransaction(IsolationLevel.ReadUncommitted);
                if (inserir)
                {
                    UsuarioBO.Inserir(db, _usuario);

                    var task = Task.Factory
                        .StartNew(() =>
                        {
                            try
                            {
                               SendEmail.Enviar(_usuario.Email, "Acesso ao sistema", html);
                            }
                            catch(Exception)
                            {
                            }
                            
                        }); 
                }
                else
                {
                    UsuarioBO.Alterar(db, _usuario);
                }
                db.SaveChanges();
                db.Database.CurrentTransaction.Commit();
                SessionHelper.AddMessage(MessageTipo.Success, "Cadastro de Usuário", "Usuário " + (inserir ? "Inserido" : "Alterado") + " com sucesso");
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                db.Database.CurrentTransaction.Rollback();
                SessionHelper.AddMessage(MessageTipo.Danger, "Cadastro de Usuário", "Ocorreu um erro ao salvar o Usuário: " + e.Message);

                ViewBag.Grupos = db.Grupo
                    .Ativos()
                    .OrderBy(x => x.Nome)
                    .ToList();
                ViewBag.GruposChecked = new List<Grupo>();
                if (!inserir)
                {
                    ViewBag.GruposChecked = db.Usuario.Find(_usuario.Id).ListGrupo.ToList();
                }

                return View(_usuario);
            }
        }

        public ActionResult Delete(int id)
        {
            Usuario usuario = db.Usuario.Find(id);
            db.Usuario.Remove(usuario);
            db.SaveChanges();
            SessionHelper.AddMessage(MessageTipo.Success, "Cadastro de Usuário", "Usuário excluído com sucesso");
            return RedirectToAction("Index");
        }

    }
}
