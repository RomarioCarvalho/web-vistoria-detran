﻿using System;
using System.Web.Http;
using Vistoria.Models.Context;
using NReco.VideoConverter;
using System.Configuration;
using System.IO;
using Vistoria.Models;

namespace Vistoria.Controllers.Api
{
    public class CameraController : ApiController
    {

        private VistoriaContext db;
        public CameraController()
        {
            db = new VistoriaContext();
        }

        [HttpGet, ActionName("TirarFoto")]
        public IHttpActionResult TirarFoto(long id)
        {
            try
            {
                var ffMpeg = new FFMpegConverter();
                //ffMpeg.ConvertMedia(url, "D:\\temp\\video.jpg", Format.mpeg);

                DateTime dateTime = DateTime.Now;

                string mesAno = db.VistoriaEcv.Find(id).dataVistoria?.ToString(@"yyyyMM");
                String path = ConfigurationManager.AppSettings["CAMINHO_FOTOS"] + "\\" + mesAno + "\\" + id;

                string nome_arquivo = mesAno + dateTime.Second.ToString();

                FotoCamera fotoCamera = new FotoCamera()
                {
                    VistoriaId = id,
                    DataHora = dateTime,
                    NomeArquivo = nome_arquivo
                };
                db.FotoCamera.Add(fotoCamera);
                db.SaveChanges();

                string imgPath = Path.Combine(path, nome_arquivo + ".png");

                ffMpeg.GetVideoThumbnail(ConfigurationManager.AppSettings["URL_RTSP"], imgPath);

                return Json(new { Status = 1, Mensagem = "Foto capturada com sucesso!" });

            }
            catch (Exception ex)
            {
                return Json(new { Status = 0, Mensagem = ex.Message });
            }
            
        }

        [HttpGet, ActionName("TirarFoto2")]
        public IHttpActionResult TirarFoto2()
        {
            try
            {
                var ffMpeg = new FFMpegConverter();
                //ffMpeg.ConvertMedia(url, "D:\\temp\\video.jpg", Format.mpeg);

                DateTime dateTime = DateTime.Now;

                string mesAno = "202212";
                String path = ConfigurationManager.AppSettings["CAMINHO_FOTOS"] + "\\" + mesAno + "\\" + "-1";

                string nome_arquivo = mesAno;

                string imgPath = Path.Combine(path, nome_arquivo + ".png");

                ffMpeg.GetVideoThumbnail(ConfigurationManager.AppSettings["URL_RTSP"], imgPath);

                return Json(new { Status = 1, Mensagem = "Foto capturada com sucesso!" });

            }
            catch (Exception ex)
            {
                return Json(new { Status = 0, Mensagem = ex.Message });
            }

        }




    }
}