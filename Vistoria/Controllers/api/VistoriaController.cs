﻿using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Vistoria.Models.Context;
using Vistoria.Models.ViewModel;
using Vistoria.Models;
using Vistoria.Business;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Data;
using Vistoria.Enumerators;

namespace Vistoria.Controllers.Api
{
    public class VistoriaController : ApiController
    {

        private VistoriaContext db;
        public VistoriaController()
        {
            db = new VistoriaContext();
        }

        [HttpPost, ActionName("ConsultarPlacaChassi")]
        public HttpResponseMessage ConsultarPlacaChassi()
        {
            try
            {
                string placa_chassi = HttpContext.Current.Request.Form["placa_chassi"];
                string user_token = HttpContext.Current.Request.Form["user_token"];

                RetornoVM retornoVM;
                HttpStatusCode httpStatusCode;

                Usuario usuario = SegurancaBO.ValidarToken(db, user_token);

                DetranVistoriaWS.consultaVeiculoBin_Output response = ConsultarVeiculoBin(placa_chassi, usuario.Sitio.Cnpj);

                if(response.codigoRetExec != 0)
                {
                    throw new Exception("Mensagem de retorno: " + response.codigoRetExec.ToString() + " - " + Utils.RetornoWSDetran(response.codigoRetExec.ToString()));
                }

                ConsultaVeiculo consulta = new ConsultaVeiculo();
                //consulta.BaseEst = false;
                //consulta.paramChassi = "9BWAA05U39T217279";
                consulta.paramChassi = response.paramChassi;
                //consulta.paramPlaca = "ABC-1234";
                consulta.paramPlaca = response.paramPlaca;
                //consulta.paramRenavam = 1315007301;
                consulta.paramRenavam = response.paramRenavam;
                consulta.anoFabricacao = response.anoFabricacao;
                consulta.anoModelo = response.anoModelo;
                consulta.capacidadeCarga = response.capacidadeCarga;
                consulta.capacidadePassag = response.capacidadePassag;
                consulta.carroceria = response.carroceria;
                consulta.categoriaVeiculo = response.categoriaVeiculo;
                consulta.cilindrada = response.cilindrada;
                consulta.codigoCarroceria = response.codigoCarroceria;
                consulta.codigoCategoria = response.codigoCategoria;
                consulta.codigoCombustive = response.codigoCombustive;
                consulta.codigoCor = response.codigoCor;
                consulta.codigoEspecie = response.codigoEspecie;
                consulta.codigoTipVeic = response.codigoTipVeic;
                consulta.combustivel = response.combustivel;
                consulta.codigoMarcaMod = response.codigoMarcaMod;
                consulta.cor = response.cor;
                consulta.especie = response.especie;
                consulta.marcaModelo = response.marcaModelo;
                consulta.nomeProprietArre = response.nomeProprietArre;
                consulta.numeroEixos = response.numeroEixos;
                consulta.numeroMotor = response.numeroMotor;
                consulta.paramChassi = response.paramChassi;
                consulta.paramPlaca = response.paramPlaca;
                consulta.paramRenavam = response.paramRenavam;
                consulta.tipoVeiculo = response.tipoVeiculo;
                consulta.potencia = response.potencia;
                consulta.pesoBrutoTotal = response.pesoBrutoTotal;
                consulta.codigoEspecie = response.codigoEspecie;
                consulta.codigoMunicipio = response.codigoMunicipio;
                consulta.codigoOperacao = response.codigoOperacao;
                consulta.codigoRetExec = response.codigoRetExec;
                consulta.nomeMunicipio = response.nomeMunicipio;
                consulta.nomeUf = response.nomeUf;
                consulta.Numero_CNPJ = response.Numero_CNPJ;
                consulta.paramUF = response.paramUF;
                consulta.procedencia = response.procedencia;
                consulta.tracaoMax = response.tracaoMax;
                consulta.unidadeFederacao = response.unidadeFederacao;
                consulta.sitioId = usuario.SitioId;

                db.ConsultaVeiculo.Add(consulta);
                db.SaveChanges();

                long taxaId = GeraDrTaxaVistoria(response.paramChassi, usuario);

                ConsultaVeiculoVM retorno = new ConsultaVeiculoVM
                {
                    Id = consulta.Id,
                    ParamChassi = consulta.paramChassi,
                    ParamPlaca = String.IsNullOrEmpty(consulta.paramPlaca) ? (placa_chassi.Length <= 7 ? placa_chassi : "") : consulta.paramPlaca,
                    ParamRenavam = consulta.paramRenavam,
                    TaxaId = taxaId,
                    TipoVeiculoId = consulta.codigoTipVeic,
                    //Km = consulta.k ,
                    AnoFabricacao = consulta.anoFabricacao,
                    AnoModelo = consulta.anoModelo,
                    CorId = consulta.codigoCor,
                    CarroceriaId = consulta.codigoCarroceria,
                    EspecieId = consulta.codigoEspecie,
                    CategoriaId = consulta.codigoCategoria,
                    CombustivelId = consulta.codigoCombustive,
                    //StatusMotor = consulta.status ,
                    //IndicaNovoUsado = consulta.indi ,
                    NumeroEixos = consulta.numeroEixos,
                    Potencia = consulta.potencia,
                    Cilindrada = consulta.cilindrada,
                    CapacidadeCarga = consulta.capacidadeCarga,
                    //Cmt = consulta.cm ,
                    //Pbt = consulta.pb ,
                    //NumeroHodometro = consulta.num ,
                    CapacidadePassag = consulta.capacidadePassag,
                    //Lacre = consulta.lac ,
                    //VinRegravado = consulta.reg ,
                    NumeroMotor = consulta.numeroMotor,
                    //Apontamento = consulta.apon ,
                };

                httpStatusCode = HttpStatusCode.OK;
                retornoVM = new RetornoVM
                {
                    Message = "OK",
                    Code = (int)httpStatusCode,
                    Status = true,
                    Data = retorno
                };

                return Request.CreateResponse(httpStatusCode, retornoVM);

            }
            catch (Exception ex)
            {
                RetornoVM retornoVM = new RetornoVM
                {
                    Message = ex.Message,
                    Code = 500,
                    Status = false,
                    Data = null
                };

                return Request.CreateResponse(HttpStatusCode.InternalServerError, retornoVM);
            }
            
        }

        private long GeraDrTaxaVistoria(string chassi, Usuario usuario)
        {
            DetranVistoriaWS.geraDrTaxaVistoria_Input input = new DetranVistoriaWS.geraDrTaxaVistoria_Input()
            {
                codigoUsuario = long.Parse(ConfigurationManager.AppSettings["USER_WS_DETRAN"]),
                senhaUsuario = ConfigurationManager.AppSettings["PASS_WS_DETRAN"],
                codigoOperacao = "INC",
                chassiVeiculo = chassi,
                CnpjPagador = usuario.Sitio.Cnpj,
                matricAtendente = usuario.MatriculaAtendente,
                tipoServicoWeb = "V",
            };

            DetranVistoriaWS.wsdetranvistoriaSoapClient ws = new DetranVistoriaWS.wsdetranvistoriaSoapClient();
            DetranVistoriaWS.geraDrTaxaVistoria_Output response = ws.geraDrTaxaVistoria(input);

            if (response.retornoCritica != 0)
            {
                throw new Exception("Código de retorno: " + response.retornoCritica);
            }

            GeraDrTaxaVistoria taxa = db.GeraDrTaxaVistoria.Add(new GeraDrTaxaVistoria
            {
                codigoOperacao = response.codigoOperacao,
                codigoBarras = response.codigoBarras,
                //origemSolicitacao = response.origemSolicitacao,
                //estacaoSolicitacao = response.estacaoSolicitacao,
                cnpjPagador = response.CnpjPagador,
                chassiVeiculo = response.chassiVeiculo,
                nossoNumero = response.nossoNumero,
                retornoCritica = response.retornoCritica,
                codigoTaxa = response.codigoTaxa,
                valorDR = response.valorDR,
                dataVencimento = response.dataVencimento,
                codBarrasEdit = response.codigoBarras,
                DataHora = DateTime.Now
            });

            db.SaveChanges();

            return taxa.Id;

            //vistoriaEcv.geraDrTaxaVistoriaId = taxa.Id;
            //db.SaveChanges();

        }

        [HttpPost, ActionName("ObterEquipamentoChecklist")]
        public HttpResponseMessage ObterEquipamentoChecklist()
        {
            try
            {
                long tipo_veiculo = long.Parse(HttpContext.Current.Request.Form["tipo_veiculo"]);
                string user_token = HttpContext.Current.Request.Form["user_token"];

                RetornoVM retornoVM;
                HttpStatusCode httpStatusCode;

                SegurancaBO.ValidarToken(db, user_token);

                List<Equipamento> listEquipagamento = db.Equipamento.Ativos().Where(x => x.TipoVeiculoId == tipo_veiculo).ToList();

                List<EquipamentoVM> listRetorno = new List<EquipamentoVM>();

                foreach (Equipamento item in listEquipagamento)
                {
                    EquipamentoVM equipamento = new EquipamentoVM
                    {
                        Id = item.Id,
                        Nome = item.Nome,
                        NomeAmigavel = item.NomeAmigavel,
                        Obrigatorio = item.Obrigatorio,
                        Checked = false
                    };

                    listRetorno.Add(equipamento);
                }

                httpStatusCode = HttpStatusCode.OK;
                retornoVM = new RetornoVM
                {
                    Message = "OK",
                    Code = (int)httpStatusCode,
                    Status = true,
                    Data = listRetorno
                };

                return Request.CreateResponse(httpStatusCode, retornoVM);

            }
            catch (Exception ex)
            {
                RetornoVM retornoVM = new RetornoVM
                {
                    Message = ex.Message,
                    Code = 500,
                    Status = false,
                    Data = null
                };

                return Request.CreateResponse(HttpStatusCode.InternalServerError, retornoVM);
            }

        }

        [HttpPost, ActionName("EnviarVistoria")]
        public HttpResponseMessage EnviarVistoria()
        {
            try
            {
                string user_token = HttpContext.Current.Request.Form["user_token"];
                string foto_chassi = HttpContext.Current.Request.Form["foto_chassi"];
                string foto_placa = HttpContext.Current.Request.Form["foto_placa"];
                string foto_motor = HttpContext.Current.Request.Form["foto_motor"];
                string foto_hodometro = HttpContext.Current.Request.Form["foto_hodometro"];
                string foto_frente = HttpContext.Current.Request.Form["foto_frente"];
                string foto_lat_esq_frente = HttpContext.Current.Request.Form["foto_lat_esq_frente"];
                string foto_lat_dir_frente = HttpContext.Current.Request.Form["foto_lat_dir_frente"];
                string foto_fundo = HttpContext.Current.Request.Form["foto_fundo"];
                string foto_lat_esq_fundo = HttpContext.Current.Request.Form["foto_lat_esq_fundo"];
                string foto_lat_dir_fundo = HttpContext.Current.Request.Form["foto_lat_dir_fundo"];
                string foto_panoramica = HttpContext.Current.Request.Form["foto_panoramica"];
                
                CaracteristicaVeiculoVM caracteristica_veiculo = JsonConvert.DeserializeObject<CaracteristicaVeiculoVM>(HttpContext.Current.Request.Form["caracteristica_veiculo"]);
                ConsultaVeiculoVM consulta_veiculo = JsonConvert.DeserializeObject<ConsultaVeiculoVM>(HttpContext.Current.Request.Form["consulta_veiculo"]);
                ServicoPlacaVM servico_placa = JsonConvert.DeserializeObject<ServicoPlacaVM>(HttpContext.Current.Request.Form["servico_placa"]);
                List<EquipamentoVM> list_equipamento = JsonConvert.DeserializeObject<List<EquipamentoVM>>(HttpContext.Current.Request.Form["list_equipamento"]);
                long id_vistoriador = long.Parse(HttpContext.Current.Request.Form["id_vistoriador"]);
                List<LogLocalizacaoVM> list_log_localizacao = JsonConvert.DeserializeObject<List<LogLocalizacaoVM>>(HttpContext.Current.Request.Form["log_localizacao"]);

                RetornoVM retornoVM;
                HttpStatusCode httpStatusCode;

                Usuario usuario = SegurancaBO.ValidarToken(db, user_token);

                db.Database.BeginTransaction(IsolationLevel.ReadUncommitted);

                ConsultaVeiculo consulta = db.ConsultaVeiculo.Find(consulta_veiculo.Id);

                VistoriaEcv vistoriaEcv = new VistoriaEcv();
                vistoriaEcv.consultaVeiculoId = consulta.Id;
                vistoriaEcv.placaVeiculo = caracteristica_veiculo.placa;
                vistoriaEcv.chassi = caracteristica_veiculo.chassi;
                vistoriaEcv.tipoVeiculoId = long.Parse(caracteristica_veiculo.tipoVeiculo);
                //vistoriaEcv.marcaModelo = caracteristica_veiculo.marcaModelo;
                vistoriaEcv.anoFabricacao = caracteristica_veiculo.anoFabricacao;
                vistoriaEcv.anoModelo = caracteristica_veiculo.anoModelo;
                vistoriaEcv.categoriaId = long.Parse(caracteristica_veiculo.categoriaVeiculo) == 0 ? 99 : long.Parse(caracteristica_veiculo.categoriaVeiculo);
                vistoriaEcv.corId = long.Parse(caracteristica_veiculo.cor);
                vistoriaEcv.carroceriaId = long.Parse(caracteristica_veiculo.carroceria);
                vistoriaEcv.especieId = long.Parse(caracteristica_veiculo.especie);
                vistoriaEcv.combustivelId = long.Parse(caracteristica_veiculo.combustivel);
                vistoriaEcv.numeroEixos = caracteristica_veiculo.eixos;
                vistoriaEcv.km = caracteristica_veiculo.km;
                vistoriaEcv.potencia = caracteristica_veiculo.potencia;
                vistoriaEcv.cilindrada = caracteristica_veiculo.cilindrada;
                vistoriaEcv.capacidadeCarga = caracteristica_veiculo.carga;
                vistoriaEcv.capacidadePassag = caracteristica_veiculo.passageiro;
                vistoriaEcv.numeroMotor = caracteristica_veiculo.numeroMotor;
                vistoriaEcv.cmt = caracteristica_veiculo.cmt;
                vistoriaEcv.pbt = caracteristica_veiculo.pbt;
                vistoriaEcv.lacre = caracteristica_veiculo.lacre;
                vistoriaEcv.numeroHodometro = caracteristica_veiculo.numeroHodometro;
                vistoriaEcv.statusMotor = caracteristica_veiculo.statusMotor;
                vistoriaEcv.indicaNovoUsado = caracteristica_veiculo.indicaNovoUsado;
                vistoriaEcv.vinRegravado = caracteristica_veiculo.vinRegravado;
                vistoriaEcv.apontamento = caracteristica_veiculo.apontamento;
                vistoriaEcv.nomeProprietario = consulta.nomeProprietArre;
                vistoriaEcv.codigoRenavam = consulta.paramRenavam;
                vistoriaEcv.observacoesVistoriador = servico_placa.observacoes;
                //vistoriaEcv.placa_acordo = servico_placa.placa_acordo;
                vistoriaEcv.solicTrocaPlacaId = servico_placa.placa_acordo;
                vistoriaEcv.vistoriadorId = id_vistoriador;
                vistoriaEcv.dataVistoria = DateTime.Now;
                vistoriaEcv.sitioId = usuario.SitioId;
                vistoriaEcv.dataValidade = DateTime.Now.AddMonths(1);
                vistoriaEcv.dataEmissao = DateTime.Now;
                vistoriaEcv.statusVistoriaId = (int)Enumerators.StatusVistoria.Aguardando_Analise;
                vistoriaEcv.geraDrTaxaVistoriaId = consulta_veiculo.TaxaId;
                //vistoriaEcv.cnpjECV = consulta.Numero_CNPJ;

                db.VistoriaEcv.Add(vistoriaEcv);
                db.SaveChanges();

                foreach (EquipamentoVM equipamento in list_equipamento)
                {
                    db.VistoriaEquipamento.Add(new VistoriaEquipamento
                    {
                        EquipamentoId = equipamento.Id,
                        VistoriaId = vistoriaEcv.Id,
                        Ativo = equipamento.Checked
                    });
                }

                db.SaveChanges();

                list_log_localizacao = list_log_localizacao.OrderBy(x => x.TipoLogLocalizacaoId).ToList();

                long[] listTipoLog = new long[] { 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
                List<long> listTipoLogAdd = new List<long>();

                long indexLogAtual = 0;
                for (int i = 0; i < list_log_localizacao.Count; i++)
                {
                    db.LogLocalizacao.Add(new LogLocalizacao
                    {
                        Latitude = list_log_localizacao[i].Latitude,
                        Longitude = list_log_localizacao[i].Longitude,
                        VistoriaId = vistoriaEcv.Id,
                        UsuarioId = list_log_localizacao[i].UsuarioId,
                        TipoLogLocalizacaoId = list_log_localizacao[i].TipoLogLocalizacaoId,
                        DataAcao = list_log_localizacao[i].DataAcao.AddHours(-3)
                    });

                    if (i > 0 && list_log_localizacao[i].TipoLogLocalizacaoId != listTipoLog[indexLogAtual])
                    {
                        db.LogLocalizacao.Add(new LogLocalizacao
                        {
                            Latitude = list_log_localizacao[i-1].Latitude,
                            Longitude = list_log_localizacao[i-1].Longitude,
                            VistoriaId = vistoriaEcv.Id,
                            UsuarioId = list_log_localizacao[i-1].UsuarioId,
                            TipoLogLocalizacaoId = listTipoLog[indexLogAtual],
                            DataAcao = list_log_localizacao[i-1].DataAcao.AddHours(-3).AddSeconds(2)
                        });
                        indexLogAtual++;
                    }

                    indexLogAtual++;
                }

                db.SaveChanges();


                //#if !DEBUG
                this.ExportToImage(foto_chassi, vistoriaEcv, "foto_chassi");
                if(!String.IsNullOrEmpty(foto_placa))
                {
                    this.ExportToImage(foto_placa, vistoriaEcv, "foto_placa");
                }
                this.ExportToImage(foto_motor, vistoriaEcv, "foto_motor");
                this.ExportToImage(foto_hodometro, vistoriaEcv, "foto_hodometro");
                this.ExportToImage(foto_frente, vistoriaEcv, "foto_frente");
                this.ExportToImage(foto_lat_esq_frente, vistoriaEcv, "foto_lat_esq_frente");
                this.ExportToImage(foto_lat_dir_frente, vistoriaEcv, "foto_lat_dir_frente");
                this.ExportToImage(foto_fundo, vistoriaEcv, "foto_fundo");
                this.ExportToImage(foto_lat_esq_fundo, vistoriaEcv, "foto_lat_esq_fundo");
                this.ExportToImage(foto_lat_dir_fundo, vistoriaEcv, "foto_lat_dir_fundo");
                //this.ExportToImage(foto_panoramica, vistoriaEcv, "foto_panoramica");
                this.TransferirImagemPatio(vistoriaEcv);
                //#endif

                db.Database.CurrentTransaction.Commit();

                httpStatusCode = HttpStatusCode.OK;
                retornoVM = new RetornoVM
                {
                    Message = "Vistoria enviada com sucesso!",
                    Code = (int)httpStatusCode,
                    Status = true,
                    Data = null
                };

                return Request.CreateResponse(httpStatusCode, retornoVM);

            }
            catch (Exception ex)
            {
                db.Database.CurrentTransaction.Rollback();

                RetornoVM retornoVM = new RetornoVM
                {
                    Message = ex.Message,
                    Code = 500,
                    Status = false,
                    Data = null
                };

                return Request.CreateResponse(HttpStatusCode.InternalServerError, retornoVM);
            }

        }

        protected void TransferirImagemPatio(VistoriaEcv vistoriaEcv)
        {
            try
            {
                string mesAno2 = "202212";
                string fileName = ConfigurationManager.AppSettings["CAMINHO_FOTOS"] + "\\" + mesAno2 + "\\-1\\" + mesAno2 + ".png";

                byte[] buffer = null;
                using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    buffer = new byte[fs.Length];
                    fs.Read(buffer, 0, (int)fs.Length);

                    using (MemoryStream ms = new MemoryStream(buffer))
                    {
                        //ms.WriteTo(Response.OutputStream);

                        DateTime dateTime = DateTime.Now;

                        string mes = dateTime.Month < 10 ? "0" + dateTime.Month.ToString() : dateTime.Month.ToString();
                        string mesAno = dateTime.Year.ToString() + mes;

                        String path = ConfigurationManager.AppSettings["CAMINHO_FOTOS"] + "\\" + mesAno + "\\" + vistoriaEcv.Id;

                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }

                        string imgPath = Path.Combine(path, "foto_panoramica" + ".png");


                        File.WriteAllBytes(imgPath, buffer);
                    }

                }
            }
            catch (Exception)
            {

            }
        }

        protected void ExportToImage(string base64, VistoriaEcv vistoriaEcv, string nome_arquivo)
        {
            try
            {
                DateTime dateTime = DateTime.Now;

                string mes = dateTime.Month < 10 ? "0" + dateTime.Month.ToString() : dateTime.Month.ToString();
                string mesAno = dateTime.Year.ToString() + mes;

                String path = ConfigurationManager.AppSettings["CAMINHO_FOTOS"] + "\\" + mesAno + "\\" + vistoriaEcv.Id;

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                string imgPath = Path.Combine(path, nome_arquivo + ".png");

                byte[] imageBytes = Convert.FromBase64String(base64);

                File.WriteAllBytes(imgPath, imageBytes);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        private DetranVistoriaWS.consultaVeiculoBin_Output ConsultarVeiculoBin(string placa_chassi, string cnpjSitio)
        {
            DetranVistoriaWS.consultaVeiculoBin_Input input = new DetranVistoriaWS.consultaVeiculoBin_Input()
            {
                codigoUsuario = long.Parse(ConfigurationManager.AppSettings["USER_WS_DETRAN"]),
                senhaUsuario = ConfigurationManager.AppSettings["PASS_WS_DETRAN"],
                codigoOperacao = "CON",
                Numero_CNPJ = cnpjSitio
            };

            if (placa_chassi.Length > 7)
            {
                input.paramChassi = placa_chassi;
            }
            else
            {
                input.paramPlaca = placa_chassi;
            }

            DetranVistoriaWS.wsdetranvistoriaSoapClient ws = new DetranVistoriaWS.wsdetranvistoriaSoapClient();
            DetranVistoriaWS.consultaVeiculoBin_Output response = ws.consultaVeiculoBin(input);

            return response;
        }

        

        
    }
}