﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Vistoria.Business;
using Vistoria.Models;
using Vistoria.Models.Context;
using Newtonsoft.Json.Linq;

namespace Vistoria.Controllers.Api
{
    public class UsuarioApiController : ApiController
    {
        private VistoriaContext db;
        public UsuarioApiController()
        {
            db = new VistoriaContext();
        }

        [HttpPost]
        public IHttpActionResult ObterPorLogin(JObject jsonData)
        {
            dynamic json = jsonData;
            string login = json.Login;
            Usuario usuario = db.Usuario.Where(x => x.Login == login).FirstOrDefault();

            return Json(usuario);
        }

        [HttpPost, ActionName("ResetarSenha")]
        public IHttpActionResult ResetarSenha(ResetVM resetVM)
        {
            Usuario usuario = db.Usuario.Ativos().Where(x => x.Email == resetVM.Email).FirstOrDefault();

            if (usuario != null)
            {
                var dic = new Dictionary<string, string>();
                dic.Add("{#Login}", usuario.Login);
                string html;
                if (usuario.AutenticaAD == false)
                {
                    string senha = "sebrae" + DateTime.Now.Second.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Hour.ToString();
                    usuario.Senha = Hash.SHA256(senha);
                    db.SaveChanges();

                    dic.Add("{#Senha}", senha);
                    html = Utils.LerTemplateEsubstituirTags(System.Web.Hosting.HostingEnvironment.MapPath("~/Templates/usuarioSenha.html"), dic);

                    Task task = Task.Factory
                        .StartNew(() =>
                        {
                            try
                            {
                                SendEmail.Enviar(usuario.Email, "Acesso ao sistema", html);
                            }
                            catch (Exception)
                            {
                            }

                        });

                    return Json(new { Status = 1, Mensagem = "Enviamos uma nova senha para e-mail: <b>" + usuario.Email + "</b>" });
                }
                else
                {
                    return Json(new { Status = 0, Mensagem = "Procure o suporte do ECV para alterar sua senha!" });
                }
            }
            else
            {
                return Json(new { Status = 0, Mensagem = "E-mail não encontrado ou desativado!" });
            }

        }

        public class ResetVM
        {
            public string Email { get; set; }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}