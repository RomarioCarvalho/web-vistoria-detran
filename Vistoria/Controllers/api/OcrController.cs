﻿using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Vistoria.Models.Context;
using Vistoria.Models.ViewModel;
using Vistoria.Models;
using Vistoria.Business;
using SimpleLPR2;
using System.Collections.Generic;
using System.Linq;

namespace Vistoria.Controllers.Api
{
    public class OcrController : ApiController
    {

        private VistoriaContext db;
        public OcrController()
        {
            db = new VistoriaContext();
        }

        [HttpPost, ActionName("Consultar")]
        public HttpResponseMessage Consultar()
        {
            try
            {
                string placa_base64 = HttpContext.Current.Request.Form["placa_base64"];
                bool is_mercosul = Boolean.Parse(HttpContext.Current.Request.Form["is_mercosul"]);
                string user_token = HttpContext.Current.Request.Form["user_token"];
                string versao = "16";

                RetornoVM retornoVM;
                HttpStatusCode httpStatusCode;

                SegurancaBO.ValidarToken(db, user_token);

                // Chamar OCR
                string textoPlaca = "";

                if (is_mercosul)
                {
                    versao = "22";
                }

                ISimpleLPR lpr = SimpleLPR.Setup(HttpContext.Current.Server.MapPath("~/bin/native" + versao));

                // Set the product key
                lpr.set_productKey(HttpContext.Current.Server.MapPath("~/key.xml"));
                // Enable Brazil
                lpr.set_countryWeight("Brazil", 1.0f);
                // Apply changes
                lpr.realizeCountryWeights();
                // Create Processor
                IProcessor proc = lpr.createProcessor();
                // Process source file
                List<Candidate> cds = proc.analyze(Utils.Base64ToBitmap(placa_base64), 120);
                if (cds.Count > 0)
                {
                    // Iterate over all candidates
                    textoPlaca = cds.OrderByDescending(x => x.confidence).First().text.Replace(" ", "");
                    textoPlaca = textoPlaca.Replace("-", "");

                    httpStatusCode = HttpStatusCode.OK;
                    retornoVM = new RetornoVM
                    {
                        Message = "OK",
                        Code = (int)httpStatusCode,
                        Status = true,
                        Data = textoPlaca
                    };
                }
                else
                {
                    httpStatusCode = HttpStatusCode.NotFound;
                    retornoVM = new RetornoVM
                    {
                        Message = "Não detectada",
                        Code = (int)httpStatusCode,
                        Status = false,
                        Data = textoPlaca
                    };
                }

                return Request.CreateResponse(httpStatusCode, retornoVM);

            }
            catch (Exception ex)
            {
                RetornoVM retornoVM = new RetornoVM
                {
                    Message = ex.Message,
                    Code = 500,
                    Status = false,
                    Data = null
                };

                return Request.CreateResponse(HttpStatusCode.InternalServerError, retornoVM);
            }
            
        }

        

        
    }
}