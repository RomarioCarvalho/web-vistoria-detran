﻿using System;
using System.Web.Http;
using Vistoria.Business;
using Vistoria.Models;
using Vistoria.Models.Context;

namespace Vistoria.Controllers.Api
{
    public class DetranWSController : ApiController
    {
        private VistoriaContext db;
        public DetranWSController()
        {
            db = new VistoriaContext();
        }

        [HttpGet]
        public void AlterarSenha()
        {
            try
            {
                AlterarSenha alterarSenha = new AlterarSenha
                {
                    codigoOperacao = "456",
                    novaSenha = "4567",
                    senhaConfirmada = "4567"
                };

                DetranVistoriaWS.wsdetranvistoriaSoapClient ws = new DetranVistoriaWS.wsdetranvistoriaSoapClient();
                ws.alterarSenhaAsync(new DetranVistoriaWS.alterarSenha_Input()
                {
                    codigoOperacao = alterarSenha.codigoOperacao,
                    senhaUsuario = "123",
                    novaSenha = alterarSenha.novaSenha,
                    senhaConfirmada = alterarSenha.senhaConfirmada,
                    
                });

                
                alterarSenha.codigoRetExec = 987;

                db.AlterarSenha.Add(alterarSenha);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet]
        public void ConsultaVeicBaseEstBaseBinAmp()
        {
            try
            {
                /*ConsultaVeicBaseEstBaseBinAmp consultaVeicBaseEst = new ConsultaVeicBaseEstBaseBinAmp();
                consultaVeicBaseEst.codigoOperacao = "456";
                consultaVeicBaseEst.paramPlaca = "ABC1234";
                consultaVeicBaseEst.paramUF = "BA";
                consultaVeicBaseEst.paramChassi = "456456ASD4SA4S4AD4SASA4";
                consultaVeicBaseEst.paramRenavam = 45656544;
                consultaVeicBaseEst.Numero_CNPJ = "45678787878";
                consultaVeicBaseEst.nomeProprietArre = "456";
                consultaVeicBaseEst.codigoMunicipio = 456;
                consultaVeicBaseEst.nomeMunicipio = "456";
                consultaVeicBaseEst.unidadeFederacao = "456";
                consultaVeicBaseEst.nomeUf = "456";
                consultaVeicBaseEst.codigoMarcaMod = 456;
                consultaVeicBaseEst.marcaModelo = "456";
                consultaVeicBaseEst.codigoTipVeic = 456;
                consultaVeicBaseEst.tipoVeiculo = "456";
                consultaVeicBaseEst.codigoCarroceria = 456;
                consultaVeicBaseEst.carroceria = "456";
                consultaVeicBaseEst.codigoCor = 456;
                consultaVeicBaseEst.cor = "456";
                consultaVeicBaseEst.codigoCategoria = 456;
                consultaVeicBaseEst.categoriaVeiculo = "456";
                consultaVeicBaseEst.codigoEspecie = 456;
                consultaVeicBaseEst.especie = "456";
                consultaVeicBaseEst.anoModelo = 456;
                consultaVeicBaseEst.anoFabricacao = 456;
                consultaVeicBaseEst.potencia = 456;
                consultaVeicBaseEst.cilindrada = 456;
                consultaVeicBaseEst.codigoCombustive = 456;
                consultaVeicBaseEst.combustivel = "456";
                consultaVeicBaseEst.numeroMotor = "456";
                consultaVeicBaseEst.tracaoMax = 456;
                consultaVeicBaseEst.pesoBrutoTotal = 456;
                consultaVeicBaseEst.capacidadeCarga = 456;
                consultaVeicBaseEst.procedencia = "456";
                consultaVeicBaseEst.capacidadePassag = 456;
                consultaVeicBaseEst.numeroEixos = 456;
                consultaVeicBaseEst.restricao1 = "456";
                consultaVeicBaseEst.restricao2 = "456";
                consultaVeicBaseEst.restricao3 = "456";
                consultaVeicBaseEst.restricao4 = "456";
                consultaVeicBaseEst.restricao5 = "456";
                consultaVeicBaseEst.restricao6 = "456";
                consultaVeicBaseEst.codigoRetExec = 456;

                db.ConsultaVeicBaseEstBaseBinAmp.Add(consultaVeicBaseEst);
                db.SaveChanges();*/
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet]
        public void ConsultaVeiculoBin()
        {
            try
            {
                /*ConsultaVeiculoBin consultaVeiculoBin = new ConsultaVeiculoBin();
                consultaVeiculoBin.codigoOperacao = "456";
                consultaVeiculoBin.paramPlaca = "456";
                consultaVeiculoBin.paramUF = "456";
                consultaVeiculoBin.paramChassi = "456";
                consultaVeiculoBin.paramRenavam = 456;
                consultaVeiculoBin.Numero_CNPJ = "456";
                consultaVeiculoBin.codigoRetExec = 456;
                consultaVeiculoBin.nomeProprietArre = "456";
                consultaVeiculoBin.codigoMunicipio = 456;
                consultaVeiculoBin.nomeMunicipio = "456";
                consultaVeiculoBin.unidadeFederacao = "456";
                consultaVeiculoBin.nomeUf = "456";
                consultaVeiculoBin.codigoMarcaMod = 456;
                consultaVeiculoBin.marcaModelo = "456";
                consultaVeiculoBin.motorDifAcesso = "456";
                consultaVeiculoBin.comunicacaoVenda = "456";
                consultaVeiculoBin.codigoCarroceria = 456;
                consultaVeiculoBin.carroceria = "456";
                consultaVeiculoBin.codigoCor = 456;
                consultaVeiculoBin.cor = "456";
                consultaVeiculoBin.codigoCategoria = 456;
                consultaVeiculoBin.categoriaVeiculo = "456";
                consultaVeiculoBin.codigoEspecie = 456;
                consultaVeiculoBin.especie = "456";

                db.ConsultaVeiculoBin.Add(consultaVeiculoBin);
                db.SaveChanges();*/
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet]
        public void GeraDrTaxaVistoria()
        {
            try
            {
                long matriculaAtendente = SessionHelper.Usuario.MatriculaAtendente;

                GeraDrTaxaVistoria geraDrTaxaVistoria = new GeraDrTaxaVistoria();
                geraDrTaxaVistoria.codigoOperacao = "456";
                geraDrTaxaVistoria.matriculaAtendente = matriculaAtendente;
                geraDrTaxaVistoria.origemSolicitacao = "456";
                geraDrTaxaVistoria.estacaoSolicitacao = "456";
                geraDrTaxaVistoria.cnpjPagador = "456";
                geraDrTaxaVistoria.chassiVeiculo = "456";
                geraDrTaxaVistoria.nossoNumero = 456;
                geraDrTaxaVistoria.retornoCritica = 456;
                geraDrTaxaVistoria.codigoTaxa = 456;
                geraDrTaxaVistoria.valorDR = 456;
                geraDrTaxaVistoria.dataVencimento = 456;
                geraDrTaxaVistoria.codigoBarras = "456";
                geraDrTaxaVistoria.codBarrasEdit = "456";

                db.GeraDrTaxaVistoria.Add(geraDrTaxaVistoria);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet]
        public void GeraVistoriaECV()
        {
            try
            {
                GeraVistoriaECV geraVistoriaECV = new GeraVistoriaECV();
                geraVistoriaECV.codigoOperacao = "456";
                geraVistoriaECV.matriculaAtendente = SessionHelper.Usuario.MatriculaAtendente;
                geraVistoriaECV.origemSolicitacao = "456";
                geraVistoriaECV.estacaoSolicitacao = "456";
                geraVistoriaECV.indicaNovoUsado = "456";
                geraVistoriaECV.ufVeiculo = "456";
                geraVistoriaECV.placaVeiculo = "456";
                geraVistoriaECV.solicTrocaPlaca = "456";
                geraVistoriaECV.chassi = "456";
                geraVistoriaECV.numeroMotor = "456";
                geraVistoriaECV.vinRegravado = "456";
                geraVistoriaECV.statusMotor = "456";
                geraVistoriaECV.codigoRenavam = 456;
                geraVistoriaECV.cpfCnpjProp = 456;
                geraVistoriaECV.tipoDocumento = 456;
                geraVistoriaECV.rg = "456";
                geraVistoriaECV.nomeProprietario = "456";
                geraVistoriaECV.endereco = "456";
                geraVistoriaECV.numeroEndereco = "456";
                geraVistoriaECV.compleImovelProp = "456";
                geraVistoriaECV.nomeBairro = "456";
                geraVistoriaECV.municipioProp = "456";
                geraVistoriaECV.UfProprietario = "456";
                geraVistoriaECV.cepImovelProp = 456;
                geraVistoriaECV.ddd = 456;
                geraVistoriaECV.numeroFoneProp = 456;
                geraVistoriaECV.nrVistoriaECV = 456;
                geraVistoriaECV.dataVistoria = 456;
                geraVistoriaECV.horaVistoria = 456;
                geraVistoriaECV.matVistoriador = 456;
                geraVistoriaECV.cnpjECV = "456";
                geraVistoriaECV.statusVistoria = "456";
                geraVistoriaECV.dataEmissao = 456;
                geraVistoriaECV.horaEmissao = 456;
                geraVistoriaECV.dataValidade = 456;
                geraVistoriaECV.codigoServico01 = 456;
                geraVistoriaECV.codigoServico02 = 456;
                geraVistoriaECV.codigoServico03 = 456;
                geraVistoriaECV.codigoServico04 = 456;
                geraVistoriaECV.codigoServico05 = 456;
                geraVistoriaECV.codigoServico06 = 456;
                geraVistoriaECV.codigoServico07 = 456;
                geraVistoriaECV.vistoriaLacrada = "456";
                geraVistoriaECV.codigoReprovado = "456";
                geraVistoriaECV.nossoNumero = 456;
                geraVistoriaECV.retornoCritica = 456;
                geraVistoriaECV.numeroVistoria = 456;
                geraVistoriaECV.placaAtribuida = "456";
                geraVistoriaECV.numeroProtocolo = 456;

                db.GeraVistoriaECV.Add(geraVistoriaECV);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}