﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Vistoria.Business;
using Vistoria.Models;
using Vistoria.Models.Context;
using NITGEN.SDK.NBioBSP;

namespace Vistoria.Controllers.Api
{
    public class BiometriaController : ApiController
    {

        private VistoriaContext db;
        public BiometriaController()
        {
            db = new VistoriaContext();
        }

        [HttpPost, ActionName("Cadastrar")]
        public IHttpActionResult Cadastrar(JObject jsonData)
        {
            dynamic json = jsonData;
            long id = long.Parse(json.id.ToString());
            string template = json.template.ToString();

            Usuario usuario = db.Usuario.Find(id);

            if (usuario != null)
            {
                usuario.Biometria = template;
                db.SaveChanges();
                
                return Json(new { Status = 1, Mensagem = "Biometria cadastrada com sucesso!" });
            }
            else
            {
                return Json(new { Status = 0, Mensagem = "Usuário inválido!" });
            }

        }

        [HttpPost, ActionName("Validar")]
        public IHttpActionResult Validar(JObject jsonData)
        {
            dynamic json = jsonData;
            long usuarioId = long.Parse(json.usuarioId.ToString());
            string tokenBiometria = json.tokenBiometria.ToString();

            Usuario usuario = db.Usuario.Find(usuarioId);

            if (usuario != null)
            {
                if(!usuario.TokenBiometria.Equals(tokenBiometria))
                {
                    return Json(new { Status = 0, Mensagem = "Token de Biometria inválido!" });
                }

                string hash = Utils.GerarToken();
                usuario.HashBiometria = hash;
                db.SaveChanges();

                return Json(new { Status = 1, Mensagem = "OK", Hash = hash, Digital = usuario.Biometria });
            }
            else
            {
                return Json(new { Status = 0, Mensagem = "Usuário inválido!" });
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}