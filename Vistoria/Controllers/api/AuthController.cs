﻿using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Vistoria.Models.Context;
using Vistoria.Models.ViewModel;
using Vistoria.Models;
using Vistoria.Business;

namespace Vistoria.Controllers.Api
{
    public class AuthController : ApiController
    {

        private VistoriaContext db;
        public AuthController()
        {
            db = new VistoriaContext();
        }

        [HttpPost, ActionName("Login")]
        public HttpResponseMessage Login()
        {
            try
            {
                string login = HttpContext.Current.Request.Form["login"];
                string senha = HttpContext.Current.Request.Form["senha"];
                double latitude = Convert.ToDouble(HttpContext.Current.Request.Form["latitude"]); // -129699479
                double longitude = Convert.ToDouble(HttpContext.Current.Request.Form["longitude"]); // -384707175

                RetornoVM retornoVM;
                HttpStatusCode httpStatusCode;

                Usuario model = new Usuario
                {
                    Login = login,
                    Senha = senha
                };

                Usuario usuario = SegurancaBO.Autenticar(db, model, true);

                double latSitio = Convert.ToDouble(usuario.Sitio.Latitude); // -129986584
                double longSitio = Convert.ToDouble(usuario.Sitio.Longitude); // -384744777

                double raioVistoria = Math.Sqrt(((latitude - latSitio) * (latitude - latSitio)) + ((longitude - longSitio) * (longitude - longSitio)));
                // 289556.88807037554

                if (raioVistoria > usuario.Sitio.Raio)
                {
                    throw new Exception("Vistoria fora do Raio!");
                }

                VistoriadorVM vistoriador = new VistoriadorVM
                {
                    Id = usuario.Id,
                    Login = usuario.Login,
                    Nome = usuario.Nome,
                    Token = usuario.Token
                };

                httpStatusCode = HttpStatusCode.OK;
                retornoVM = new RetornoVM
                {
                    Message = "OK",
                    Code = (int)httpStatusCode,
                    Status = true,
                    Data = vistoriador
                };

                return Request.CreateResponse(httpStatusCode, retornoVM);

            }
            catch (Exception ex)
            {
                RetornoVM retornoVM = new RetornoVM
                {
                    Message = ex.Message,
                    Code = 500,
                    Status = false,
                    Data = null
                };

                return Request.CreateResponse(HttpStatusCode.InternalServerError, retornoVM);
            }
            
        }

        

        
    }
}