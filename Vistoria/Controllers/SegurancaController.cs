﻿using Vistoria.Business;
using Vistoria.Models;
using Vistoria.Models.Context;
using Vistoria.Models.ViewModel;
using Vistoria.Enumerators;
using System;
using System.Configuration;
using System.Web.Mvc;
using System.Web.Security;

namespace Vistoria.Controllers
{
    public class SegurancaController : AbstractController<VistoriaContext>
    {
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            if (Request.IsAuthenticated && !string.IsNullOrEmpty(returnUrl))
            {
                return Redirect(returnUrl);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Usuario model, string returnUrl)
        {
            try
            {
                SegurancaBO.Autenticar(db, model, false);
                
                FormsAuthentication.SetAuthCookie(model.Login, false);

                if (AcessoBO.TemAcesso("Vistoria", "Home", "Index"))
                {
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return Redirect(ConfigurationManager.AppSettings["URL_DEFAULT"]);
                    }
                }
                else
                {
                    return Redirect("~/Home/Index");
                }

            }
            catch (Exception ex)
            {
                SessionHelper.AddMessage(MessageTipo.Danger, "Erro de autenticação", ex.Message);
                ViewBag.ReturnUrl = returnUrl;
                return View(model);
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);

            filterContext.ExceptionHandled = true;

            var returnUrl = Request["ReturnUrl"];

            if (!string.IsNullOrEmpty(returnUrl))
            {
                filterContext.Result = new RedirectResult(returnUrl);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Logout(string returnUrl)
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Login", new { returnUrl });
        }

        public ActionResult AlterarSenha(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AlterarSenha(AlterarSenhaVM model, string returnUrl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ViewBag.ReturnUrl = returnUrl;
                    SegurancaBO.AlterarSenha(db, model);
                    SessionHelper.AddMessage(MessageTipo.Success, "Alterar Senha", "Senha atualiza com sucesso!");
                    return RedirectToAction("AlterarSenha");
                }
            }
            catch(Exception e)
            {
                SessionHelper.AddMessage(MessageTipo.Danger, "Alterar Senha", e.Message);
            }
            return View(model);
        }

    }
}