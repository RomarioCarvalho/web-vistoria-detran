﻿using Vistoria.Models;
using Vistoria.Models.Context;
using Vistoria.Business;
using Vistoria.Enumerators;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Vistoria.Controllers
{
    public class GrupoController : AbstractController<VistoriaContext>
    {
        // GET: Grupos
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [OutputCache(Duration = 0)]
        public virtual ActionResult Pagination(string sEcho, int iDisplayStart, int iColumns, int iDisplayLength, string sSearch, int iSortCol_0, string sSortDir_0)
        {
            string controller = ControllerContext.RouteData.Values["Controller"].ToString();
            sSearch = $"%{sSearch}%";
            var query = db.Grupo.Ativos().Where(x => DbFunctions.Like(x.Nome, sSearch));
            if(sSortDir_0.ToLower() == "asc") {
                switch(iSortCol_0)
                {
                    case 0:
                        query = query.OrderBy(x => x.Id);
                        break;
                    case 1:
                        query = query.OrderBy(x => x.Nome);
                        break;
                }
            }
            else
            {
                switch (iSortCol_0)
                {
                    case 0:
                        query = query.OrderByDescending(x => x.Id);
                        break;
                    case 1:
                        query = query.OrderByDescending(x => x.Nome);
                        break;
                }
            }
            int recordsTotal = query.Count();
            var aList = query.Skip(iDisplayStart).Take(iDisplayLength);
            var data = aList.ToList().Select(x => new
            {
                x.Id,
                x.Nome,
                acoes = $"<a href='{Url.Action("Edit", controller)}/{x.Id}'><i class='glyphicon glyphicon-pencil'></i> Editar</a><button class='btn-link' onClick=\"excluir('{controller}', {x.Id}, '{x.Nome}')\"><i class='glyphicon glyphicon-trash'></i> Excluir</button>"
            }).ToArray();
            return Json(new
            {
                iDraw = 1,
                sEcho,
                iTotalRecords = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                aaData = data
            }, JsonRequestBehavior.AllowGet);
        }

        // GET: Grupos/Edit/5
        public ActionResult Edit(long? id)
        {
            Grupo grupo = new Grupo();
            ViewBag.Grupos = db.Modulo
                .Ativos()
                .OrderBy(x => x.Nome)
                .ToList();
            ViewBag.ModulosChecked = new List<Modulo>();
            if (id != null)
            {
                grupo = db.Grupo.Find(id);
                ViewBag.ModulosChecked = db.Grupo.Find(grupo.Id).ListModulo.ToList();
            }
            var selectListItems = new List<SelectListItem>();
            return View(grupo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Grupo _grupo)
        {
            bool inserir = _grupo.Id == 0;
            try
            {

                db.Database.BeginTransaction(IsolationLevel.ReadUncommitted);
                var nome = _grupo.Nome.ToLower();

                if (inserir)
                {
                    //Validar se o nome do grupo já existe                    
                    var exist = db.Grupo.Any(n => n.Nome.ToLower() == nome);
                    if (exist)
                    {
                        throw new Exception($"O grupo {nome} já está cadastrado.");
                    }
                    _grupo.ListModulo = new List<Modulo>();
                    db.Grupo.Add(_grupo);
                }
                else
                {

                    //Validar se o nome do grupo ja existe
                    var exist = db.Grupo.Any(n => n.Nome.ToLower() == nome && n.Id != _grupo.Id);
                    if (exist)
                    {
                        throw new Exception($"O grupo {nome} já está cadastrado.");
                    }

                    Grupo banco = db.Grupo.Find(_grupo.Id);
                    banco.Nome = _grupo.Nome;
                    banco.ModulosId = _grupo.ModulosId;
                    _grupo = banco;
                }
                db.SaveChanges();

                _grupo.ListModulo.Clear();
                _grupo.ModulosId.ForEach(x =>
                   _grupo.ListModulo.Add(db.Modulo.Find(x))
                );

                db.SaveChanges();
                db.Database.CurrentTransaction.Commit();
                SessionHelper.AddMessage(MessageTipo.Success, "Cadastro de Grupo", "Grupo "+ (inserir ? "Inserido":"Alterado") + " com sucesso");
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                db.Database.CurrentTransaction.Rollback();
                SessionHelper.AddMessage(MessageTipo.Danger, "Cadastro de Grupo", "Ocorreu um erro ao salvar o Grupo: " + e.Message);

                ViewBag.Grupos = db.Modulo
                    .Ativos()
                    .OrderBy(x => x.Nome)
                    .ToList();
                ViewBag.ModulosChecked = new List<Modulo>();
                if (!inserir)
                {
                    ViewBag.ModulosChecked = db.Grupo.Find(_grupo.Id).ListModulo.ToList();
                }
                return View(_grupo);
            }
        }

        public ActionResult Delete(int id)
        {
            Grupo grupo = db.Grupo.Find(id);
            db.Grupo.Remove(grupo);
            db.SaveChanges();
            SessionHelper.AddMessage(MessageTipo.Success, "Cadastro de Grupo", "Grupo excluído com sucesso");
            return RedirectToAction("Index");
        }

    }
}
