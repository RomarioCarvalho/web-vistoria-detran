﻿using Vistoria.Models.Context;
using Newtonsoft.Json.Linq;
using Vistoria.Business;
using System;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Text.RegularExpressions;

namespace Vistoria.Controllers
{
    public class LogController : AbstractController<VistoriaContext>
    {
        // GET: Log
        public ActionResult Index()
        {
            var list = db.Modulo
                .Where(x => x.Nome != "ConversacaoBot")
                .Where(x => x.Nome != "Sugestão QnA")
                .Where(x => x.Nome != "Log")
                .Where(x => x.Nome != "Dashboard")
                .Where(x => x.Nome != "Dashboard Eventos")
                .Select(t => new SelectListItem
                 {
                     Text = t.Nome,
                     Value = t.Nome
                 })
                .OrderBy(x => x.Text)
                .ToList();
            list.Insert(0, new SelectListItem { Text = "AtendimentoSebrae", Value = "AtendimentoSebrae" });

            ViewBag.ModuloId = list;
            return View();
        }

        public FileResult BaixarArquivo(string modulo, string usuario, string data1, string data2, string protocolo)
        {
            var sSearch = $"%{modulo}%";
            var query = db.Log.Where(x => DbFunctions.Like(x.Type, sSearch));

            if (!string.IsNullOrEmpty(usuario))
            {
                var usuario1 = db.Usuario.FirstOrDefault(n => n.Nome.ToLower().Contains(usuario.ToLower()));
                if (usuario1 != null)
                    query = query.Where(x => x.UsuarioId == usuario1.Id);
            }

            if (!string.IsNullOrEmpty(data1) && !string.IsNullOrEmpty(data2))
            {
                var dt1 = Convert.ToDateTime(data1);
                var dt2 = Convert.ToDateTime(data2);
                //query = query.Where(x => x.Data >= dt1 && x.Data <= dt2);
                query = query.Where(n => DbFunctions.TruncateTime(n.Data) >= DbFunctions.TruncateTime(dt1)
               && DbFunctions.TruncateTime(n.Data) <= DbFunctions.TruncateTime(dt2));
            }

            if (!string.IsNullOrEmpty(protocolo))
            {
                query = query.Where(x => x.Value.Contains(protocolo));
            }

            var strB = new StringBuilder();
            strB.AppendLine("Id;Data;Usuário;Ação;Módulo;Registro");

            foreach (var item in query.ToList())
            {
                string valor = ObterRegistro(item.Value, item.Type).Trim();
                if (valor != null && valor.IndexOf("\n") != -1)
                {
                    valor = "\"" + valor + "\"";
                }

                strB.AppendLine(
                    $"{item.Id};" +
                    $"{item.Data:dd/MM/yyyy HH:mm};" +
                    $"{db.Usuario.Where(x => x.Id == item.UsuarioId).FirstOrDefault()?.Nome};" +
                    $"{Utils.GetEnumDescription(item.Acao)};" +
                    $"{item.Type?.Trim()};" +
                    $"{valor.Replace("<b>", "").Replace("</b>", "")};"
                );
            }
            
            var data = Encoding.UTF8.GetBytes(strB.ToString());
            var result = Encoding.UTF8.GetPreamble().Concat(data).ToArray();

            return File(result, "application/csv", $"Logs {DateTime.Now}.csv");

        }

        //[HttpGet]
        [OutputCache(Duration = 0)]
        public virtual ActionResult Pagination(string sEcho, int iDisplayStart, int iColumns, int iDisplayLength,
            string ModuloId, string Usuario, string data1, string data2, string Protocolo, int iSortCol_0, string sSortDir_0)
        {
            string controller = ControllerContext.RouteData.Values["Controller"].ToString();
            var sSearch = $"%{ModuloId}%";
            var query = db.Log.Where(x => DbFunctions.Like(x.Type, sSearch));

            if (!string.IsNullOrEmpty(Usuario))
            {
                var usuario = db.Usuario.FirstOrDefault(n => n.Nome.ToLower().Contains(Usuario.ToLower()));
                if (usuario != null)
                    query = query.Where(x => x.UsuarioId == usuario.Id);
            }

            if (!string.IsNullOrEmpty(data1) && !string.IsNullOrEmpty(data2))
            {
                var dt1 = Convert.ToDateTime(data1);
                var dt2 = Convert.ToDateTime(data2);
                query = query.Where(n => DbFunctions.TruncateTime(n.Data) >= DbFunctions.TruncateTime(dt1)
                && DbFunctions.TruncateTime(n.Data) <= DbFunctions.TruncateTime(dt2));
                //query = query.Where(x =>   x.Data >= dt1 && x.Data <= dt2);
            }

            if (!string.IsNullOrEmpty(Protocolo))
            {
                query = query.Where(x => x.Value.Contains(Protocolo));
            }


            if (sSortDir_0.ToLower() == "asc")
            {

                switch (iSortCol_0)
                {
                    case 0:
                        query = query.OrderBy(n => n.Id);
                        break;
                    case 1:
                        query = query.OrderBy(n => n.Data);
                        break;
                    case 2:
                        query = query.OrderBy(n => n.UsuarioId);
                        break;
                    case 3:
                        query = query.OrderBy(n => n.Acao);
                        break;
                    case 4:
                        query = query.OrderBy(n => n.Type);
                        break;
                }
            }
            else
            {
                switch (iSortCol_0)
                {
                    case 0:
                        query = query.OrderByDescending(n => n.Id);
                        break;
                    case 1:
                        query = query.OrderByDescending(n => n.Data);
                        break;
                    case 2:
                        query = query.OrderByDescending(n => n.UsuarioId);
                        break;
                    case 3:
                        query = query.OrderByDescending(n => n.Acao);
                        break;
                    case 4:
                        query = query.OrderByDescending(n => n.Type);
                        break;
                }
            }

            int recordsTotal = query.Count();

            var data = query.ToList().Skip(iDisplayStart).Take(iDisplayLength).Select(x => new
            {
                x.Id,
                x.Type,
                Data = $"{x.Data:dd/MM/yyyy HH:mm}",
                Registro = ObterRegistro(x.Value, x.Type),
                Usuario = db.Usuario.Where(y => y.Id == x.UsuarioId).FirstOrDefault()?.Nome,
                Acao = Utils.GetEnumDescription(x.Acao)
            }).ToArray();

            return Json(new
            {
                iDraw = 1,
                sEcho,
                iTotalRecords = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                aaData = data
            }, JsonRequestBehavior.AllowGet);
        }

        private string ObterRegistro(string value, string type)
        {
            dynamic valor = JObject.Parse(value);

            switch (type)
            {
                case "Usuario":
                case "Modulo":
                case "Grupo":
                case "Bot":
                case "FluxoConversa":
                    return "<b>Nome:</b> " + valor.Nome.ToObject<string>();
                case "QnA":
                    return "<b>Pergunta:</b> " + valor.Pergunta.ToObject<string>();
                case "AtendimentoSebrae":
                    return "<b>Protocolo:</b> " + valor?.Protocolo?.ToObject<string>() +
                            "<br><b>Cpf:</b> " + Utils.FormatarCpfCnpj(valor?.Cpf?.ToObject<string>()) +
                            "<br><b>Cnpj:</b> " + Utils.FormatarCpfCnpj(valor?.Cnpj?.ToObject<string>()) +
                            "<br><b>Conversação Bot:</b> #" + valor?.ConversacaoBotId?.ToObject<string>();
                default:
                    string texto = "";
                    foreach (JProperty item in valor)
                    {
                        if (texto!="")
                        {
                            texto += "<br>";
                        }
                        texto+="<b>"+ item.Name + ":</b> " + StripHTML(item.Value.ToString());
                    }
                    return texto;
            }

        }

        public string StripHTML(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                string noHTML = Regex.Replace(input, @"<[^>]+>|&nbsp;", "").Trim();
                return Regex.Replace(noHTML, @"\s{2,}", " ");
            }
            return string.Empty;
        }

    }
}