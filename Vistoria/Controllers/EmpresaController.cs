﻿using Vistoria.Models;
using Vistoria.Models.Context;
using Vistoria.Business;
using Vistoria.Enumerators;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Vistoria.Controllers
{
    public class EmpresaController : AbstractController<VistoriaContext>
    {
        // GET: Empresa
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [OutputCache(Duration = 0)]
        public virtual ActionResult Pagination(string sEcho, int iDisplayStart, int iColumns, int iDisplayLength, string sSearch, int iSortCol_0, string sSortDir_0)
        {
            string controller = ControllerContext.RouteData.Values["Controller"].ToString();
            sSearch = $"%{sSearch}%";
            var query = db.Empresa.Ativos().Where(x => DbFunctions.Like(x.Nome, sSearch));
            if(sSortDir_0.ToLower() == "asc") {
                switch(iSortCol_0)
                {
                    case 0:
                        query = query.OrderBy(x => x.Id);
                        break;
                    case 1:
                        query = query.OrderBy(x => x.Nome);
                        break;
                }
            }
            else
            {
                switch (iSortCol_0)
                {
                    case 0:
                        query = query.OrderByDescending(x => x.Id);
                        break;
                    case 1:
                        query = query.OrderByDescending(x => x.Nome);
                        break;
                }
            }
            int recordsTotal = query.Count();
            var aList = query.Skip(iDisplayStart).Take(iDisplayLength);
            var data = aList.ToList().Select(x => new
            {
                x.Id,
                x.Nome,
                acoes = $"<a href='{Url.Action("Edit", controller)}/{x.Id}'><i class='glyphicon glyphicon-pencil'></i> Editar</a><button class='btn-link' onClick=\"excluir('{controller}', {x.Id}, '{x.Nome}')\"><i class='glyphicon glyphicon-trash'></i> Excluir</button>"
            }).ToArray();
            return Json(new
            {
                iDraw = 1,
                sEcho,
                iTotalRecords = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                aaData = data
            }, JsonRequestBehavior.AllowGet);
        }

        // GET: Empresa/Edit/5
        public ActionResult Edit(long? id)
        {
            Empresa empresa = new Empresa();
            
            if (id != null)
            {
                empresa = db.Empresa.Find(id);
            }
            
            return View(empresa);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Empresa _empresa)
        {
            bool inserir = _empresa.Id == 0;
            try
            {

                db.Database.BeginTransaction(IsolationLevel.ReadUncommitted);
                var nome = _empresa.Nome.ToLower();

                if (inserir)
                {
                    //Validar se o nome do Empresa já existe                    
                    var exist = db.Empresa.Any(n => n.Nome.ToLower() == nome);
                    if (exist)
                    {
                        throw new Exception($"A empresa {nome} já está cadastrada.");
                    }
                    db.Empresa.Add(_empresa);
                }
                else
                {

                    //Validar se o nome do Empresa ja existe
                    var exist = db.Empresa.Any(n => n.Nome.ToLower() == nome && n.Id != _empresa.Id);
                    if (exist)
                    {
                        throw new Exception($"A empresa {nome} já está cadastrada.");
                    }

                    Empresa banco = db.Empresa.Find(_empresa.Id);
                    banco.Nome = _empresa.Nome;

                    _empresa = banco;
                }
                db.SaveChanges();

                db.Database.CurrentTransaction.Commit();
                SessionHelper.AddMessage(MessageTipo.Success, "Cadastro de Empresa", "Empresa "+ (inserir ? "Inserida":"Alterada") + " com sucesso");

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                db.Database.CurrentTransaction.Rollback();
                SessionHelper.AddMessage(MessageTipo.Danger, "Cadastro de Empresa", "Ocorreu um erro ao salvar a Empresa: " + e.Message);
                
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            Empresa empresa = db.Empresa.Find(id);
            db.Empresa.Remove(empresa);
            db.SaveChanges();
            SessionHelper.AddMessage(MessageTipo.Success, "Cadastro de Empresa", "Empresa excluída com sucesso");
            return RedirectToAction("Index");
        }

    }
}
