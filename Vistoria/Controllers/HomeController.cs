﻿using Vistoria.Models.Context;
using System.Web.Mvc;

namespace Vistoria.Controllers
{
    public class HomeController : AbstractController<VistoriaContext>
    {
        // GET: Usuarios
        public ActionResult Index()
        {
            return View();
        }

    }

}

