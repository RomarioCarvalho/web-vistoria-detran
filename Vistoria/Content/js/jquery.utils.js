﻿function onShowMessageDangerBox(msg, titulo) {

    onShowMessageBox(msg, titulo, 'fa-ban','alert alert-danger alert-dismissible'); 
}

function onShowMessageSuccessBox(msg, titulo) {

    onShowMessageBox(msg, titulo, 'fa-check','alert alert-success alert-dismissible'); 
}


function onShowMessageBox(msg, titulo,icon,styleClass) {

    var alert = '<div class="' + styleClass + '">' +
        '<button type = "button" class="close" data-dismiss="alert" aria-hidden="true">x</button >' +
        '<h4><i class="icon fa '+ icon +'"></i>' + titulo + '</h4>' +
        '<i class="close icon"></i>' +
        '<p> ' + msg + ' </p></div>';

    $('#messageAlert').append(alert); 

}


 