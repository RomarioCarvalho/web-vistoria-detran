﻿using System.Reflection;

namespace Vistoria.Helpers
{
    public static class HtmlHelpers
    {
        // <summary>
        ///     Obter a versão da aplicação
        ///     <para>A cada build a versão é alterada</para>
        /// </summary>
        /// <returns></returns>
        public static string ObterVersaoAplicacao()
        {
            var major = Assembly.GetExecutingAssembly().GetName().Version.Major;
            var minor = Assembly.GetExecutingAssembly().GetName().Version.Minor;
            var build = Assembly.GetExecutingAssembly().GetName().Version.Revision;
            var versao = $"Versão {major}.{minor} Rev - {build}";
            return versao;
        }
        /// <summary>
        ///     Obter o nome do servidor
        ///     <para>Homologação | Produção </para>
        /// </summary>
        /// <returns></returns>
        public static string ObterAmbiente()
        {
            return System.Configuration.ConfigurationManager.AppSettings["AMBIENTE"];
        }

    }
}