﻿using Vistoria.Business;
using Vistoria.Enumerators;
using Vistoria.Models.ViewModel;
using RestSharp;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Mvc;

namespace Vistoria.Filters
{
    public class ValidarAcessoFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Controller abstractController = (Controller)filterContext.Controller;

            if (abstractController.User.Identity.IsAuthenticated)
            {
                string projeto = HttpContext.Current.ApplicationInstance.GetType().BaseType.Assembly.GetName().Name;
                string controller = HttpContext.Current.Request.RequestContext.RouteData.Values["Controller"].ToString();
                string action = HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString();

                if (SessionHelper.Usuario == null || SessionHelper.Usuario.Login != abstractController.User.Identity.Name)
                {
                    var client = new RestClient(Utils.FullUrlServer + ConfigurationManager.AppSettings["URL_CONTROLEACESSO_API"]);

                    string Login = abstractController.User.Identity.Name;

                    var request = new RestRequest("UsuarioApi/ObterPorLogin", Method.POST);
                    request.AddObject(new { Login });

                    var response = client.Execute<UsuarioVM>(request);

                    UsuarioVM usuario = response.Data;

                    usuario.ListModulo = new List<ModuloVM>();
                    foreach (GrupoVM grupo in usuario.ListGrupo)
                    {
                        usuario.ListModulo.AddRange(grupo.ListModulo);
                    }

                    usuario.ListModulo.Add(new ModuloVM() { Path = "Vistoria/Home/Index" });
                    usuario.ListModulo.Add(new ModuloVM() { Path = "Vistoria/Seguranca/AlterarSenha" });
                    usuario.ListModulo.Add(new ModuloVM() { Path = "Vistoria/Seguranca/Login" });
                    usuario.ListModulo.Add(new ModuloVM() { Path = "Vistoria/Seguranca/Logout" });
                    
                    SessionHelper.Usuario = usuario;

                }

                if (!AcessoBO.TemAcesso(projeto, controller, action))
                {
                    if(SessionHelper.Usuario != null)
                    {
                        if(controller != "Dashboard")
                        {
                            SessionHelper.AddMessage(MessageTipo.Danger, "Acesso Inválido", "Usuário não tem acesso ao módulo!");
                        }
                        filterContext.Result = new RedirectResult("~/Home/Index");
                    }
                }

            }

            base.OnActionExecuting(filterContext);
        }

    }
}